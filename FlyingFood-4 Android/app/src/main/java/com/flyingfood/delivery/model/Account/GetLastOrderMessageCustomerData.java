package com.flyingfood.delivery.model.Account;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 01/07/17.
 */

public class GetLastOrderMessageCustomerData {

    @SerializedName("Data")
    public ArrayList<GetLastOrderMessageCustomer> getLastOrderMessageCustomerArrayList;
}
