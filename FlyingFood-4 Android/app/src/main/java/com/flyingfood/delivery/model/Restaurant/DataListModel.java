package com.flyingfood.delivery.model.Restaurant;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Mr. DIPAK on 25-Jul-17.
 */

public class DataListModel {

    @SerializedName("Data")
    public ArrayList<DataModel> dataModelArrayList;


    public ArrayList<DataModel> getDataModelArrayList() {
        return dataModelArrayList;
    }

    public void setDataModelArrayList(ArrayList<DataModel> dataModelArrayList) {
        this.dataModelArrayList = dataModelArrayList;
    }
}
