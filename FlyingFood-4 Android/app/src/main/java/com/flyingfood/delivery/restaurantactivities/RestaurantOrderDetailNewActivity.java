package com.flyingfood.delivery.restaurantactivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.CommonChatActivity;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.GetOrderMenuItems;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class RestaurantOrderDetailNewActivity extends AppCompatActivity {

    private ImageView imgChat;

    TextView txtNameSurname, txtAddress, txtTelephone, txtEmail, txtRestaurantDetails, txtRestaurantTime, txtDeliveryTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_order_details);

        txtNameSurname = (TextView) findViewById(R.id.txtNameSurname);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtTelephone = (TextView) findViewById(R.id.txtTelephone);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
//        txtRestaurantDetails = (TextView) findViewById(R.id.txtRestaurantDetails);
        txtRestaurantTime = (TextView) findViewById(R.id.txtRestaurantTime);
        txtDeliveryTime = (TextView) findViewById(R.id.txtDeliveryTime);


        txtAddress.setText(getIntent().getStringExtra("Address"));
        //   txtNameSurname.setText(getIntent().getStringExtra("Name"));
        txtDeliveryTime.setText(getIntent().getStringExtra("DeliveryTime"));

        txtNameSurname.setText(getIntent().getStringExtra("Name"));
        txtTelephone.setText(getIntent().getStringExtra("Mobile"));
        txtEmail.setText(getIntent().getStringExtra("Email"));


        java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm a", Locale.US);
        java.util.Date date1 = null;
        try {
            date1 = df.parse(getIntent().getStringExtra("DeliveryTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.HOUR,0);
        cal.add(Calendar.MINUTE,-10);
        if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
            txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" PM");
        }else{
            txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" AM");
        }

        setToolbar();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Ordine #"+getIntent().getStringExtra("OrderID"));
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
