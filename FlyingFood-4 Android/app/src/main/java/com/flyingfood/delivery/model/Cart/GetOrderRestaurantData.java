package com.flyingfood.delivery.model.Cart;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 07/07/17.
 */

public class GetOrderRestaurantData {

    @SerializedName("Data")
    public ArrayList<GetOrderMenuItems> getOrderMenuItemsArrayList;
}
