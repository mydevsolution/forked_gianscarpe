package com.flyingfood.delivery.model.Restaurant;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr. DIPAK on 25-Jul-17.
 */

public class ResponseModel {

    @SerializedName("ResponseData")
    public DataListModel dataListModel;

    @SerializedName("Response")
    public CommonResponse commonResponse;

    public DataListModel getDataListModel() {
        return dataListModel;
    }

    public void setDataListModel(DataListModel dataListModel) {
        this.dataListModel = dataListModel;
    }
}
