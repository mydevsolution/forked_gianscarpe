package com.flyingfood.delivery.restaurantactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.MainActivity;
import com.flyingfood.delivery.custom.PrefUtils;
import com.google.android.gms.vision.text.Line;

public class RestaurantAccountActivity extends AppCompatActivity {

    TextView txtLogout;
    LinearLayout llWaitingTime,llMenu,llCourierRequest,llOrderHistory, llSetCloseRestaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_account);


        llWaitingTime= (LinearLayout) findViewById(R.id.llWaitingTime);
        llMenu= (LinearLayout) findViewById(R.id.llMenu);
        llOrderHistory= (LinearLayout) findViewById(R.id.llOrderHistory);
        llSetCloseRestaurant = (LinearLayout) findViewById(R.id.llSetRestaurantClose) ;

        llWaitingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RestaurantAccountActivity.this,RestaurantWatingTimeActivity.class);
                startActivity(intent);


            }
        });
        llMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RestaurantAccountActivity.this,RestaurantMenuActivity.class);
                startActivity(intent);


            }
        });


        llOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RestaurantAccountActivity.this,RestaurantOrderHistoryActivity.class);
                startActivity(intent);


            }
        });

        llSetCloseRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "rest Switch i", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), RestaurantSetCloseActivity.class);
                startActivity(intent);


            }
        });

        setToolbar();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account_logout_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnLogOut:

                PrefUtils.clearCurrentUser(RestaurantAccountActivity.this);
                Intent intent = new Intent(RestaurantAccountActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Account");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}
