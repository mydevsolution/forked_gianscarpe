package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.custom.StripeCardEntry;
import com.flyingfood.delivery.model.Account.GetPaymentDetails;
import com.flyingfood.delivery.model.Account.GetPaymentDetailsMain;

import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseMain;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;
import com.stripe.android.model.Card;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CustomerPaymentDetailsActivity extends AppCompatActivity {
    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetPaymentDetails> profileListView;

    Card cardToSave;
//    private StripeCardEntry mSwipeCardEntry;
   CardInputWidget mCardInputWidget;
    private EditText etDescription;
    private Button mPayButton;

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Payment Details");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_payment_details);
        profileList = (GridView)findViewById(R.id.lvProfiles);

        setToolbar();
        getPaymentDetails();
        mCardInputWidget = (CardInputWidget) findViewById(R.id.card_input_widget);
//        mSwipeCardEntry = (StripeCardEntry) findViewById(R.id.swipecardentry);
        mPayButton = (Button) findViewById(R.id.paybutton);


//        mSwipeCardEntry.setListener(new StripeCardEntry.Listener() {
//            @Override
//            public void onCardEntryCompleted(boolean completed) {
//                mPayButton.setEnabled(completed);
//                mPayButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//
//            }
//        });

        mPayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cardToSave = mCardInputWidget.getCard();
                if (cardToSave == null) {
//                    mErrorDialogHandler.showError("Invalid Card Data");
                    Toast.makeText(CustomerPaymentDetailsActivity.this, "Invalid Card Data", Toast.LENGTH_SHORT).show();
                }else{
                    addPaymentMethod();
                    Log.e("cvv",cardToSave.getCVC()+"");
                    Log.e("CardNumber",cardToSave.getNumber()+"");
                    Log.e("month",cardToSave.getExpMonth()+"");
                    Log.e("year",cardToSave.getExpYear()+"");

                }


            }
        });
    }




    private void addPaymentMethod() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerPaymentDetailsActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();




            try {
                registrationObject.put("CVV", cardToSave.getCVC());
                registrationObject.put("CardNumber", cardToSave.getNumber());
                registrationObject.put("ExpiryDate", cardToSave.getExpMonth()+"/"+cardToSave.getExpYear());
                registrationObject.put("UserId", PrefUtils.getUser(CustomerPaymentDetailsActivity.this).UserID+"");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.SAVE_PAYMENT, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerPaymentDetailsActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CustomerPaymentDetailsActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }




    private void getPaymentDetails() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerPaymentDetailsActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("UserId", PrefUtils.getUser(CustomerPaymentDetailsActivity.this).UserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GET_PAYMENT_DETAILS, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetPaymentDetailsMain submitResponse = new GsonBuilder().create().fromJson(response, GetPaymentDetailsMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getPaymentDetailsData.getPaymentDetailsArrayList;

                        myCustomAdapter = new MyCustomAdapter(CustomerPaymentDetailsActivity.this, profileListView);
                        profileList.setAdapter(myCustomAdapter);


                    } else {
                        Toast.makeText(CustomerPaymentDetailsActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerPaymentDetailsActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetPaymentDetails> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetPaymentDetails> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetPaymentDetails>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtExpireDate,txtCvv,name;
//            TextView txtPrice, txtOrderName,txtRestaurantName;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final CustomerPaymentDetailsActivity.MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_payment_card, parent, false);
                holder = new CustomerPaymentDetailsActivity.MyCustomAdapter.ViewHolder();


                holder.txtExpireDate= (TextView) convertView.findViewById(R.id.txtExpireDate);
                holder.txtCvv= (TextView) convertView.findViewById(R.id.txtCvv);
                holder.name= (TextView) convertView.findViewById(R.id.name);


                convertView.setTag(holder);
            } else {
                holder = (CustomerPaymentDetailsActivity.MyCustomAdapter.ViewHolder) convertView.getTag();
            }
            double price=0;
            holder.txtExpireDate.setText(profileListView.get(position).ExpiryDate+"");
            holder.txtCvv.setText(profileListView.get(position).CVV+"");
                    holder.name.setText("Viza MasterCard xxx- "+profileListView.get(position).CardNumber.substring(profileListView.get(position).CardNumber.length()-3,profileListView.get(position).CardNumber.length()));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return convertView;
        }




    }
    
    
}
