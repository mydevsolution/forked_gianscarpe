package com.flyingfood.delivery.restaurantactivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseMain;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class RestaurantOrderHistoryActivity extends AppCompatActivity {

    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetOrderUserIdWiseData> profileListView;
    private GetOrderUserIdWiseMain getOrderUserIdWiseMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_order_history);
      //  getOrderUserIdWiseData = PrefUtils.getRestaurantOrderHistory(RestaurantOrderHistoryActivity.this);
        profileList = (GridView)findViewById(R.id.lvProfiles);
        //profileListView =  ArrayList<GetOrderMenuItems> getOrderMenuItemsArrayList;
        getOrdersUserIdWise();
        setToolbar();
       
    }
    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantOrderHistoryActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("UserId", PrefUtils.getUser(RestaurantOrderHistoryActivity.this).UserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetOrderHistoryRestaurant, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetOrderUserIdWiseMain submitResponse = new GsonBuilder().create().fromJson(response, GetOrderUserIdWiseMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getOrderUserIdWiseDataArrayList;

                        myCustomAdapter = new MyCustomAdapter(RestaurantOrderHistoryActivity.this, profileListView);
                        profileList.setAdapter(myCustomAdapter);

                        //myCustomAdapter.filter2(3+"");
                        profileList.invalidate();

                    } else {
                        Toast.makeText(RestaurantOrderHistoryActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();



        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) RestaurantOrderHistoryActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetOrderUserIdWiseData> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetOrderUserIdWiseData> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetOrderUserIdWiseData>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtOrderTitle, txtOrderItem,txtOrderAddress,txtDeliveryTime;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.courier_order_list_item, parent, false);
                holder = new ViewHolder();


                holder.txtOrderTitle= (TextView) convertView.findViewById(R.id.txtOrderTitle);
                holder.txtOrderItem= (TextView) convertView.findViewById(R.id.txtOrderItem);
                holder.txtOrderAddress= (TextView) convertView.findViewById(R.id.txtOrderAddress);
                holder.txtDeliveryTime= (TextView) convertView.findViewById(R.id.txtDeliveryTime);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            double price=0;
            for(int i=0;i<profileListView.get(position).getOrderMenuItemsArrayList.size();i++){
                price=price+ Double.parseDouble(profileListView.get(position).getOrderMenuItemsArrayList.get(i).Price);
            }
            String finalString = null;
            try {
                String start_dt = profileListView.get(position).getOrderMenuItemsArrayList.get(0).OrderDate;
                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                Date date = (Date)formatter.parse(start_dt);
                SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yy");
                finalString = newFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.txtOrderTitle.setText(profileListView.get(position).getOrderMenuItemsArrayList.get(0).Name.toString().trim()+" (Ordine #"+profileListView.get(position).OrderId+")");
            holder.txtOrderAddress.setText("Address to delivery, "+profileListView.get(position).getOrderMenuItemsArrayList.get(0).DeliveryAddress.toString().trim());
            holder.txtDeliveryTime.setText(profileListView.get(position).getOrderMenuItemsArrayList.get(0).DeliveryTime.split("-")[1]);
            holder.txtOrderItem.setText(profileListView.get(position).getOrderMenuItemsArrayList.get(0).RestaurantName);

            convertView.setOnClickListener(new View.OnClickListener() {

                String address= holder.txtOrderAddress.getText().toString();
              //  String name= holder.txtOrderTitle.getText().toString();
                String mobileno= holder.txtOrderTitle.getText().toString();
                String email= holder.txtOrderTitle.getText().toString();
                String deliverytime= holder.txtDeliveryTime.getText().toString();
                @Override
                public void onClick(View v) {
                    PrefUtils.setRestaurantOrderHistory(profileListView.get(position), RestaurantOrderHistoryActivity.this);
                    Intent i = new Intent(RestaurantOrderHistoryActivity.this, RestaurantOrderDetailActivity.class);

                    i.putExtra("Name",profileListView.get(position).getOrderMenuItemsArrayList.get(0).Name+"");
                    i.putExtra("Email",profileListView.get(position).getOrderMenuItemsArrayList.get(0).Email+"");
                    i.putExtra("Mobile",profileListView.get(position).getOrderMenuItemsArrayList.get(0).Mobile+"");
                    i.putExtra("Address",address);
                    //i.putExtra("Name",name);
                    i.putExtra("DeliveryTime",deliverytime);

                    startActivity(i);
                }
            });
            return convertView;
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetOrderUserIdWiseData movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.getOrderMenuItemsArrayList.get(0).OrderStatusMasterID.toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }

        public void filter2(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetOrderUserIdWiseData movieDetails : arraylist) {

                    if (charText.length() != 0 && !movieDetails.getOrderMenuItemsArrayList.get(0).OrderStatusMasterID.toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }

    }
   

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Order History");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
