package com.flyingfood.delivery.model.Cart;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 01/07/17.
 */

public class GetOrderUserIdWiseMain {


    @SerializedName("DataList")
    public ArrayList<GetOrderUserIdWiseData> getOrderUserIdWiseDataArrayList;

    @SerializedName("Response")
    public CommonResponse commonResponse;
}
