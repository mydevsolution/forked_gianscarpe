package com.flyingfood.delivery.customerfragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.MainActivity;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerAccountActivity;
import com.flyingfood.delivery.customeractivities.CustomerDiventaUnCorriereActivity;
import com.flyingfood.delivery.customeractivities.CustomerIndirizziSalvatiActivity;
import com.flyingfood.delivery.customeractivities.CustomerInformationAccountActivity;
import com.flyingfood.delivery.customeractivities.CustomerIscriviRistoranteActivity;
import com.flyingfood.delivery.customeractivities.CustomerIscrivitiComeAziendaActivity;
import com.flyingfood.delivery.customeractivities.CustomerPaymentDetailsActivity;
import com.flyingfood.delivery.customeractivities.CustomerRewardActivity;
import com.flyingfood.delivery.customeractivities.CustomerRichiediSupportoActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerAccountFragment extends Fragment {

    LinearLayout llAccount, llPaymentCard, llLocation, llMessageSupport, llFedel, llHouse, llCourier, llCompany;
    private TextView txtAccountName;
    TextView txtLogout;

    public CustomerAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_customer_account, container, false);
        txtAccountName = (TextView) convertView.findViewById(R.id.txtAccountName);
        txtAccountName.setText(PrefUtils.getUser(getActivity()).Username + " Account");

        llAccount = (LinearLayout) convertView.findViewById(R.id.llAccount);
        llPaymentCard = (LinearLayout) convertView.findViewById(R.id.llPaymentCard);
        llLocation = (LinearLayout) convertView.findViewById(R.id.llLocation);
        llMessageSupport = (LinearLayout) convertView.findViewById(R.id.llMessageSupport);
        llFedel = (LinearLayout) convertView.findViewById(R.id.llFedel);
        llHouse = (LinearLayout) convertView.findViewById(R.id.llHouse);
        llCourier = (LinearLayout) convertView.findViewById(R.id.llCourier);
        llCompany = (LinearLayout) convertView.findViewById(R.id.llCompany);

        llLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerIndirizziSalvatiActivity.class);
                startActivity(i);

            }
        });

        llFedel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerRewardActivity.class);
                startActivity(i);

            }
        });
        llPaymentCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerPaymentDetailsActivity.class);
                startActivity(i);

            }
        });


        llMessageSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerRichiediSupportoActivity.class);
                startActivity(i);

            }
        });


        llHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerIscriviRistoranteActivity.class);
                startActivity(i);

            }
        });


        llCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerIscrivitiComeAziendaActivity.class);
                startActivity(i);

            }
        });

        llCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerDiventaUnCorriereActivity.class);
                startActivity(i);

            }
        });


        llAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), CustomerInformationAccountActivity.class);
                startActivity(i);

            }
        });

        
        return convertView;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater. inflate(R.menu.menu_account_logout_button, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_account_logout_button, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnLogOut:

                PrefUtils.clearRandom(getActivity());
                PrefUtils.clearCurrentUser(getActivity());

                LoginManager.getInstance().logOut();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
