package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Account.GetAddress;
import com.flyingfood.delivery.model.Account.GetAddressMain;
import com.flyingfood.delivery.model.MyAddress;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CustomerIndirizziSalvatiActivity extends AppCompatActivity {


    private ImageView imgAddAddress;
    private GridView profileList;
    private CustomerIndirizziSalvatiActivity.MyCustomAdapter myCustomAdapter;
    private ArrayList<GetAddress> profileListView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_indirizzi_salvati);
        imgAddAddress= (ImageView) findViewById(R.id.imgAddAddress);

        imgAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(CustomerIndirizziSalvatiActivity.this,CustomerInserisciIndirizzoActivity.class);
                startActivity(i);
            }
        });
        profileList= (GridView) findViewById(R.id.lvProfiles);


        setToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrdersUserIdWise();
    }

    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerIndirizziSalvatiActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {


                registrationObject.put("ActualAddress", "");
                registrationObject.put("Address", "");
                registrationObject.put("UserID", PrefUtils.getUser(CustomerIndirizziSalvatiActivity.this).UserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GET_ADDRESS, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetAddressMain submitResponse = new GsonBuilder().create().fromJson(response, GetAddressMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getAddressData.getAddressArrayList;

                        myCustomAdapter = new CustomerIndirizziSalvatiActivity.MyCustomAdapter(CustomerIndirizziSalvatiActivity.this, profileListView);
                        profileList.setAdapter(myCustomAdapter);



                    } else {
                        Toast.makeText(CustomerIndirizziSalvatiActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerIndirizziSalvatiActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetAddress> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetAddress> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetAddress>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtName, txtOrderName,txtRestaurantName;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, final ViewGroup parent) {
            final CustomerIndirizziSalvatiActivity.MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customer_indirizzi_salvati_list_item, parent, false);
                holder = new CustomerIndirizziSalvatiActivity.MyCustomAdapter.ViewHolder();


                holder.txtName= (TextView) convertView.findViewById(R.id.txtName);

                convertView.setTag(holder);
            } else {
                holder = (CustomerIndirizziSalvatiActivity.MyCustomAdapter.ViewHolder) convertView.getTag();
            }

holder.txtName.setText(profileListView.get(position).Address);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MyAddress myAddress=new MyAddress();
                    myAddress.addressId=position+"";
                    myAddress.addressName=profileListView.get(position).Address+"";
                    myAddress.actualAddressName=profileListView.get(position).ActualAddress+"";
                    PrefUtils.setAddress(myAddress,CustomerIndirizziSalvatiActivity.this);

                    boolean isCart=getIntent().getBooleanExtra("is_checkout",false);
                    if(isCart){
                        finish();
                    }else{
                        Intent i=new Intent(CustomerIndirizziSalvatiActivity.this,CustomerHomeActivity.class);
                        startActivity(i);
                    }

                }
            });
            return convertView;
        }


    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_indirizzi_salvati, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:

                Intent i = new Intent(CustomerIndirizziSalvatiActivity.this, CustomerInserisciIndirizzoActivity.class);
                startActivity(i);
                break;
                // Toast.makeText(getApplicationContext(),"Item 1 Selected",Toast.LENGTH_LONG).show();



            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Indirizzi salvati");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}
