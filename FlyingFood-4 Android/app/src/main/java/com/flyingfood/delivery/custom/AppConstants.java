package com.flyingfood.delivery.custom;

public class AppConstants {

    public static final String BASE_URL="http://admin.flyingfood.it/Services/";
//    public static final String BASE_URL="http://wcf.webcayon.com/Services/";
//    public static final String BASE_URL2="http://admin.flyingfood.it/Services/";

    //Account.svc
    public static final String LOGIN=BASE_URL+"Account.svc/json/Login";
    public static final String REGISTRATION=BASE_URL+"Account.svc/json/Registation";
    public static final String CHANGE_PASSWORD=BASE_URL+"Account.svc/json/ChangePassword";
    public static final String FORGOT_PASSWORD=BASE_URL+"/Account.svc/json/ForgotPassword";
    public static final String ADD_ADDRESS=BASE_URL+"Account.svc/json/AddAddress";
    public static final String ADD_COMPANY=BASE_URL+"Account.svc/json/AddCompany";
    public static final String ADD_COURIER=BASE_URL+"Account.svc/json/AddCourier";

    public static final String GET_ADDRESS=BASE_URL+"Account.svc/json/GetAddress";
    public static final String GET_COURIER_PERSION=BASE_URL+"Account.svc/json/GetCourierPersion";
    public static final String GET_LAST_ORDER_MESSAGE_CUSOMER=BASE_URL+"Account.svc/json/GetLastOrderMessageCusomer";
    public static final String GET_LAST_ORDER_MESSAGE_LIST_COURIER=BASE_URL+"Account.svc/json/GetLastOrderMessageListCourier";
    public static final String GET_LAST_ORDER_MESSAGE_LIST_RESTAURANT=BASE_URL+"Account.svc/json/GetLastOrderMessageListRestaurant";
    public static final String GET_MESSAGE=BASE_URL+"Account.svc/json/GetMessage";
    public static final String GET_PAYMENT_DETAILS=BASE_URL+"Account.svc/json/GetPaymentDetails";
    public static final String SAVE_PAYMENT=BASE_URL+"Account.svc/json/SavePayment";
    public static final String SEND_MESSAGE=BASE_URL+"Account.svc/json/SendMessage";
    public static final String SEND_MESSAGE_TO_SUPPORT=BASE_URL+"Account.svc/json/SendMsgToSupport";

    //Cart.svc
    public static final String AcceptOrderCourier=BASE_URL+"Cart.svc/json/AcceptOrderCourier";
    public static final String AcceptRestaurantOrder=BASE_URL+"Cart.svc/json/AcceptRestaurantOrder";
    public static final String AddCart=BASE_URL+"Cart.svc/json/AddCart";
    public static final String CompleteOrderCourier=BASE_URL+"Cart.svc/json/CompleteOrderCourier";
    public static final String CompleteRestaurantOrder=BASE_URL+"Cart.svc/json/CompleteRestaurantOrder";
    public static final String DeleteCartItem=BASE_URL+"Cart.svc/json/DeleteCartItem";
    public static final String GetCart=BASE_URL+"Cart.svc/json/GetCart";
    public static final String GetOrderCourier=BASE_URL+"Cart.svc/json/GetOrderCourier";
    public static final String GetOrderHistoryCourier=BASE_URL+"Cart.svc/json/GetOrderHistoryCourier";

    public static final String GetOrderRestaurantUserWise=BASE_URL+"Cart.svc/json/GetOrderRestaurantUserWise";
    public static final String GetOrderUserIDWise=BASE_URL+"Cart.svc/json/GetOrderUserIDWise";
    public static final String GetRestaurantOrderForRestaurant=BASE_URL+"Cart.svc/json/GetRestaurantOrderForRestaurant";
    public static final String PlaceOrder=BASE_URL+"Cart.svc/json/PlaceOrder";
    public static final String CheckCourierAvailability = BASE_URL + "Cart.svc/json/CheckCourierAvailability";

    //Restaurant.svc
    public static final String GetFood=BASE_URL+"Restaurant.svc/json/GetFood";
    public static final String GetFoodRestaurantWise=BASE_URL+"Restaurant.svc/json/GetFoodRestaurantWise";
    public static final String GetRestaurant=BASE_URL+"Restaurant.svc/json/GetRestaurant";
    public static final String GetRestaurantFoodWise=BASE_URL+"Restaurant.svc/json/GetRestaurantFoodWise";
    public static final String GetRestaurantMenu=BASE_URL+"Restaurant.svc/json/GetRestaurantMenu";
    public static final String GET_OPEN_RESTAURANTS=BASE_URL+"Restaurant.svc/json/GetOpenRestaurants";
    public static final String SubscribeRestaurant=BASE_URL+"Restaurant.svc/json/SubscribeRestaurant";
    public static final String SetCloseOrOpenRestaurant=BASE_URL + "Restaurant.svc/json/SetCloseOrOpenRestaurant";
    public static final String SetOpenTimeRestaurant = BASE_URL + "Restaurant.svc/json/SetOpenTimeRestaurant";
    public static final String UpdateMenu = BASE_URL + "Restaurant.svc/json/UpdateMenu";


    //TODO DIPAK

    //Account.svc
    public static final String UPDATE_PROFILE=BASE_URL+"Account.svc/json/UpdateProfile";

    //Cart.svc
    public static final String GetOrderHistoryRestaurant=BASE_URL+"Cart.svc/json/GetOrderHistoryRestaurant";
    public static final String GetOrderRestaurant=BASE_URL+"Cart.svc/json/GetOrderRestaurant";
    public static final String PlaceRestaurantOrder=BASE_URL+"Cart.svc/json/PlaceRestaurantOrder";

    //Restaurant.svc
    public static final String CourierWeeklyAvailable=BASE_URL+"Restaurant.svc/json/CourierWeeklyAvailable";
    public static final String UpdateWaitingTime=BASE_URL+"Restaurant.svc/json/UpdateWaitingTime";
    public static final String DeleteFoodCat=BASE_URL+"Restaurant.svc/json/DeleteFoodCat";
    public static final String DeleteMenu=BASE_URL+"Restaurant.svc/json/DeleteMenu";
    public static final String AddCategory=BASE_URL+"Restaurant.svc/json/AddCategory";
    public static final String AddMenu=BASE_URL+"Restaurant.svc/json/AddMenu";
    public static final String SendCourierRequest=BASE_URL+"Restaurant.svc/json/SendCourierRequest";
}
