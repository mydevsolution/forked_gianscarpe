package com.flyingfood.delivery.model.Account;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 04/07/17.
 */

public class GetAddressData {

    @SerializedName("Data")
    public ArrayList<GetAddress> getAddressArrayList;
}
