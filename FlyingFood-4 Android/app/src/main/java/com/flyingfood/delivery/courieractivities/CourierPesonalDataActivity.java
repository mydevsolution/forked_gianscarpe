package com.flyingfood.delivery.courieractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.flyingfood.delivery.model.LoginResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CourierPesonalDataActivity extends AppCompatActivity {

    Button btnUpdate;
    EditText etName, etEmail, etAddress, etAge, etVehicleType;
    LoginResponse user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier_pesonal_data);

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etAge = (EditText) findViewById(R.id.etAge);
        etVehicleType = (EditText) findViewById(R.id.etVehicleType);
        user=PrefUtils.getUser(CourierPesonalDataActivity.this);
        etName.setText(user.Name);
        etEmail.setText(user.Email);
        etAddress.setText(user.Address);
        etAge.setText(user.Age);
        etVehicleType.setText(user.VehicleType);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etName.getText().toString().length()==0){
                    Toast.makeText(CourierPesonalDataActivity.this, "Please Enter Your Name", Toast.LENGTH_SHORT).show();
                }else if(etEmail.getText().toString().length()==0){
                    Toast.makeText(CourierPesonalDataActivity.this, "Please Enter  Your Email", Toast.LENGTH_SHORT).show();
                }
                else if(etAddress.getText().toString().length()==0){
                    Toast.makeText(CourierPesonalDataActivity.this, "Please Enter  Your Address", Toast.LENGTH_SHORT).show();
                }else if(etAge.getText().toString().length()==0){
                    Toast.makeText(CourierPesonalDataActivity.this, "Please Enter  Your Age", Toast.LENGTH_SHORT).show();
                }else if(etVehicleType.getText().toString().length()==0){
                    Toast.makeText(CourierPesonalDataActivity.this, "Please Enter  Vehicle Type", Toast.LENGTH_SHORT).show();
                }else {
                    doPostOperation();
                }


            }
        });

        setToolbar();
    }

    private void doPostOperation() {

        if (isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CourierPesonalDataActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();


            try {
                registrationObject.put("Address", etAddress.getText().toString().trim());
                registrationObject.put("Age", etAge.getText().toString().trim());
                registrationObject.put("Email", etEmail.getText().toString().trim());
                registrationObject.put("Mobile", "");
                registrationObject.put("Name", etName.getText().toString().trim());
                registrationObject.put("VehicleType", etVehicleType.getText().toString().trim());
                registrationObject.put("UserId", PrefUtils.getUser(CourierPesonalDataActivity.this).UserID + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.UPDATE_PROFILE, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CourierPesonalDataActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        etName.setText(user.Name);
                        etEmail.setText(user.Email);
                        etAddress.setText(user.Address);
                        etAge.setText(user.Age);
                        etVehicleType.setText(user.VehicleType);


                        user.Name=etName.getText().toString();
                        user.Email=etEmail.getText().toString();
                        user.Address=etAddress.getText().toString();
                        user.Age=etAge.getText().toString();
                        user.VehicleType=etVehicleType.getText().toString();
                        PrefUtils.setUser(user,CourierPesonalDataActivity.this);
                        finish();
                    } else {
                        Toast.makeText(CourierPesonalDataActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Personal Data");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
