package com.flyingfood.delivery.customeractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.courieractivities.CourierPesonalDataActivity;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Account.GetLastOrderMEssageCustomerMain;
import com.flyingfood.delivery.model.CommonResponse;
import com.flyingfood.delivery.model.LoginResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerInformationAccountActivity extends AppCompatActivity {

    private EditText etMobile,etName,etEmail;
    LoginResponse user;
//    private LinearLayout llChangePassword;
TextView llChangePassword;
    private Button btnLogin;




    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Informazioni account");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_information_account);
        llChangePassword= (TextView) findViewById(R.id.llChangePassword);
        setToolbar();
        user= PrefUtils.getUser(CustomerInformationAccountActivity.this);
        llChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(CustomerInformationAccountActivity.this,CustomerChangePasswordActivity.class);
                startActivity(i);
            }
        });
        etName= (EditText) findViewById(R.id.etName);
        etMobile= (EditText) findViewById(R.id.etMobile);
        etEmail= (EditText) findViewById(R.id.etEmail);
        btnLogin= (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etName.getText().toString().length()==0){
                    Toast.makeText(CustomerInformationAccountActivity.this, "Please Enter Your Name", Toast.LENGTH_SHORT).show();
                }else if(etEmail.getText().toString().length()==0){
                    Toast.makeText(CustomerInformationAccountActivity.this, "Please Enter  Your Email", Toast.LENGTH_SHORT).show();
                }
                else if(etMobile.getText().toString().length()==0){
                    Toast.makeText(CustomerInformationAccountActivity.this, "Please Enter  Your Mobile", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation(); 
                }
                
              
                
            }
        });
        etName.setText(user.Username);
        etMobile.setText(user.Mobile);
        etEmail.setText(user.Email);


    }

    private void doPostOperation() {

        if (isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerInformationAccountActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();


            try {

                registrationObject.put("Email", etEmail.getText().toString().trim());
                registrationObject.put("Mobile", etMobile.getText().toString().trim());
                registrationObject.put("Name", etName.getText().toString().trim());
                registrationObject.put("UserId", PrefUtils.getUser(CustomerInformationAccountActivity.this).UserID + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.UPDATE_PROFILE, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerInformationAccountActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                        user.Username=etName.getText().toString();
                        user.Mobile=etMobile.getText().toString();
                        user.Email=etEmail.getText().toString();
                        PrefUtils.setUser(user,CustomerInformationAccountActivity.this);
                        finish();
                    } else {
                        Toast.makeText(CustomerInformationAccountActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerInformationAccountActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
