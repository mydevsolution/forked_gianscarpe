package com.flyingfood.delivery.restaurantactivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class RestaurantCourierRequestActivity extends AppCompatActivity {

    private LinearLayout ll7Days,ll8Days,ll11Days,ll60Days,ll61Days,ll62Days,ll63Days,ll156Days,ll189Days;
    private ImageView imag7Check,imag8Check,imag11Check,imag60Check,imag61Check,imag62Check,imag63Check,imag156Check,imag189Check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_courier_request);

        imag7Check = (ImageView) findViewById(R.id.imag7Check);
        imag8Check = (ImageView) findViewById(R.id.imag8Check);
        imag11Check = (ImageView) findViewById(R.id.imag11Check);
        imag60Check = (ImageView) findViewById(R.id.imag60Check);
        imag61Check = (ImageView) findViewById(R.id.imag61Check);
        imag62Check = (ImageView) findViewById(R.id.imag62Check);
        imag63Check = (ImageView) findViewById(R.id.imag63Check);
        imag156Check = (ImageView) findViewById(R.id.imag156Check);
        imag189Check = (ImageView) findViewById(R.id.imag189Check);


        ll7Days = (LinearLayout) findViewById(R.id.ll7Days);
        ll8Days = (LinearLayout) findViewById(R.id.ll8Days);
        ll11Days = (LinearLayout) findViewById(R.id.ll11Days);
        ll60Days = (LinearLayout) findViewById(R.id.ll60Days);
        ll61Days = (LinearLayout) findViewById(R.id.ll61Days);
        ll62Days = (LinearLayout) findViewById(R.id.ll62Days);
        ll63Days = (LinearLayout) findViewById(R.id.ll63Days);
        ll156Days = (LinearLayout) findViewById(R.id.ll156Days);
        ll189Days = (LinearLayout) findViewById(R.id.ll189Days);



        ll7Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imag7Check.setVisibility(View.VISIBLE);
                doPostOperation("7 days");


            }
        });
        ll8Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag8Check.setVisibility(View.VISIBLE);
                doPostOperation("8 days");
            }
        });
        ll11Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag11Check.setVisibility(View.VISIBLE);
                doPostOperation("11 days");

            }
        });
        ll60Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag60Check.setVisibility(View.VISIBLE);
                doPostOperation("60 days");

            }
        });
        ll61Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag61Check.setVisibility(View.VISIBLE);
                doPostOperation("61 days");


            }
        });
        ll62Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imag62Check.setVisibility(View.VISIBLE);
                doPostOperation("62 days");

            }
        });
        ll63Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imag63Check.setVisibility(View.VISIBLE);
                doPostOperation("63 days");

            }
        });
        ll156Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imag156Check.setVisibility(View.VISIBLE);
                doPostOperation("156 days");

            }
        });
        ll189Days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imag189Check.setVisibility(View.VISIBLE);
                doPostOperation("189 days");

            }
        });




        setToolbar();


    }
    private void doPostOperation(String days) {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantCourierRequestActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("AvailableDays", days);
                registrationObject.put("UserId", PrefUtils.getUser(RestaurantCourierRequestActivity.this).UserID+"");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.CourierWeeklyAvailable, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(RestaurantCourierRequestActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(RestaurantCourierRequestActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Courier Request");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}

