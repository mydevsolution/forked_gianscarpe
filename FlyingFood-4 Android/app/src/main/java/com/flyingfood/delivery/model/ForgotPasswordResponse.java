package com.flyingfood.delivery.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 30/06/17.
 */

public class ForgotPasswordResponse {

    @SerializedName("Response")
    public CommonResponse commonResponse;
}
