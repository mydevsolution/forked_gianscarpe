package com.flyingfood.delivery.model.Account;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 04/07/17.
 */

public class GetPaymentDetailsMain {

    @SerializedName("Response")
    public CommonResponse commonResponse;
    @SerializedName("ResponseData")
    public GetPaymentDetailsData getPaymentDetailsData;


}
