package com.flyingfood.delivery.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 22/07/17.
 */

public class TransactionResponse {

    @SerializedName("data")
    public String data;

    @SerializedName("msg")
    public String message;
}
