package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseMain;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import info.hoang8f.android.segmented.SegmentedGroup;

public class CustomerOrdineActivity extends AppCompatActivity {

    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetOrderUserIdWiseData> profileListView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_ordine);
        setToolbar();

        SegmentedGroup segmented3 = (SegmentedGroup) findViewById(R.id.segmentedButtonGroup);
        //Tint color, and text color when checked
        segmented3.setTintColor(Color.parseColor("#ef0c22"), Color.parseColor("#ffffff"));

        segmented3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.btnSignUp:

                        myCustomAdapter.filter2(3 + "");
                        profileList.invalidate();
                        break;
                    case R.id.btnSignIn:
                        myCustomAdapter.filter(3 + "");
                        profileList.invalidate();
                        break;
                    default:
                        // Nothing to do
                }
            }
        });

        profileList = (GridView) findViewById(R.id.lvProfiles);
        getOrdersUserIdWise();
    }

    private void getOrdersUserIdWise() {

        if (isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerOrdineActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("UserID", PrefUtils.getUser(CustomerOrdineActivity.this).UserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetOrderUserIDWise, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("order response...", response + "");
                    GetOrderUserIdWiseMain submitResponse = new GsonBuilder().create().fromJson(response, GetOrderUserIdWiseMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView = submitResponse.getOrderUserIdWiseDataArrayList;

                        myCustomAdapter = new MyCustomAdapter(CustomerOrdineActivity.this, profileListView);
                        profileList.setAdapter(myCustomAdapter);

                        myCustomAdapter.filter2(3 + "");
                        profileList.invalidate();

                    } else {
                        Toast.makeText(CustomerOrdineActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerOrdineActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetOrderUserIdWiseData> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }

        ArrayList<GetOrderUserIdWiseData> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetOrderUserIdWiseData>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtPrice, txtOrderName, txtRestaurantName;


        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customer_in_attesa_list_item, parent, false);
                holder = new ViewHolder();


                holder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
                holder.txtRestaurantName = (TextView) convertView.findViewById(R.id.txtRestaurantName);
                holder.txtOrderName = (TextView) convertView.findViewById(R.id.txtOrderName);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            double price = 0.0;
            for (int i = 0; i < profileListView.get(position).getOrderMenuItemsArrayList.size(); i++) {
                price = price + (Double.parseDouble(profileListView.get(position).getOrderMenuItemsArrayList.get(i).Price)*Integer.parseInt(profileListView.get(position).getOrderMenuItemsArrayList.get(i).Quntity));
       Log.e("price",price+"");
            }
            price=price+3;
            String finalString = null;
            try {
                String start_dt = profileListView.get(position).getOrderMenuItemsArrayList.get(0).OrderDate;
                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                Date date =  formatter.parse(start_dt);
                SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yy");
                finalString = newFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.txtPrice.setText("€" + price + "");
            holder.txtRestaurantName.setText(profileListView.get(position).getOrderMenuItemsArrayList.get(0).RestaurantName.toString().trim());
            holder.txtOrderName.setText("Ordine #" + profileListView.get(position).OrderId + " (" + finalString + ")");


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    PrefUtils.setRestaurantOrderHistory(profileListView.get(position), CustomerOrdineActivity.this);

                    //PrefUtils.setRestaurantOrderHistory(profileListView.get(position));
                    Intent i = new Intent(CustomerOrdineActivity.this, CustomerOrderDetailActivity.class);
                    startActivity(i);

                }
            });
            return convertView;
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetOrderUserIdWiseData movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.getOrderMenuItemsArrayList.get(0).OrderStatusMasterID.toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }

        public void filter2(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetOrderUserIdWiseData movieDetails : arraylist) {

                    if (charText.length() != 0 && !movieDetails.getOrderMenuItemsArrayList.get(0).OrderStatusMasterID.toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }

    }
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Ordine");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}
