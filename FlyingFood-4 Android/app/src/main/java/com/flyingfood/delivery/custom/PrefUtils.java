package com.flyingfood.delivery.custom;

import android.content.Context;

import com.flyingfood.delivery.model.Cart.GetCartDataList;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;
import com.flyingfood.delivery.model.LoginResponse;
import com.flyingfood.delivery.model.MyAddress;
import com.flyingfood.delivery.model.storage.RandomNumberModel;
import com.flyingfood.delivery.model.storage.TokenModel;


public class PrefUtils {

    public static void setUser(LoginResponse currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.putObject("user_value", currentUser);
        complexPreferences.commit();
    }

    public static void clearCurrentUser( Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static LoginResponse getUser(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        LoginResponse currentUser = complexPreferences.getObject("user_value", LoginResponse.class);
        return currentUser;
    }


    public static void setRandom(RandomNumberModel currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "random_prefs", 0);
        complexPreferences.putObject("random_value", currentUser);
        complexPreferences.commit();
    }

    public static void clearRandom( Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "random_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }


    public static RandomNumberModel getRandom(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "random_prefs", 0);
        RandomNumberModel currentUser = complexPreferences.getObject("random_value", RandomNumberModel.class);
        return currentUser;
    }


    public static void setToken(TokenModel currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "token_list_pref", 0);
        complexPreferences.putObject("token_list_pref_value", currentUser);
        complexPreferences.commit();
    }


    public static TokenModel getToken(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "token_list_pref", 0);
        TokenModel currentUser = complexPreferences.getObject("token_list_pref_value", TokenModel.class);
        return currentUser;
    }


    public static void setAccount(TokenModel currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "token_list_pref", 0);
        complexPreferences.putObject("token_list_pref_value", currentUser);
        complexPreferences.commit();
    }


    public static TokenModel getAccount(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "token_list_pref", 0);
        TokenModel currentUser = complexPreferences.getObject("token_list_pref_value", TokenModel.class);
        return currentUser;
    }


//
//
    public static void setCartDataList(GetCartDataList currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "cart_list_prefs", 0);
        complexPreferences.putObject("cart_list_prefs_value", currentUser);
        complexPreferences.commit();
    }

    public static void clearCartDataList( Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "cart_list_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }



    public static GetCartDataList getCartDataList(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "cart_list_prefs", 0);
        GetCartDataList currentUser = complexPreferences.getObject("cart_list_prefs_value", GetCartDataList.class);
        return currentUser;
    }

    public static void setRestaurantOrderHistory(GetOrderUserIdWiseData currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "get_order_list_pref", 0);
        complexPreferences.putObject("get_order_list_pref_value", currentUser);
        complexPreferences.commit();
    }


    public static GetOrderUserIdWiseData getRestaurantOrderHistory(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "get_order_list_pref", 0);
        GetOrderUserIdWiseData currentUser = complexPreferences.getObject("get_order_list_pref_value", GetOrderUserIdWiseData.class);
        return currentUser;
    }


    public static void setAddress(MyAddress currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "address_pref", 0);
        complexPreferences.putObject("address_value", currentUser);
        complexPreferences.commit();
    }


    public static MyAddress getAddress(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "address_pref", 0);
        MyAddress currentUser = complexPreferences.getObject("address_value", MyAddress.class);
        return currentUser;
    }


//
//
//
//
//
//    public static SongList getSongsPlayList(Context ctx){
//        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "songs_playlist_prefs", 0);
//        SongList currentUser = complexPreferences.getObject("songs_playlist_prefs_value", SongList.class);
//        return currentUser;
//    }
//
//
//    public static void setSongsPlayList(SongList currentUser, Context ctx){
//        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "songs_playlist_prefs", 0);
//        complexPreferences.putObject("songs_playlist_prefs_value", currentUser);
//        complexPreferences.commit();
//    }
//
//    public static void clearSongsPlayList( Context ctx){
//        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "songs_playlist_prefs", 0);
//        complexPreferences.clearObject();
//        complexPreferences.commit();
//    }
//
//
//
//
//    public static void setNotificationId(String login, Context ctx){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString("notification", login);
//        editor.apply();
//    }
//
//    public static String getNotificationId(Context ctx){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
//        return preferences.getString("notification", "");
//
//    }
//
//
//
//    public static void setFavourites(FavouriteList currentUser, Context ctx){
//        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "favourite_prefs", 0);
//        complexPreferences.putObject("favourite_prefs_value", currentUser);
//        complexPreferences.commit();
//    }
//
//
//
//    public static FavouriteList getFavorites(Context ctx){
//        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "favourite_prefs", 0);
//        FavouriteList currentUser = complexPreferences.getObject("favourite_prefs_value", FavouriteList.class);
//        return currentUser;
//    }
//
//    public static void setAdCounter(int login, Context ctx){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putInt("advertise", login);
//        editor.apply();
//    }
//
//    public static int getAdCounter(Context ctx) {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
//        return preferences.getInt("advertise", 0);
//
//    }
//

}
