package com.flyingfood.delivery.commonactivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.courieractivities.CourierAccountActivity;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;

import com.flyingfood.delivery.model.Account.GetLastOrderMEssageCustomerMain;
import com.flyingfood.delivery.model.Account.GetLastOrderMessageCustomer;
import com.flyingfood.delivery.restaurantactivities.RestaurantOrderDetailActivity;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CommonChatActivity extends AppCompatActivity {


    private GridView profileList;
    private CommonChatActivity.MyCustomAdapter myCustomAdapter;
    private ArrayList<GetLastOrderMessageCustomer> profileListView;

    String orderId,senderId,receiverId;

    private ImageView imgSend;
    private EditText etMessage;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_chat);

        orderId=getIntent().getStringExtra("order_id");
        senderId=getIntent().getStringExtra("send_id");
        receiverId=getIntent().getStringExtra("rec_id");


        etMessage= (EditText) findViewById(R.id.etMessage);
        imgSend= (ImageView) findViewById(R.id.imgSend);

        imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(etMessage.getText().toString().length()==0){
                   Toast.makeText(CommonChatActivity.this, "Please enter some message", Toast.LENGTH_SHORT).show();
               }else{
                   getOrdersUserIdWise2();
               }
            }
        });


        setToolbar();
        profileList = (GridView)findViewById(R.id.lvProfiles);
        getOrdersUserIdWise();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Chat");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CommonChatActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CommonChatActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {

                registrationObject.put("OrderID", orderId);
                registrationObject.put("ReceiverID",receiverId);
                registrationObject.put("SenderID", senderId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GET_MESSAGE, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetLastOrderMEssageCustomerMain submitResponse = new GsonBuilder().create().fromJson(response, GetLastOrderMEssageCustomerMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getLastOrderMessageCustomerData.getLastOrderMessageCustomerArrayList;

                        myCustomAdapter = new CommonChatActivity.MyCustomAdapter(CommonChatActivity.this, profileListView);
                        profileList.setAdapter(myCustomAdapter);



                    } else {
                        Toast.makeText(CommonChatActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }

    private void getOrdersUserIdWise2() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CommonChatActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {


                registrationObject.put("OrderID", orderId);
                if(getIntent().getBooleanExtra("isFromChat",false)){
                    if(receiverId.equalsIgnoreCase(PrefUtils.getUser(CommonChatActivity.this).UserID)){
                        registrationObject.put("ReceiverID",senderId);
                    }else{
                        registrationObject.put("ReceiverID",receiverId);
                    }

                }else{
                    registrationObject.put("ReceiverID",receiverId);
                }

                registrationObject.put("SenderID", PrefUtils.getUser(CommonChatActivity.this).UserID);
                registrationObject.put("Text", etMessage.getText().toString());
                registrationObject.put("ParentMessageID", "0");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.SEND_MESSAGE, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetLastOrderMEssageCustomerMain submitResponse = new GsonBuilder().create().fromJson(response, GetLastOrderMEssageCustomerMain.class);
                   etMessage.setText("");
                    getOrdersUserIdWise();
                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {


                    } else {
                        Toast.makeText(CommonChatActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }






    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetLastOrderMessageCustomer> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetLastOrderMessageCustomer> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetLastOrderMessageCustomer>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMessage, txtTitle;
            ImageView imgRestaurant;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final CommonChatActivity.MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                if(profileListView.get(position).SenderID.equalsIgnoreCase(PrefUtils.getUser(CommonChatActivity.this).UserID)){
                    convertView = mInflater.inflate(R.layout.item_my_chat, parent, false);
                }else{
                    convertView = mInflater.inflate(R.layout.item_your_chat, parent, false);
                }

                holder = new CommonChatActivity.MyCustomAdapter.ViewHolder();

//
                holder.txtMessage= (TextView) convertView.findViewById(R.id.txtMessage);



                convertView.setTag(holder);
            } else {
                holder = (CommonChatActivity.MyCustomAdapter.ViewHolder) convertView.getTag();
            }

            holder.txtMessage.setText(profileListView.get(position).Text.trim());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return convertView;
        }



    }

    
}
