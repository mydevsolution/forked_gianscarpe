package com.flyingfood.delivery.customerfragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerCartActivity;
import com.flyingfood.delivery.customeractivities.CustomerCartDetailActivity;
import com.flyingfood.delivery.model.Cart.GetCartData;
import com.flyingfood.delivery.model.Cart.GetCartDataList;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerCartFragment extends Fragment {

    private GridView profileList;
    //  double total=0.0;
    // private LinearLayout llCart;
    //TextView txtName, txtTotal,txtRestaurantName;
    private ArrayList<GetCartData> profileListView;
    GetCartDataList submitResponse;
    private MyCustomAdapter myCustomAdapter;
    // CardView ccCart;
    
    public CustomerCartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_customer_cart, container, false);
//        ccCart= (CardView) findViewById(R.id.ccCart);
        profileList = (GridView)convertView.findViewById(R.id.lvProfiles);
//        llCart= (LinearLayout) findViewById(R.id.llCart);
//        llCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                PrefUtils.setCartDataList(submitResponse,getActivity());
//                Intent i=new Intent(getActivity(),CustomerCartDetailActivity.class);
//
//                startActivity(i);
//                finish();
//            }
//        });
//        ccCart.setVisibility(View.GONE);
//        txtName= (TextView) findViewById(R.id.txtName);
//        txtTotal= (TextView) findViewById(R.id.txtTotal);

        return convertView;
    }



    @Override
    public void onResume() {
        super.onResume();
        getOrdersUserIdWise();
    }

    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("CartUniqueID", PrefUtils.getRandom(getActivity()).randomNumber);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetCart, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    submitResponse = new GsonBuilder().create().fromJson(response, GetCartDataList.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getCartDataArrayList;
//                        if(profileListView.size()>0){
//                            ccCart.setVisibility(View.VISIBLE);
//                        }


//                        for(int i=0;i<profileListView.size();i++){
//                            Log.e("restaurant name",profileListView.get(i).RestaurantName);
//                            for(int j=0;j<profileListView.get(i).cartItemArrayList.size();j++) {
//                                Log.e("food name",profileListView.get(i).cartItemArrayList.get(j).MenuName);
//                                Log.e("food qty",profileListView.get(i).cartItemArrayList.get(j).Quntity);
//                                Log.e("food price",profileListView.get(i).cartItemArrayList.get(j).Price);
//                            }
//                        }

//                        for(int i=0;i<profileListView.get(0).cartItemArrayList.size();i++){
//
//                            total=total+ (Double.parseDouble(profileListView.get(0).cartItemArrayList.get(i).Price)*Integer.parseInt(profileListView.get(0).cartItemArrayList.get(i).Quntity));
//                            Log.e("total","€"+total+"");
//                        }
//                        txtTotal.setText("TOTAL: "+"€"+total);

                        myCustomAdapter = new MyCustomAdapter(getActivity(), profileListView);
                        profileList.setAdapter(myCustomAdapter);

                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetCartData> profileListView;
        Context context;
        int myPosition=0;
        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetCartData> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetCartData>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtName, txtTotal,txtRestaurantName;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_cart_view, parent, false);
                holder = new MyCustomAdapter.ViewHolder();
                holder.txtName= (TextView) convertView.findViewById(R.id.txtName);
                holder.txtTotal= (TextView) convertView.findViewById(R.id.txtTotal);

                for(int i=0;i<profileListView.get(position).cartItemArrayList.size();i++){

                    profileListView.get(position).TotalAmount=profileListView.get(position).TotalAmount+(Integer.parseInt(profileListView.get(position).cartItemArrayList.get(i).Quntity)*Double.parseDouble(profileListView.get(position).cartItemArrayList.get(i).Price));

                }
                convertView.setTag(holder);
            } else {
                holder = (MyCustomAdapter.ViewHolder) convertView.getTag();
            }
            holder.txtName.setText(profileListView.get(position).RestaurantName);


            Log.e("size",profileListView.get(position).cartItemArrayList.size()+"");


            holder.txtTotal.setText("€"+profileListView.get(position).TotalAmount+"");
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myPosition=position;
                    PrefUtils.setCartDataList(submitResponse,getActivity());
                    Intent i=new Intent(getActivity(),CustomerCartDetailActivity.class);

                    i.putExtra("position",myPosition);

                    startActivity(i);

                }
            });
            return convertView;
        }



    }
    
    

}
