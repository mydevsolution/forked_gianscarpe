package com.flyingfood.delivery.model.Cart;

import android.os.Parcel;
import android.os.Parcelable;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by nirav on 04/07/17.
 */

public class GetCartDataList implements Serializable {

    @SerializedName("CartUniqueID")
    public String CartUniqueID;

    @SerializedName("DataList")
    public ArrayList<GetCartData> getCartDataArrayList;

    @SerializedName("Response")
    public CommonResponse commonResponse;



}
