package com.flyingfood.delivery.customeractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerIscriviRistoranteActivity extends AppCompatActivity {

    private Button btnLogin;
    
    private EditText etMessage,etRestaurant,etMobile,etName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_iscrivi_ristorante);

        etName= (EditText) findViewById(R.id.etName);
        etMobile= (EditText) findViewById(R.id.etMobile);
        etRestaurant= (EditText) findViewById(R.id.etRestaurant);
        etMessage= (EditText) findViewById(R.id.etMessage);

        btnLogin= (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etName.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscriviRistoranteActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
                }else  if(etMobile.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscriviRistoranteActivity.this, "Please Enter Mobile", Toast.LENGTH_SHORT).show();
                }else  if(etRestaurant.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscriviRistoranteActivity.this, "Please Enter Restaurant Name", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation();
                }

            }
        });

        setToolbar();
    }


    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerIscriviRistoranteActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();


            try {
                registrationObject.put("Name", etName.getText().toString().trim()+"");
                registrationObject.put("Phone", etMobile.getText().toString().trim());
                registrationObject.put("RestaurantName", etRestaurant.getText().toString().trim());
                registrationObject.put("UserID", PrefUtils.getUser(CustomerIscriviRistoranteActivity.this).UserID);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.SubscribeRestaurant, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerIscriviRistoranteActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CustomerIscriviRistoranteActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Iscrivi ristorante");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}
