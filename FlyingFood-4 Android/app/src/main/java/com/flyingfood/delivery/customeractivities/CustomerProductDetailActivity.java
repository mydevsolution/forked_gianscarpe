package com.flyingfood.delivery.customeractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.AddCartMain;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerProductDetailActivity extends AppCompatActivity {

    private EditText etDescription;
    private Button btnLogin;
    private TextView txtQty,txPlus,txtMinus;
    int qty=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_product_details);
        setToolbar();
        etDescription= (EditText) findViewById(R.id.etDescription);
        btnLogin= (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doPostOperation();
            }
        });

        txtQty= (TextView) findViewById(R.id.txtQty);
        txPlus= (TextView) findViewById(R.id.txtPlus);
        txtMinus= (TextView) findViewById(R.id.txtMinus);




        txPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(qty>=0){
                    qty=qty+1;
                    txtQty.setText(qty+" Pezzo");
                }
            }
        });

        txtMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(qty<=1)){
                    qty=qty-1;
                    txtQty.setText(qty+" Pezzo");
                }
            }
        });

    }
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle(getIntent().getStringExtra("menu_name"));
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerProductDetailActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {



                RadioGroup rg = (RadioGroup)findViewById(R.id.rgCart);
                String radiovalue = ((RadioButton)findViewById(rg.getCheckedRadioButtonId())).getText().toString();

                registrationObject.put("CartMasterID", "0");
                registrationObject.put("CartUniqueID",PrefUtils.getRandom(CustomerProductDetailActivity.this).randomNumber);
                registrationObject.put("Description", etDescription.getText().toString().trim());
                registrationObject.put("FoodId", getIntent().getStringExtra("food_id")+"");
                registrationObject.put("ItemSold", radiovalue);
                registrationObject.put("MenuId", getIntent().getStringExtra("menu_id"));
                registrationObject.put("Price",  getIntent().getStringExtra("price")+"");

                registrationObject.put("Quntity",  qty+"");
                registrationObject.put("RestaurantId",  getIntent().getStringExtra("res_id"));
                registrationObject.put("UserID", PrefUtils.getUser(CustomerProductDetailActivity.this).UserID+"");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("cart request object", registrationObject + "");

            new PostServiceCall(AppConstants.AddCart, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    AddCartMain submitResponse = new GsonBuilder().create().fromJson(response, AddCartMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerProductDetailActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CustomerProductDetailActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

}
