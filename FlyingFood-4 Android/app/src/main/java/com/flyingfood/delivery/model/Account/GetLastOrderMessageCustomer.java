package com.flyingfood.delivery.model.Account;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 01/07/17.
 */

public class GetLastOrderMessageCustomer {


    @SerializedName("CourierId")
    public String CourierId;

    @SerializedName("CourierName")
    public String CourierName;

    @SerializedName("Email")
    public String Email;

    @SerializedName("Mobile")
    public String Mobile;

    @SerializedName("Name")
    public String Name;

    @SerializedName("OrderId")
    public String OrderId;

    @SerializedName("ReceiverID")
    public String ReceiverID;

    @SerializedName("RestaurantId")
    public String RestaurantId;

    @SerializedName("RestaurantImage")
    public String RestaurantImage;

    @SerializedName("RestaurantName")
    public String RestaurantName;

    @SerializedName("SenderID")
    public String SenderID;

    @SerializedName("Text")
    public String Text;

    @SerializedName("UserID")
    public String UserID;

    @SerializedName("Username")
    public String Username;


}
