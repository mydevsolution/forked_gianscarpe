package com.flyingfood.delivery.model.Restaurant;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 01/07/17.
 */

public class GetRestaurantFoodWiseMain {

    @SerializedName("Response")
    public CommonResponse commonResponse;

    @SerializedName("ResponseData")
    public GetFoodRestaurantWiseData foodRestaurantWiseData;
}
