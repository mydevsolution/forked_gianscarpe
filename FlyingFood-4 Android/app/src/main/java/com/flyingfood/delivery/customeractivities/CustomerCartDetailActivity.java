package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.CartItem;
import com.flyingfood.delivery.model.Cart.GetCartDataList;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.JsonAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CustomerCartDetailActivity extends AppCompatActivity {

    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<CartItem> profileListView;
    GetCartDataList submitResponse;
    double total=0.0;
    private TextView txtCharges,txtTotal,txtFinal,txtAddProduct;
    double finalTotal=0.0;
    private LinearLayout llCheckOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_check_out);
        setToolbar();
        txtAddProduct= (TextView) findViewById(R.id.txtAddProduct);
        txtAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(CustomerCartDetailActivity.this,CustomerBurgerHouseActivity.class);

                i.putExtra("food_id",submitResponse.getCartDataArrayList.get(getIntent().getIntExtra("position",0)).cartItemArrayList.get(0).FoodId);
                i.putExtra("res_id",submitResponse.getCartDataArrayList.get(getIntent().getIntExtra("position",0)).RestaurantId);
                startActivity(i);
                finish();

            }
        });

        profileList= (GridView) findViewById(R.id.lvProfiles);
        submitResponse= PrefUtils.getCartDataList(CustomerCartDetailActivity.this);
        txtTotal= (TextView) findViewById(R.id.txtTotal);
        txtCharges= (TextView) findViewById(R.id.txtCharges);
        txtFinal= (TextView) findViewById(R.id.txtFinal);
        llCheckOut= (LinearLayout) findViewById(R.id.llCheckOut);


        profileListView = submitResponse.getCartDataArrayList.get(getIntent().getIntExtra("position",0)).cartItemArrayList;

        llCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(CustomerCartDetailActivity.this,CustomerCartDetailFinalActivity.class);
                int position = getIntent().getIntExtra("position",0);
                i.putExtra("position", position );
                startActivity(i);
                finish();
            }
        });

        setTotal();


        myCustomAdapter = new MyCustomAdapter(CustomerCartDetailActivity.this, profileListView);
        profileList.setAdapter(myCustomAdapter);
        
    }

    private void setTotal(){
        for(int i=0;i<profileListView.size();i++){
            total=total+   (Double.parseDouble(profileListView.get(i).Price)*Integer.parseInt(profileListView.get(i).Quntity));

        }

       // txtTotal.setText(total+"");
      //  txtOrderRuppes.setText(finalTotal + ""+"€")
        txtTotal.setText("€"+total+"");
        txtCharges.setText("€"+"3.0");
        finalTotal=total+3.0;
        txtFinal.setText("€"+finalTotal+"");
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("CheckOut");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }


    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<CartItem> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }

        ArrayList<CartItem> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;

        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtPrice, txtTitle;
            ImageView imgCancel;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_cart_product, parent, false);
                holder = new ViewHolder();


                holder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
                holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
                holder.imgCancel = (ImageView) convertView.findViewById(R.id.imgCancel);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txtPrice.setText("€"+(Double.parseDouble(profileListView.get(position).Price)*Integer.parseInt(profileListView.get(position).Quntity))+"");
            holder.txtTitle.setText((position+1)+" "+profileListView.get(position).MenuName.toString().trim()+" (Quantity "+profileListView.get(position).Quntity+")");


            holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //Call Delete API

                    doPostOperation(profileListView.get(position).CartMasterID);
                }
            });


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    Intent i = new Intent(CustomerOrdineActivity.this, CustomerOrderDetailActivity.class);
//                    startActivity(i);

                }
            });
            return convertView;
        }




    }



    private void doPostOperation(String id) {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerCartDetailActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("CartMasterId", id);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.DeleteCartItem, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerCartDetailActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CustomerCartDetailActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



}
