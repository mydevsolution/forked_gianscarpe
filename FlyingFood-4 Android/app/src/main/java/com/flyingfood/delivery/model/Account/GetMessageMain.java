package com.flyingfood.delivery.model.Account;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 03/07/17.
 */

public class GetMessageMain {

    @SerializedName("Response")
    public CommonResponse commonResponse;

    @SerializedName("ResponseData")
    public GetMessageData getMessageData;
}
