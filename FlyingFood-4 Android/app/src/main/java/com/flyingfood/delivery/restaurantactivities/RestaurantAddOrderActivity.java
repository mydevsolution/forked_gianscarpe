package com.flyingfood.delivery.restaurantactivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ArrayAdapter;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.adapter.PlaceAutocompleteAdapter;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.flyingfood.delivery.model.Restaurant.RestaurantAddOrderResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RestaurantAddOrderActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private Button btnFatto;
    private ImageView imgBack;
    private EditText etNomeECognome,etNumeroDi;

    protected GoogleApiClient mGoogleApiClient;

    private PlaceAutocompleteAdapter mAdapter;

    private AutoCompleteTextView mAutocompleteView;

    private EditText etActualadress;
    private TextView tvCourierPrice;
    private Spinner spOrariConsegna;
    int hour;
    int minute;
    private String currentCourierFee;
    private String currentDeliveryTime;

    private  String[] arrayItems;
    private String[] orariDiConsegna;
    final String[] actualValues = {"3.0","2.0","1.0"};
    final int[] minutesWaiting = {20, 30, 40};
    List<String> options = new ArrayList<String>();
    ArrayAdapter<String> SpinerAdapter;
    List<String> orari = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_add_order);

        btnFatto = (Button) findViewById(R.id.btnFatto);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        etNomeECognome = (EditText) findViewById(R.id.etNomeECognome);
        etNumeroDi = (EditText) findViewById(R.id.etNumeroDi);
        etActualadress = (EditText) findViewById(R.id.etActualadress);
        spOrariConsegna = (Spinner) findViewById(R.id.spOraDiConsegna);





//                        DateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
//                        if (df.parse(selected_time).before(df.parse(current_time))) {
//                            Toast.makeText(getApplicationContext(), "Please select future time",Toast.LENGTH_LONG).show();
//                        }else if (df.parse(selected_time).equals(df.parse(current_time))) {
//                            Toast.makeText(getApplicationContext(), "Please select future time",Toast.LENGTH_LONG).show();
//                        }




        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //    Intent i = new Intent(ProvidersNameActivity.this, LoginActivity.class);
                //    startActivity(i);
                //   finish();

            }
        });
        btnFatto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doPostOperation();

            }
        });

        setToolbar();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        mAutocompleteView = (AutoCompleteTextView)
                findViewById(R.id.autocomplete_customer_address);


        etActualadress= (EditText) findViewById(R.id.etOpzioniAppartamento);


        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);


        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, BOUNDS_GREATER_SYDNEY,
                null);





        mAutocompleteView.setAdapter(mAdapter);


    }

    private void checkOrariConsegna()
    {
        long ONE_MINUTE_IN_MILLIS=60000;//millisecs


        int i = 0;
        for(int m : minutesWaiting)
        {
            long time = Calendar.getInstance().getTimeInMillis() + ONE_MINUTE_IN_MILLIS * m;
            Date date = new Date(time);
            DateFormat formatter = new SimpleDateFormat("h:mm a", Locale.US);
            String dateFormatted = formatter.format(date);
            String optionString = "Corriere disponibile alle " + dateFormatted + " ; costo: € " + actualValues[i];
            checkCourierAvailability(dateFormatted, optionString);
            i++;

        }
        orariDiConsegna = orari.toArray(new String[0]);


        spOrariConsegna.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                currentCourierFee = actualValues[arg2];
                currentDeliveryTime = orariDiConsegna[arg2];

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });





    }

    public void updateAdapter()
    {
        arrayItems = options.toArray(new String[0]);
        orariDiConsegna = orari.toArray(new String[0]);
        SpinerAdapter = new CustomArrayAdapter<>(this,
                 arrayItems);
        SpinerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spOrariConsegna.setAdapter(SpinerAdapter);
    }





    private Boolean checkCourierAvailability(final String datetime, final String option) {


        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantAddOrderActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("DeliveryAddress", mAutocompleteView.getText().toString().trim());
                registrationObject.put("DeliveryTime", datetime);
                registrationObject.put("RestaurantId", PrefUtils.getUser(RestaurantAddOrderActivity.this).RestaurantId+"");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e(" request object", registrationObject + "");


            PostServiceCall serviceCall = new PostServiceCall(AppConstants.CheckCourierAvailability, registrationObject) {
                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        options.add(option);
                        orari.add(datetime);
                        updateAdapter();

                    }
                    Log.e("login response...", response + "");


                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            };
                serviceCall.call();
                return true;





        }
        else
            return  false;
    }



    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i("error", "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

//            Toast.makeText(getApplicationContext(), "Clicked: " + primaryText,
//                    Toast.LENGTH_SHORT).show();
            Log.i("error", "Called getPlaceById to get Place details for " + placeId);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e("error", "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // Format details of the place for display and show it in a TextView.
//            mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
//                    place.getId(), place.getAddress(), place.getPhoneNumber(),
//                    place.getWebsiteUri())+" "+place.getLatLng());

            // Display the third party attributions if set.
            final CharSequence thirdPartyAttribution = places.getAttributions();
//            if (thirdPartyAttribution == null) {
//                mPlaceDetailsAttribution.setVisibility(View.GONE);
//            } else {
//                mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
//                mPlaceDetailsAttribution.setText(Html.fromHtml(thirdPartyAttribution.toString()));
//            }

            Log.i("error", "Place details received: " + place.getName());

            places.release();
            checkOrariConsegna();
        }
    };


    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantAddOrderActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("CartUniqueID", "");
                registrationObject.put("DeliveryAddress", mAutocompleteView.getText().toString().trim());
                registrationObject.put("DeliveryTime", currentDeliveryTime);
                registrationObject.put("Name", etNomeECognome.getText().toString().trim());
                registrationObject.put("Phone", etNumeroDi.getText().toString().trim());
                registrationObject.put("UserId", PrefUtils.getUser(RestaurantAddOrderActivity.this).UserID+"");
                registrationObject.put("RestaurantId", PrefUtils.getUser(RestaurantAddOrderActivity.this).RestaurantId+"");
                registrationObject.put("Description", etActualadress.getText().toString().trim());
                registrationObject.put("CourierFee", currentCourierFee + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e(" request object", registrationObject + "");

            new PostServiceCall(AppConstants.PlaceRestaurantOrder, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    RestaurantAddOrderResponse submitResponse = new GsonBuilder().create().fromJson(response, RestaurantAddOrderResponse.class);

                    if (submitResponse.getCommonResponse().ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(RestaurantAddOrderActivity.this, submitResponse.getCommonResponse().ResponseCode, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(RestaurantAddOrderActivity.this, submitResponse.getCommonResponse().ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Add Order");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.e("error", "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    static class CustomArrayAdapter<T> extends ArrayAdapter<T>
    {
        public CustomArrayAdapter(Context ctx, T [] objects)
        {
            super(ctx, android.R.layout.simple_spinner_item, objects);
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View view = super.getView(position, convertView, parent);

            TextView text = (TextView)view.findViewById(android.R.id.text1);
            text.setTextColor(Color.WHITE);


            return view;

        }
    }

}



