package com.flyingfood.delivery.customeractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerChangePasswordActivity extends AppCompatActivity {

    private EditText etConfirmPassword,etNewPassword,etOldPassword;
    private Button btnChangePass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_change_password);
        setToolbar();

        etOldPassword= (EditText) findViewById(R.id.etOldPassword);
        etNewPassword= (EditText) findViewById(R.id.etNewPassword);
        etConfirmPassword= (EditText) findViewById(R.id.etConfirmPassword);

        btnChangePass= (Button) findViewById(R.id.btnChangePass);

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etOldPassword.getText().toString().length()==0){
                    Toast.makeText(CustomerChangePasswordActivity.this, "Please Enter Old Password", Toast.LENGTH_SHORT).show();
                }else if(etNewPassword.getText().toString().length()==0){
                    Toast.makeText(CustomerChangePasswordActivity.this, "Please Enter New Password", Toast.LENGTH_SHORT).show();
                }else if(etConfirmPassword.getText().toString().length()==0){
                    Toast.makeText(CustomerChangePasswordActivity.this, "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
                }else if(!etNewPassword.getText().toString().equalsIgnoreCase(etConfirmPassword.getText().toString())){
                    Toast.makeText(CustomerChangePasswordActivity.this, "New & Confirmed Password must be match", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation();
                }
            }
        });
        

    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Change Your Password");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }


    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerChangePasswordActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("CurrentPassword", etOldPassword.getText().toString().trim());
                registrationObject.put("NewPassword", etNewPassword.getText().toString().trim());
                registrationObject.put("UserId", PrefUtils.getUser(CustomerChangePasswordActivity.this).UserID+"");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.CHANGE_PASSWORD, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerChangePasswordActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CustomerChangePasswordActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    

    
}
