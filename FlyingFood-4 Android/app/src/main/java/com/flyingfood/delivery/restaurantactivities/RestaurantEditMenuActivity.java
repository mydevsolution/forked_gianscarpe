package com.flyingfood.delivery.restaurantactivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class RestaurantEditMenuActivity extends AppCompatActivity {


    private Button btnDone;
    private EditText etName,etDes,etPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_edit_menu);

        btnDone = (Button) findViewById(R.id.btnUpdateDone);

        etName = (EditText) findViewById(R.id.etNewName);

        etDes = (EditText) findViewById(R.id.etNewDescription);

        etPrice = (EditText) findViewById(R.id.etNewPrice);

        String price, description, name;

        price = getIntent().getStringExtra("price");

        description = getIntent().getStringExtra("description");

        name = getIntent().getStringExtra("name");

        etName.setText(name);
        etDes.setText(description);
        etPrice.setText(price);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etName.getText().toString().length()==0){
                    Toast.makeText(RestaurantEditMenuActivity.this, "Please enter name", Toast.LENGTH_SHORT).show();
                }else if(etDes.getText().toString().length()==0){
                    Toast.makeText(RestaurantEditMenuActivity.this, "Please enter Description", Toast.LENGTH_SHORT).show();
                }else if(etPrice.getText().toString().length()==0){
                    Toast.makeText(RestaurantEditMenuActivity.this, "Please enter Price", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation();
                }
            }
        });
        setToolbar();
    }

    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantEditMenuActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("MenuId", getIntent().getStringExtra("menu_id"));
                registrationObject.put("FoodId", getIntent().getStringExtra("food_id"));
                registrationObject.put("MenuDescription", etDes.getText().toString().trim());
                registrationObject.put("MenuName", etName.getText().toString().trim()+"");
                registrationObject.put("Price", etPrice.getText().toString().trim()+"");
                registrationObject.put("RestaurantId", PrefUtils.getUser(RestaurantEditMenuActivity.this).RestaurantId+"");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.UpdateMenu, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(RestaurantEditMenuActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(RestaurantEditMenuActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("food add response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Aggiorna prodotto del menu");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }



}
