package com.flyingfood.delivery.restaurantactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.flyingfood.delivery.R;

public class RestaurantSetCloseActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    EditText et_motivation;
    Switch sw_open_close;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_set_close);

        et_motivation = (EditText) findViewById(R.id.et_CloseMotivation);
        sw_open_close = (Switch) findViewById(R.id.sw_open_close);


        sw_open_close.setOnCheckedChangeListener(this);

        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Stato ristorante");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Toast.makeText(this, "Ristorante chiuso",
                Toast.LENGTH_SHORT).show();
        if(isChecked) {
            Toast.makeText(this, "Ristorante Aperto",
                    Toast.LENGTH_SHORT).show();
        } else {
            //do stuff when Switch if OFF
        }
    }

}
