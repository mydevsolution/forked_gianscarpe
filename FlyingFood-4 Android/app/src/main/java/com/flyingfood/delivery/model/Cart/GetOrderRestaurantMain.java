package com.flyingfood.delivery.model.Cart;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 07/07/17.
 */

public class GetOrderRestaurantMain {

    @SerializedName("Response")
    public CommonResponse commonResponse;

    @SerializedName("ResponseData")
    public GetOrderRestaurantData getOrderRestaurantData;
}
