package com.flyingfood.delivery.courieractivities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.GetOrderMenuItems;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;

import java.util.ArrayList;

public class CourierOrderDetailMenuItemActivity extends AppCompatActivity {

    private GridView profileList;
    private GetOrderUserIdWiseData getOrderUserIdWiseData;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetOrderMenuItems> profileListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier_order_detail_menu_item);

        getOrderUserIdWiseData = PrefUtils.getRestaurantOrderHistory(CourierOrderDetailMenuItemActivity.this);
        profileList = (GridView) findViewById(R.id.lvProfiles);
        profileListView = getOrderUserIdWiseData.getOrderMenuItemsArrayList;

        myCustomAdapter = new MyCustomAdapter(CourierOrderDetailMenuItemActivity.this, profileListView);
        profileList.setAdapter(myCustomAdapter);
        setToolbar();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Menu Item");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetOrderMenuItems> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMenuOrder, txtMenuPrice, txt2, txt3, txt4, txt5, txt6,txtMenuOrderDes;
            ImageView imgProfile;
            LinearLayout llFeed, llOption1, llOption2, llOption3, llOption4, llOption5, llOption6;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_order_detail_list_item, parent, false);
                holder = new ViewHolder();

                holder.txtMenuOrder = (TextView) convertView.findViewById(R.id.txtMenuOrder);
                holder.txtMenuOrderDes = (TextView) convertView.findViewById(R.id.txtMenuOrderDes);
                holder.txtMenuPrice = (TextView) convertView.findViewById(R.id.txtMenuPrice);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();


            }
            holder.txtMenuOrderDes.setText(profileListView.get(position).Description);
            holder.txtMenuOrder.setText(profileListView.get(position).MenuName + " (quantity " + profileListView.get(position).Quntity + ")");
            holder.txtMenuPrice.setText("€"+(Double.parseDouble(profileListView.get(position).Price)*Integer.parseInt(profileListView.get(position).Quntity)));


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return convertView;
        }
    }
}
