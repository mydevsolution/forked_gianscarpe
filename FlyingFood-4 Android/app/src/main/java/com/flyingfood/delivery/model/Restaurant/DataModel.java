package com.flyingfood.delivery.model.Restaurant;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr. DIPAK on 25-Jul-17.
 */

public class DataModel {


    @SerializedName("DateGenerated")
    public String DateGenerated;
    @SerializedName("DeliveryAddress")
    public String DeliveryAddress;
    @SerializedName("DeliveryTime")
    public String DeliveryTime;
    @SerializedName("Name")
    public String Name;
    @SerializedName("OrderId")
    public String OrderId;
    @SerializedName("OrderStatusId")
    public String OrderStatusId;
    @SerializedName("Phone")
    public String Phone;
    @SerializedName("RestaurantId")
    public String RestaurantId;
    @SerializedName("Response")
    public String Response;

    @SerializedName("MenuName")
    public String MenuName;


    @SerializedName("RestaurantName")
    public String RestaurantName;

    public String getDateGenerated() {
        return DateGenerated;
    }

    public void setDateGenerated(String dateGenerated) {
        DateGenerated = dateGenerated;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getDeliveryTime() {
        return DeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        DeliveryTime = deliveryTime;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getOrderStatusId() {
        return OrderStatusId;
    }

    public void setOrderStatusId(String orderStatusId) {
        OrderStatusId = orderStatusId;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getRestaurantId() {
        return RestaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        RestaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return RestaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        RestaurantName = restaurantName;
    }
}
