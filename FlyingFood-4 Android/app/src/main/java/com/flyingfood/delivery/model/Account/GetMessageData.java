package com.flyingfood.delivery.model.Account;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 03/07/17.
 */

public class GetMessageData {


    @SerializedName("Data")
    public ArrayList<GetMessage> getMessageArrayList;
}
