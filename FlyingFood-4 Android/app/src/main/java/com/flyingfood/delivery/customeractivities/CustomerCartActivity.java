package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.GetCartData;
import com.flyingfood.delivery.model.Cart.GetCartData;
import com.flyingfood.delivery.model.Cart.GetCartDataList;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseMain;
import com.flyingfood.delivery.restaurantactivities.RestaurantHomeActivity;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CustomerCartActivity extends AppCompatActivity {

    private GridView profileList;

    private ArrayList<GetCartData> profileListView;
    GetCartDataList submitResponse;
    private MyCustomAdapter myCustomAdapter;



    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Cart");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_cart);
        setToolbar();

        profileList = (GridView)findViewById(R.id.lvProfiles);


    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrdersUserIdWise();
    }

    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerCartActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("CartUniqueID", PrefUtils.getRandom(CustomerCartActivity.this).randomNumber);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetCart, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    submitResponse = new GsonBuilder().create().fromJson(response, GetCartDataList.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getCartDataArrayList;
//                        if(profileListView.size()>0){
//                            ccCart.setVisibility(View.VISIBLE);
//                        }


//                        for(int i=0;i<profileListView.size();i++){
//                            Log.e("restaurant name",profileListView.get(i).RestaurantName);
//                            for(int j=0;j<profileListView.get(i).cartItemArrayList.size();j++) {
//                                Log.e("food name",profileListView.get(i).cartItemArrayList.get(j).MenuName);
//                                Log.e("food qty",profileListView.get(i).cartItemArrayList.get(j).Quntity);
//                                Log.e("food price",profileListView.get(i).cartItemArrayList.get(j).Price);
//                            }
//                        }

//                        for(int i=0;i<profileListView.get(0).cartItemArrayList.size();i++){
//
//                            total=total+ (Double.parseDouble(profileListView.get(0).cartItemArrayList.get(i).Price)*Integer.parseInt(profileListView.get(0).cartItemArrayList.get(i).Quntity));
//                            Log.e("total","€"+total+"");
//                        }
//                        txtTotal.setText("TOTAL: "+"€"+total);

                        myCustomAdapter = new MyCustomAdapter(CustomerCartActivity.this, profileListView);
                        profileList.setAdapter(myCustomAdapter);

                    } else {
                        Toast.makeText(CustomerCartActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerCartActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetCartData> profileListView;
        Context context;


        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetCartData> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetCartData>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtName, txtTotal,txtRestaurantName;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView( int position, View convertView, ViewGroup parent) {
            final CustomerCartActivity.MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_cart_view, parent, false);
                holder = new CustomerCartActivity.MyCustomAdapter.ViewHolder();
                holder.txtName= (TextView) convertView.findViewById(R.id.txtName);
                holder.txtTotal= (TextView) convertView.findViewById(R.id.txtTotal);

                for(int i=0;i<profileListView.get(position).cartItemArrayList.size();i++){

                    profileListView.get(position).TotalAmount=profileListView.get(position).TotalAmount+(Integer.parseInt(profileListView.get(position).cartItemArrayList.get(i).Quntity)*Double.parseDouble(profileListView.get(position).cartItemArrayList.get(i).Price));

                }
                convertView.setTag(holder);
            } else {
                holder = (CustomerCartActivity.MyCustomAdapter.ViewHolder) convertView.getTag();
            }
            holder.txtName.setText(profileListView.get(position).RestaurantName);


            Log.e("size",profileListView.get(position).cartItemArrayList.size()+"");


            holder.txtTotal.setText("€"+profileListView.get(position).TotalAmount+"");
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                PrefUtils.setCartDataList(submitResponse,CustomerCartActivity.this);
//                Intent i=new Intent(CustomerCartActivity.this,CustomerCartDetailActivity.class);
//
//                    i.putExtra("position",myPosition);
//                    Log.e("pos value",myPosition+"");
//                    Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
//                startActivity(i);
//                finish();
                }
            });
            return convertView;
        }



    }

}
