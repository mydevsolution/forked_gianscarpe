package com.flyingfood.delivery.model.storage;

/**
 * Created by nirav on 06/07/17.
 */

public class TokenModel {

    public String token;

    public TokenModel(String token) {
        this.token = token;
    }
}
