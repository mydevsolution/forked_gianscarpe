package com.flyingfood.delivery.restaurantactivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.flyingfood.delivery.model.Restaurant.GetFood;
import com.flyingfood.delivery.model.Restaurant.GetRFoodListMain;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class RestaurantMenuActivity extends AppCompatActivity {

    boolean isVisible = false;

    //   private LinearLayout llBurgerHouse,llClassic,llBevandeBurgerHouse,llPatatineBurgerHouse,llNewBurgerHouse,llSpecialBurgerHouse;
    //  private ImageView btnDeleteFoodcat,btnAddFoodCat,imgDeleteSpecial,imgDeleteNew,imgDeletePatatine,imgDeleteBevande,imgDeleteClassic,imgDeleteBurger;
    private ImageView btnDeleteFoodcat, btnAddFoodCat,imgDelete;
    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetFood> profileListView;
    boolean isSelected=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_menu);

        profileList = (GridView) findViewById(R.id.profileList);

        btnAddFoodCat = (ImageView) findViewById(R.id.btnAddFoodCat);
        btnDeleteFoodcat = (ImageView) findViewById(R.id.btnDeleteFoodcat);
        btnDeleteFoodcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("yes","click");

                if(isSelected==true){
                    isSelected=false;
                    myCustomAdapter = new RestaurantMenuActivity.MyCustomAdapter(RestaurantMenuActivity.this, profileListView,isSelected);
                    profileList.setAdapter(myCustomAdapter);


                }else{
                    Log.e("yes","true");
                    isSelected=true;
                    myCustomAdapter = new RestaurantMenuActivity.MyCustomAdapter(RestaurantMenuActivity.this, profileListView,isSelected);
                    profileList.setAdapter(myCustomAdapter);


                }
            }
        });

        btnAddFoodCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RestaurantMenuActivity.this, RestaurantAddingFoodcatActivity.class);
                startActivity(intent);

            }
        });

        setToolbar();

    }


    @Override
    protected void onResume() {
        super.onResume();
        GetFood();
    }

    private void GetFood() {

        if (isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantMenuActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {

                registrationObject.put("RestaurantID", PrefUtils.getUser(RestaurantMenuActivity.this).RestaurantId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetFoodRestaurantWise, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetRFoodListMain submitResponse = new GsonBuilder().create().fromJson(response, GetRFoodListMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {

                        profileListView = submitResponse.getRFoodListData.getRFoodListArrayList.get(1).getFoodArrayList;
                        Log.e("size", profileListView.size() + "");
                        myCustomAdapter = new MyCustomAdapter(RestaurantMenuActivity.this, profileListView, isSelected);
                        profileList.setAdapter(myCustomAdapter);
                    } else {
                        Toast.makeText(RestaurantMenuActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) RestaurantMenuActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetFood> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }

        ArrayList<GetFood> arraylist;
        private Boolean isDeleteShow;
        public MyCustomAdapter(Context context, ArrayList<GetFood> postArrayList, boolean b) {
            isDeleteShow=b;
            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetFood>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMenuName;
            ImageView imgDelete;




        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_menu_list_item, parent, false);
                holder = new ViewHolder();

                holder.txtMenuName = (TextView) convertView.findViewById(R.id.txtMenuName);
                holder.imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if(isDeleteShow){
                holder.imgDelete.setVisibility(View.VISIBLE);
            }else{
                holder.imgDelete.setVisibility(View.GONE);
            }


            holder.txtMenuName.setText(profileListView.get(position).FoodName.toString().trim());
            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doPostOperation(profileListView.get(position).FoodId);
                }
            });



            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent i = new Intent(RestaurantMenuActivity.this, RestaurantAddFoodActivity.class);
                    i.putExtra("food_id", profileListView.get(position).FoodId);
                    i.putExtra("food_name", profileListView.get(position).FoodName);
                    i.putExtra("res_id", PrefUtils.getUser(RestaurantMenuActivity.this).RestaurantId);
                    startActivity(i);
//                    Log.e("restaurant id",profileListView.get(position).RestaurantID+"");
//                    String foodId=null;
//                    for(int i=0;i<horizontalList.size();i++){
//                        if(horizontalList.get(i).FoodName.equalsIgnoreCase(profileListView.get(position).Category)){
//                            foodId=horizontalList.get(i).FoodId;
//                        }
//                    }
//                    Log.e("food id",foodId+"");
//


                }
            });

            return convertView;
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetFood movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.FoodName.toLowerCase(Locale.getDefault()).contains(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Menu");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }


    private void doPostOperation(String menuIdValue) {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantMenuActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("FoodCatId", menuIdValue);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.DeleteFoodCat, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(RestaurantMenuActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(RestaurantMenuActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("food add response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



}
