package com.flyingfood.delivery.customerfragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerAccountActivity;
import com.flyingfood.delivery.customeractivities.CustomerBurgerHouseMenuActivity;
import com.flyingfood.delivery.customeractivities.CustomerCartActivity;
import com.flyingfood.delivery.customeractivities.CustomerChatListActivity;
import com.flyingfood.delivery.customeractivities.CustomerHomeActivity;
import com.flyingfood.delivery.customeractivities.CustomerIndirizziSalvatiActivity;
import com.flyingfood.delivery.customeractivities.CustomerOrdineActivity;
import com.flyingfood.delivery.model.LoginResponse;
import com.flyingfood.delivery.model.Restaurant.GetFood;
import com.flyingfood.delivery.model.Restaurant.GetFoodMain;
import com.flyingfood.delivery.model.Restaurant.GetFoodRestaurantWise;
import com.flyingfood.delivery.model.Restaurant.GetRestaurantFoodWiseMain;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.GsonBuilder;
import com.google.maps.android.SphericalUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerHomeFragment extends Fragment {

    private TextView spinnerCustom;
    private TextView txtHeader,txtAccountName;
    private LinearLayout llCart,llAccount,llChat,llOrder,llHome;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private MyCustomAdapter2 myCustomAdapter2;
    private ArrayList<GetFoodRestaurantWise> profileListView;
    private ArrayList<GetFoodRestaurantWise> filteredData;
    private ArrayList<GetFoodRestaurantWise> doubleFiltered;

    private LoginResponse user;
    // Spinner Drop down elements
    ArrayList<String> languages;
    private Double myLatitude,myLongitude;
    //    String item;
    LatLng from;


    private ArrayList<GetFood> horizontalList,verticalList;
    private HorizontalAdapter horizontalAdapter;
    private RecyclerView horizontal_recycler_view;

    public CustomerHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_customer_home, container, false);
        user= PrefUtils.getUser(getActivity());

//        txtAccountName= (TextView) convertView.findViewById(R.id.txtAccountName);
//        txtAccountName.setText(user.Username);
        profileList = (GridView)convertView.findViewById(R.id.lvProfiles);



        View headerView=getActivity().getLayoutInflater().inflate(R.layout.header_view,null);

        txtHeader= (TextView) headerView.findViewById(R.id.txtHeader);
        horizontal_recycler_view= (RecyclerView) convertView.findViewById(R.id.horizontal_recycler_view);

        getFoodItems();
        getRestaurantFoodWise();



        return convertView;
    }


    @Override
    public void onResume() {
        super.onResume();
        getRestaurantFoodWise();
    }



    public static JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            address = address.replaceAll(" ","%20");

            String api = "AIzaSyC1G_Cb_s3Hdcdf_bzQ1hZZlxIzCBAqY3I";

            HttpPost httppost = new HttpPost("https://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false&key=" +
                     api );
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();


            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    public  LatLng getLatLong(JSONObject jsonObject) {
        LatLng l=null;
        try {

            myLongitude = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");

            myLatitude = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");
            l=new LatLng(myLatitude,myLongitude);

        } catch (JSONException e) {


        }

        return l;
    }




    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    private void getRestaurantFoodWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {

                registrationObject.put("Latitude","45.68000");
                registrationObject.put("Longitude","9.72000");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetRestaurantFoodWise, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetRestaurantFoodWiseMain submitResponse = new GsonBuilder().create().fromJson(response, GetRestaurantFoodWiseMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.foodRestaurantWiseData.foodRestaurantWiseArrayList;
                        Log.e("lat lng",PrefUtils.getAddress(getActivity()).addressName+"");

                        ((CustomerHomeActivity)getActivity()).updateToolbar();


                        new AsyncTask<Void,LatLng,LatLng>(){

                            @Override
                            protected LatLng doInBackground(Void... voids) {
                                from = getLatLong(getLocationInfo(PrefUtils.getAddress(getActivity()).addressName)) ;
                                return from;
                            }

                            @Override
                            protected void onPostExecute(LatLng aVoid) {
                                from = aVoid;
                                Log.e("lat lng",from+"");

                                filteredData=new ArrayList<>();
                                for(int i=0;i<profileListView.size();i++){
                                    LatLng to=new LatLng(Double.parseDouble(profileListView.get(i).Latitude),Double.parseDouble(profileListView.get(i).Longitude));
                                    Log.e("from",from+"");
                                    Log.e("to",to+"");
                                    double distance= SphericalUtil.computeDistanceBetween(from, to);
                                    distance=round((distance/1000),2);

                                    Log.e("distance",distance+"");
                                    if(distance<=5.0){
                                        Log.e("distance inside ",distance+"");
                                        filteredData.add(profileListView.get(i));
//                                filteredData.get(i).distance=distance;
                                    }
                                }



                                doubleFiltered=new ArrayList<GetFoodRestaurantWise>();
                                doubleFiltered.addAll(filteredData);




                                for(int i=0;i<doubleFiltered.size();i++){



                                    for(int j=0;j<doubleFiltered.size();j++){
                                        if(i==j){
                                            continue;
                                        }
                                        if(doubleFiltered.get(i).RestaurantID.equalsIgnoreCase(doubleFiltered.get(j).RestaurantID)){
                                            doubleFiltered.get(i).Category=doubleFiltered.get(i).Category+", "+doubleFiltered.get(j).Category;
                                            doubleFiltered.remove(j);
                                        }
                                    }
                                }

                                for(int i=0;i<filteredData.size();i++){
                                    Log.e("data",filteredData.get(i).Category);
                                }

                                myCustomAdapter = new MyCustomAdapter(getActivity(), doubleFiltered,true);
                                profileList.setAdapter(myCustomAdapter);
                            }
                        }.execute();





                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private void getFoodItems() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("UserId",user.UserID );
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetFood, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetFoodMain submitResponse = new GsonBuilder().create().fromJson(response, GetFoodMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        horizontalList=submitResponse.getFoodData.getFoodArrayList;



                        horizontalAdapter=new HorizontalAdapter(horizontalList);

                        LinearLayoutManager horizontalLayoutManagaer
                                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

                        horizontal_recycler_view.setAdapter(horizontalAdapter);

                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetFoodRestaurantWise> pSize;
        Context context;

        @Override
        public int getCount() {
            return pSize.size();
        }
        ArrayList<GetFoodRestaurantWise> arraylist;

        boolean isFilter;
        public MyCustomAdapter(Context context, ArrayList postArrayList, boolean isFilterd) {

            this.context = context;
            this.pSize = postArrayList;
            arraylist = new ArrayList<GetFoodRestaurantWise>();
            arraylist.addAll(postArrayList);
            isFilter=isFilterd;
        }

        @Override
        public Object getItem(int position) {
            return pSize.get(position);
        }

        class ViewHolder {
            TextView txtCategory, txtRestaurantName,txtRating,txtTime;
            ImageView imageViewLogo;


        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.home_list_item, parent, false);
                holder = new MyCustomAdapter.ViewHolder();

                holder.imageViewLogo= (ImageView) convertView.findViewById(R.id.imageViewLogo);
                holder.txtCategory= (TextView) convertView.findViewById(R.id.txtCategory);
                holder.txtRestaurantName= (TextView) convertView.findViewById(R.id.txtRestaurantName);
                holder.txtRating= (TextView) convertView.findViewById(R.id.txtRating);
                holder.txtTime= (TextView) convertView.findViewById(R.id.txtTime);

                convertView.setTag(holder);
            } else {
                holder = (MyCustomAdapter.ViewHolder) convertView.getTag();
            }

            Glide.with(getActivity()).load(pSize.get(position).RestaurantImage).into(holder.imageViewLogo);
            holder.txtCategory.setText(pSize.get(position).Category);
            holder.txtRestaurantName.setText(pSize.get(position).RestaurantName);
            holder.txtRating.setText(pSize.get(position).Ratting);
            holder.txtTime.setText(pSize.get(position).Timing.split(" ")[0]+" min");

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("restaurant id",pSize.get(position).RestaurantID+"");
//                String foodId=null;
//                for(int i=0;i<horizontalList.size();i++){
//                    if(horizontalList.get(i).FoodName.equalsIgnoreCase(profileListView.get(position).Category)){
//                        foodId=horizontalList.get(i).FoodId;
//                    }
//                }
//                Log.e("food id",foodId+"");


                    Intent i=new Intent(getActivity(),CustomerBurgerHouseMenuActivity.class);
//                i.putExtra("food_id",foodId+"");
                    i.putExtra("res_id",pSize.get(position).RestaurantID+"");
                    i.putExtra("timing",pSize.get(position).Timing+"");
                    i.putExtra("l1",pSize.get(position).LunchStartTime+"");
                    i.putExtra("l2",pSize.get(position).LunchEndTime+"");
                    i.putExtra("d1",pSize.get(position).DinnerStartTime+"");
                    i.putExtra("d2",pSize.get(position).DinnerEndTime+"");
                    i.putExtra("rating",pSize.get(position).Ratting+"");
                    i.putExtra("res_name",pSize.get(position).RestaurantName+"");
                    startActivity(i);




                }
            });
            return convertView;
        }

        public void filterSearch(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            pSize.clear();
            if (charText.length() == 0) {
                pSize.addAll(arraylist);

            } else {
                for (GetFoodRestaurantWise movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.RestaurantName.toLowerCase(Locale.getDefault()).contains(charText)) {
                        pSize.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            pSize.clear();
            if (charText.length() == 0) {
                pSize.addAll(arraylist);

            } else {
                for (GetFoodRestaurantWise movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.Category.toLowerCase(Locale.getDefault()).contains(charText)) {
                        pSize.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }


    class MyCustomAdapter2 extends BaseAdapter

    {

        private ArrayList<GetFoodRestaurantWise> pSize;
        Context context;

        @Override
        public int getCount() {
            return pSize.size();
        }
        ArrayList<GetFoodRestaurantWise> arraylist;

        boolean isFilter;
        public MyCustomAdapter2(Context context, ArrayList postArrayList, boolean isFilterd) {

            this.context = context;
            this.pSize = postArrayList;
            arraylist = new ArrayList<GetFoodRestaurantWise>();
            arraylist.addAll(postArrayList);
            isFilter=isFilterd;
        }

        @Override
        public Object getItem(int position) {
            return pSize.get(position);
        }

        class ViewHolder {
            TextView txtCategory, txtRestaurantName,txtRating,txtTime;
            ImageView imageViewLogo;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                final MyCustomAdapter2.ViewHolder holder;
                LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.home_list_item, parent, false);
                    holder = new MyCustomAdapter2.ViewHolder();

                    holder.imageViewLogo = (ImageView) convertView.findViewById(R.id.imageViewLogo);
                    holder.txtCategory = (TextView) convertView.findViewById(R.id.txtCategory);
                    holder.txtRestaurantName = (TextView) convertView.findViewById(R.id.txtRestaurantName);
                    holder.txtRating = (TextView) convertView.findViewById(R.id.txtRating);
                    holder.txtTime = (TextView) convertView.findViewById(R.id.txtTime);

                    convertView.setTag(holder);
                } else {
                    holder = (MyCustomAdapter2.ViewHolder) convertView.getTag();
                }

                Glide.with(getActivity()).load(pSize.get(position).RestaurantImage).into(holder.imageViewLogo);
                holder.txtCategory.setText(pSize.get(position).Category);
                holder.txtRestaurantName.setText(pSize.get(position).RestaurantName);
                holder.txtRating.setText(pSize.get(position).Ratting);
                holder.txtTime.setText(pSize.get(position).Timing.split(" ")[0] + " min");

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.e("restaurant id", pSize.get(position).RestaurantID + "");
//                String foodId=null;
//                for(int i=0;i<horizontalList.size();i++){
//                    if(horizontalList.get(i).FoodName.equalsIgnoreCase(profileListView.get(position).Category)){
//                        foodId=horizontalList.get(i).FoodId;
//                    }
//                }
//                Log.e("food id",foodId+"");


                        Intent i = new Intent(getActivity(), CustomerBurgerHouseMenuActivity.class);
//                i.putExtra("food_id",foodId+"");
                        i.putExtra("res_id", pSize.get(position).RestaurantID + "");
                        i.putExtra("timing", pSize.get(position).Timing + "");
                        i.putExtra("l1", pSize.get(position).LunchStartTime + "");
                        i.putExtra("l2", pSize.get(position).LunchEndTime + "");
                        i.putExtra("d1", pSize.get(position).DinnerStartTime + "");
                        i.putExtra("d2", pSize.get(position).DinnerEndTime + "");
                        i.putExtra("rating", pSize.get(position).Ratting + "");
                        i.putExtra("res_name", pSize.get(position).RestaurantName + "");
                        startActivity(i);


                    }
                });
                return convertView;
            }catch(Exception e)
            {
                return  convertView;
            }
        }


    }



 

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

   
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

        private List<GetFood> horizontalList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txtFood;
            public ImageView imgFood;

            public MyViewHolder(View view) {
                super(view);
                txtFood = (TextView) view.findViewById(R.id.txtFood);
                imgFood=(ImageView) view.findViewById(R.id.imgFood);
            }
        }


        public HorizontalAdapter(List<GetFood> horizontalList) {
            this.horizontalList = horizontalList;
        }

        @Override
        public HorizontalAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_food, parent, false);

            return new HorizontalAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final HorizontalAdapter.MyViewHolder holder, final int position) {
            holder.txtFood.setText(horizontalList.get(position).FoodName);
            Glide.with(getActivity()).load(horizontalList.get(position).FoodImage).into(holder.imgFood);


            holder.imgFood.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getActivity(),"click",Toast.LENGTH_SHORT).show();
                    try
                    {
                        doubleFiltered.clear();
                        doubleFiltered.addAll(filteredData);
                        for(int i=0;i<doubleFiltered.size();i++){
                            if(doubleFiltered.get(i).Category.contains(", ")){
                                doubleFiltered.get(i).Category=doubleFiltered.get(i).Category.split(", ")[0];
                            }
                        }

                        myCustomAdapter.notifyDataSetChanged();

//                    doubleFiltered.clear();
//                    doubleFiltered.addAll(getFoodRestaurantWises);
//                    myCustomAdapter.notifyDataSetChanged();
//                    for(int i=0;i<getFoodRestaurantWises.size();i++){
//                        Log.e("data",getFoodRestaurantWises.get(i).Category);
//                    }
                        myCustomAdapter.filter(horizontalList.get(position).FoodName);
                    }catch(Exception e)
                    {

                        Toast.makeText(getActivity(),"Aspetta un secondo mentre caricarichiamo i ristoranti",Toast.LENGTH_SHORT).show();
                    }


                }
            });
        }

        @Override
        public int getItemCount() {
            return horizontalList.size();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
//                Toast.makeText(getActivity(), "called", Toast.LENGTH_SHORT).show();



                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myCustomAdapter.filterSearch(searchQuery.toString().trim());
                profileList.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });

    }



}
