package com.flyingfood.delivery.model.Cart;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 01/07/17.
 */

public class GetOrderUserIdWiseData {

    @SerializedName("MenuItems")
    public ArrayList<GetOrderMenuItems> getOrderMenuItemsArrayList;

    @SerializedName("OrderId")
    public String OrderId;


    @SerializedName("OrderDescription")
    public String OrderDescription;

}
