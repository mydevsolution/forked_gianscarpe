package com.flyingfood.delivery.model.Restaurant;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 01/07/17.
 */

public class GetRestaurantMenu {


    @SerializedName("Description")
    public String Description;

    @SerializedName("FoodId")
    public String FoodId;

    @SerializedName("MenuId")
    public String MenuId;

    @SerializedName("MenuName")
    public String MenuName;

    @SerializedName("Price")
    public String Price;

    @SerializedName("RestaurantId")
    public String RestaurantId;



}
