package com.flyingfood.delivery.restaurantfragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseMain;
import com.flyingfood.delivery.model.Restaurant.DataModel;
import com.flyingfood.delivery.model.Restaurant.ResponseModel;
import com.flyingfood.delivery.restaurantactivities.RestaurantAddOrderActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantHomeActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantOrderDetailActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantOrderDetailNewActivity;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import info.hoang8f.android.segmented.SegmentedGroup;

import static com.flyingfood.delivery.R.id.txtOrderAddress;


/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantHomeFragment extends Fragment {


    private LinearLayout llAccountName,llHome,llOrdini,llChat,llAccount,llCart;
    private TextView txtAccountName;

    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private MyCustomAdapter2 myCustomAdapter2;
    private ArrayList<GetOrderUserIdWiseData> profileListView;
    private ArrayList<DataModel> profileListView2;
    SegmentedGroup segmented3;


    public RestaurantHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater. inflate(R.menu.menu_restaurant_add_order_button, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_restaurant_home, container, false);
        segmented3= (SegmentedGroup)convertView.findViewById(R.id.segmentedButtonGroup);
        //Tint color, and text color when checked
        segmented3.setTintColor(Color.parseColor("#ef0c22"), Color.parseColor("#ffffff"));

        segmented3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.btnSignUp:

                        getOrdersUserIdWise();
                        break;
                    case R.id.btnSignIn:
                        getOrderForRestaurant();
                        break;
                    default:
                        // Nothing to do
                }
            }
        });

        profileList = (GridView)convertView.findViewById(R.id.lvProfiles);
        getOrdersUserIdWise();
        return convertView;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.btnAddOrder:

                Intent i = new Intent(getActivity(), RestaurantAddOrderActivity.class);
                startActivity(i);
                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getOrderForRestaurant() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();


            try {
                registrationObject.put("RestaurantId", PrefUtils.getUser(getActivity()).RestaurantId);
                registrationObject.put("CourierMasterId", "0");
            } catch (JSONException e) {
                e.printStackTrace();

            }

            String url=null;
            Log.e("login request object", registrationObject + "");

            url=   AppConstants.GetRestaurantOrderForRestaurant;

            new PostServiceCall(url, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("restaurant response...", response + "");
                    ResponseModel submitResponse = new GsonBuilder().create().fromJson(response, ResponseModel.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView2=submitResponse.dataListModel.dataModelArrayList;


                        myCustomAdapter2 = new MyCustomAdapter2(getActivity(), profileListView2);
                        profileList.setAdapter(myCustomAdapter2);


                        myCustomAdapter2.filter2(3+"");
                        profileList.invalidate();

                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    class MyCustomAdapter2 extends BaseAdapter

    {

        private ArrayList<DataModel> profileListView2;
        Context context;

        @Override
        public int getCount() {
            return profileListView2.size();
        }
        ArrayList<DataModel> arraylist;

        public MyCustomAdapter2(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView2 = postArrayList;
            arraylist = new ArrayList<DataModel>();
            arraylist.addAll(profileListView2);
        }

        @Override
        public Object getItem(int position) {
            return profileListView2.get(position);
        }

        class ViewHolder {
            TextView txtOrderTitle, txtOrderItem,txtOrderAddress,txtDeliveryTime;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final MyCustomAdapter2.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.courier_order_list_item, parent, false);
                holder = new MyCustomAdapter2.ViewHolder();


                holder.txtOrderTitle= (TextView) convertView.findViewById(R.id.txtOrderTitle);
                holder.txtOrderItem= (TextView) convertView.findViewById(R.id.txtOrderItem);
                holder.txtOrderAddress= (TextView) convertView.findViewById(txtOrderAddress);
                holder.txtDeliveryTime= (TextView) convertView.findViewById(R.id.txtDeliveryTime);

                convertView.setTag(holder);
            } else {
                holder = (MyCustomAdapter2.ViewHolder) convertView.getTag();
            }

            String finalString = null;
            try {
                String start_dt = profileListView2.get(position).DateGenerated;
                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                Date date = (Date)formatter.parse(start_dt);
                SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yy");
                finalString = newFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.txtOrderTitle.setText(profileListView2.get(position).getName().toString().trim()+" (Ordine #"+profileListView2.get(position).OrderId+")");
            holder.txtOrderAddress.setText("Address to delivery, "+profileListView2.get(position).DeliveryAddress.toString().trim());
            try {
                holder.txtDeliveryTime.setText(profileListView2.get(position).DeliveryTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.txtOrderItem.setText(profileListView2.get(position).RestaurantName);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    String address= holder.txtOrderAddress.getText().toString();
                    //  String name= holder.txtOrderTitle.getText().toString();
                    String mobileno= holder.txtOrderTitle.getText().toString();
                    String email= holder.txtOrderTitle.getText().toString();
                    String deliverytime= holder.txtDeliveryTime.getText().toString();

                    //String address =txtOrderAddress.setText().toString();
//                    PrefUtils.setRestaurantOrderHistory(profileListView2.get(position), getActivity());
                    Intent i = new Intent(getActivity(), RestaurantOrderDetailNewActivity.class);
                    i.putExtra("Name",profileListView2.get(position).Name+"");
                    i.putExtra("OrderID",profileListView2.get(position).OrderId+"");
                    i.putExtra("Mobile",profileListView2.get(position).Phone+"");
                    i.putExtra("Address",address);
                    //i.putExtra("Name",name);
                    i.putExtra("DeliveryTime",deliverytime);
                    startActivity(i);

                }
            });
            return convertView;
        }
        public void filter2(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView2.clear();
            if (charText.length() == 0) {
                profileListView2.addAll(arraylist);

            } else {
                for (DataModel movieDetails : arraylist) {

                    if (charText.length() != 0 && !movieDetails.getOrderStatusId().toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView2.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }



    }


    @Override
    public void onResume() {
        super.onResume();
        switch (segmented3.getCheckedRadioButtonId()) {
            case R.id.btnSignUp:

                getOrdersUserIdWise();
                break;
            case R.id.btnSignIn:
                getOrderForRestaurant();
                break;
            default:
                // Nothing to do
        }
    }

    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("RestaurantId", "0");
                registrationObject.put("UserID", PrefUtils.getUser(getActivity()).UserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("url",AppConstants.GetOrderRestaurantUserWise);
            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetOrderRestaurantUserWise, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetOrderUserIdWiseMain submitResponse = new GsonBuilder().create().fromJson(response, GetOrderUserIdWiseMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getOrderUserIdWiseDataArrayList;

                        myCustomAdapter = new MyCustomAdapter(getActivity(), profileListView);
                        profileList.setAdapter(myCustomAdapter);

                        myCustomAdapter.filter2(3+"");
                        profileList.invalidate();

                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetOrderUserIdWiseData> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetOrderUserIdWiseData> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetOrderUserIdWiseData>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtOrderTitle, txtOrderItem,txtOrderAddress,txtDeliveryTime;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.courier_order_list_item, parent, false);
                holder = new MyCustomAdapter.ViewHolder();


                holder.txtOrderTitle= (TextView) convertView.findViewById(R.id.txtOrderTitle);
                holder.txtOrderItem= (TextView) convertView.findViewById(R.id.txtOrderItem);
                holder.txtOrderAddress= (TextView) convertView.findViewById(txtOrderAddress);
                holder.txtDeliveryTime= (TextView) convertView.findViewById(R.id.txtDeliveryTime);

                convertView.setTag(holder);
            } else {
                holder = (MyCustomAdapter.ViewHolder) convertView.getTag();
            }

            String finalString = null;
            try {
                String start_dt = profileListView.get(position).getOrderMenuItemsArrayList.get(0).OrderDate;
                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                Date date = (Date)formatter.parse(start_dt);
                SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yy");
                finalString = newFormat.format(date);
            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.txtOrderTitle.setText(profileListView.get(position).getOrderMenuItemsArrayList.get(0).Name.toString().trim()+" (Ordine #"+profileListView.get(position).OrderId+")");
            holder.txtOrderAddress.setText("Address to delivery, "+profileListView.get(position).getOrderMenuItemsArrayList.get(0).DeliveryAddress.toString().trim());
            try {
                holder.txtDeliveryTime.setText(profileListView.get(position).getOrderMenuItemsArrayList.get(0).DeliveryTime.split("-")[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.txtOrderItem.setText(profileListView.get(position).getOrderMenuItemsArrayList.get(0).RestaurantName);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    String address= holder.txtOrderAddress.getText().toString();
                    //  String name= holder.txtOrderTitle.getText().toString();
                    String mobileno= holder.txtOrderTitle.getText().toString();
                    String email= holder.txtOrderTitle.getText().toString();
                    String deliverytime= holder.txtDeliveryTime.getText().toString();

                    //String address =txtOrderAddress.setText().toString();
                    PrefUtils.setRestaurantOrderHistory(profileListView.get(position), getActivity());
                    Intent i = new Intent(getActivity(), RestaurantOrderDetailActivity.class);
                    i.putExtra("Name",profileListView.get(position).getOrderMenuItemsArrayList.get(0).Name+"");
                    i.putExtra("Email",profileListView.get(position).getOrderMenuItemsArrayList.get(0).Email+"");
                    i.putExtra("Mobile",profileListView.get(position).getOrderMenuItemsArrayList.get(0).Mobile+"");
                    i.putExtra("Address",address);
                    //i.putExtra("Name",name);
                    i.putExtra("DeliveryTime",deliverytime);
                    startActivity(i);

                }
            });
            return convertView;
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetOrderUserIdWiseData movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.getOrderMenuItemsArrayList.get(0).OrderStatusMasterID.toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }

        public void filter2(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetOrderUserIdWiseData movieDetails : arraylist) {

                    if (charText.length() != 0 && !movieDetails.getOrderMenuItemsArrayList.get(0).OrderStatusMasterID.toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }

    }
    

}
