package com.flyingfood.delivery.courieractivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.MainActivity;
import com.flyingfood.delivery.custom.PrefUtils;

import java.util.Calendar;

public class CourierAccountActivity extends AppCompatActivity {

    LinearLayout llPersonalData, llOrderHistory, llWeeklyAvaibility, llPaymentMethod, llSupportMail;
    TextView txtLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier_account);
//        txtLogout= (TextView) findViewById(R.id.txtLogout);
//        txtLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PrefUtils.clearCurrentUser(CourierAccountActivity.this);
//                Intent intent = new Intent(CourierAccountActivity.this,MainActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });

        llPersonalData = (LinearLayout) findViewById(R.id.llPersonalData);
        llOrderHistory = (LinearLayout) findViewById(R.id.llOrderHistory);
        llWeeklyAvaibility = (LinearLayout) findViewById(R.id.llWeeklyAvaibility);
        llPaymentMethod = (LinearLayout) findViewById(R.id.llPaymentMethod);
        llSupportMail = (LinearLayout) findViewById(R.id.llSupportMail);

        Calendar calendar = Calendar.getInstance();
        final int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                // Current day is Sunday

            case Calendar.MONDAY:
                // Current day is Monday

            case Calendar.TUESDAY:
                // etc.
        }

        llPersonalData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CourierAccountActivity.this, CourierPesonalDataActivity.class);
                startActivity(intent);

            }
        });
        llOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CourierAccountActivity.this, CourierOrderHistoryActivity.class);
                startActivity(intent);


            }
        });
        llWeeklyAvaibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(day== Calendar.MONDAY){
                    Intent intent = new Intent(CourierAccountActivity.this, CourierWeeklyAvaibilityActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(CourierAccountActivity.this, "You can change only on monday", Toast.LENGTH_SHORT).show();
                }
            


            }
        });
        llPaymentMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(CourierAccountActivity.this,Cour.class);
//                startActivity(intent);


            }
        });
        llSupportMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CourierAccountActivity.this, CourierRichiediSupportoActivity.class);
                startActivity(intent);


            }
        });

        setToolbar();

    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account_logout_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnLogOut:

                PrefUtils.clearCurrentUser(CourierAccountActivity.this);

                Intent intent = new Intent(CourierAccountActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Account");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
