package com.flyingfood.delivery.model.Cart;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 04/07/17.
 */

public class CartItem {



    @SerializedName("CartMasterID")
    public String CartMasterID;

    @SerializedName("Description")
    public String Description;

    @SerializedName("FoodId")
    public String FoodId;

    @SerializedName("ItemSold")
    public String ItemSold;

    @SerializedName("MenuId")
    public String MenuId;

    @SerializedName("MenuName")
    public String MenuName;

    @SerializedName("Price")
    public String Price;

    @SerializedName("Quntity")
    public String Quntity;

    @SerializedName("UserID")
    public String UserID;


}
