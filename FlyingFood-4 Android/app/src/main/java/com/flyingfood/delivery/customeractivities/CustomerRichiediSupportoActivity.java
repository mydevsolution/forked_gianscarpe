package com.flyingfood.delivery.customeractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.flyingfood.delivery.model.LoginResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerRichiediSupportoActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText txtMessage,txtTitle;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_richiedi_supporto);

        txtTitle= (EditText) findViewById(R.id.txtTitle);
        txtMessage= (EditText) findViewById(R.id.txtMessage);
        btnLogin= (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtTitle.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerRichiediSupportoActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                }else  if(txtTitle.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerRichiediSupportoActivity.this, "Please Enter Message", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation();
                }
                
            }
        });


        setToolbar();


    }



    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerRichiediSupportoActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();
            LoginResponse user= PrefUtils.getUser(CustomerRichiediSupportoActivity.this);


            try {
                registrationObject.put("Email", user.Email);
                registrationObject.put("Message", txtTitle.getText().toString().trim());
                registrationObject.put("Subject", txtMessage.getText().toString().trim());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.SEND_MESSAGE_TO_SUPPORT, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerRichiediSupportoActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CustomerRichiediSupportoActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Richiedi supporto");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
    
}
