package com.flyingfood.delivery.restaurantactivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.flyingfood.delivery.model.Restaurant.GetRestaurantMenu;
import com.flyingfood.delivery.model.Restaurant.GetRFoodListMain;
import com.flyingfood.delivery.model.Restaurant.GetRestaurantMenu;
import com.flyingfood.delivery.model.Restaurant.GetRestaurantMenuMain;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class RestaurantAddFoodActivity extends AppCompatActivity {

    private ImageView btnAddFood;
    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ImageView btnDeleteFoodcat;
    private ArrayList<GetRestaurantMenu> profileListView;
    boolean isSelected=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_add_food);

        btnAddFood = (ImageView) findViewById(R.id.btnAddFood);
        profileList = (GridView) findViewById(R.id.profileList);
        btnDeleteFoodcat= (ImageView) findViewById(R.id.btnDeleteFoodcat);
        btnDeleteFoodcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("yes","click");

                if(isSelected==true){
                    isSelected=false;
                    myCustomAdapter = new MyCustomAdapter(RestaurantAddFoodActivity.this, profileListView,isSelected);
                    profileList.setAdapter(myCustomAdapter);

                    myCustomAdapter.filter(getIntent().getStringExtra("food_id"));
                    profileList.invalidate();
                }else{
                    Log.e("yes","true");
                    isSelected=true;
                    myCustomAdapter = new MyCustomAdapter(RestaurantAddFoodActivity.this, profileListView,isSelected);
                    profileList.setAdapter(myCustomAdapter);

                    myCustomAdapter.filter(getIntent().getStringExtra("food_id"));
                    profileList.invalidate();
                }
            }
        });
        btnAddFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(RestaurantAddFoodActivity.this, RestaurantAddingFoodItemActivity.class);
               i.putExtra("food_id",getIntent().getStringExtra("food_id"));
                startActivity(i);

            }
        });
        setToolbar();


    }

    private void doPostOperation(String menuIdValue) {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantAddFoodActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("MenuId", menuIdValue);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.DeleteMenu, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(RestaurantAddFoodActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(RestaurantAddFoodActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("food add response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }





    @Override
    protected void onResume() {
        super.onResume();
        GetRestaurantMenu();
    }

    private void GetRestaurantMenu() {

        if (isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantAddFoodActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("FoodId", getIntent().getStringExtra("food_id"));
                registrationObject.put("RestaurantID", PrefUtils.getUser(RestaurantAddFoodActivity.this).RestaurantId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetRestaurantMenu, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetRestaurantMenuMain submitResponse = new GsonBuilder().create().fromJson(response, GetRestaurantMenuMain.class);

                    profileListView = submitResponse.getRestaurantMenuData.getRestaurantMenuArrayList;
                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {

                        Log.e("size", profileListView.size() + "");
                        myCustomAdapter = new MyCustomAdapter(RestaurantAddFoodActivity.this, profileListView,false);
                        profileList.setAdapter(myCustomAdapter);

                        myCustomAdapter.filter(getIntent().getStringExtra("food_id"));
                        profileList.invalidate();
                    } else {
                        Toast.makeText(RestaurantAddFoodActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) RestaurantAddFoodActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetRestaurantMenu> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }

        ArrayList<GetRestaurantMenu> arraylist;
        private Boolean isDeleteShow;

        public MyCustomAdapter(Context context, ArrayList<GetRestaurantMenu> postArrayList, final boolean b) {
            isDeleteShow=b;
            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetRestaurantMenu>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMenuName;
            ImageView imgDelete;




        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_menu_list_item, parent, false);
                holder = new ViewHolder();

                holder.txtMenuName = (TextView) convertView.findViewById(R.id.txtMenuName);
                holder.imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);

                convertView.setTag(holder);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent editMenu = new Intent(RestaurantAddFoodActivity.this, RestaurantEditMenuActivity.class);
                        editMenu.putExtra("name", profileListView.get(position).MenuName);
                        editMenu.putExtra("food_id", profileListView.get(position).FoodId);

                        editMenu.putExtra("menu_id", profileListView.get(position).MenuId);

                        editMenu.putExtra("price", profileListView.get(position).Price);


                        editMenu.putExtra("description", profileListView.get(position).Description);
                        startActivity(editMenu);

                    }});

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if(isDeleteShow){
                holder.imgDelete.setVisibility(View.VISIBLE);
            }else{
                holder.imgDelete.setVisibility(View.GONE);
            }

            holder.txtMenuName.setText(profileListView.get(position).MenuName.toString().trim());
            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doPostOperation(profileListView.get(position).MenuId);
                }
            });


            return convertView;
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetRestaurantMenu movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.FoodId.toLowerCase(Locale.getDefault()).equalsIgnoreCase(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Menu");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }


}
