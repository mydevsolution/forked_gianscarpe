package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.model.LoginResponse;
import com.flyingfood.delivery.model.Restaurant.GetFoodMain;
import com.flyingfood.delivery.model.Restaurant.GetFoodRestaurantWise;
import com.flyingfood.delivery.model.Restaurant.GetRFoodListMain;
import com.flyingfood.delivery.model.Restaurant.GetFood;
import com.flyingfood.delivery.model.Restaurant.GetFoodMain;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class CustomerBurgerHouseMenuActivity extends AppCompatActivity {

    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetFood> profileListView;
    private LoginResponse user;

    private TextView txtD2,txtD1,txtL1,txtL2,txtTiming;
    private ImageView imgRatings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_burger_house_menu);
        setToolbar();
        profileList = (GridView)findViewById(R.id.lvProfiles);

        imgRatings= (ImageView) findViewById(R.id.imgRatings);
        txtD2= (TextView) findViewById(R.id.txtD2);
        txtD1= (TextView) findViewById(R.id.txtD1);
        txtL1= (TextView) findViewById(R.id.txtL1);
        txtL2= (TextView) findViewById(R.id.txtL2);
        txtTiming= (TextView) findViewById(R.id.txtTiming);
        txtTiming.setText(getIntent().getStringExtra("timing"));
        txtL1.setText(getIntent().getStringExtra("l1").equals("0") ? "Chiuso" : getIntent().getStringExtra("l1") );
        txtL2.setText(getIntent().getStringExtra("l2").equals("0") ? "Chiuso" : getIntent().getStringExtra("l2"));
        txtD1.setText(getIntent().getStringExtra("d1").equals("0") ? "Chiuso" : getIntent().getStringExtra("d1"));
        txtD2.setText(getIntent().getStringExtra("d2").equals("0") ? "Chiuso" : getIntent().getStringExtra("d2"));

        String ratings=getIntent().getStringExtra("rating");
        if(ratings.equalsIgnoreCase("1")){
            imgRatings.setImageResource(R.drawable.star1);
        }else if(ratings.equalsIgnoreCase("2")){
            imgRatings.setImageResource(R.drawable.star2);
        }else if(ratings.equalsIgnoreCase("3")){
            imgRatings.setImageResource(R.drawable.star3);
        }else if(ratings.equalsIgnoreCase("4")){
            imgRatings.setImageResource(R.drawable.star4);
        }else if(ratings.equalsIgnoreCase("5")){
            imgRatings.setImageResource(R.drawable.star5);
        }
        GetFood();
    }


    private void GetFood() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerBurgerHouseMenuActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();

            JSONObject registrationObject = new JSONObject();

            try {

                registrationObject.put("RestaurantID",getIntent().getStringExtra("res_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetFoodRestaurantWise, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetRFoodListMain submitResponse = new GsonBuilder().create().fromJson(response, GetRFoodListMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {

                        profileListView=submitResponse.getRFoodListData.getRFoodListArrayList.get(1).getFoodArrayList;
                        Log.e("size",profileListView.size()+"");
                        myCustomAdapter=new MyCustomAdapter(CustomerBurgerHouseMenuActivity.this,profileListView);
                        profileList.setAdapter(myCustomAdapter);
                    } else {
                        Toast.makeText(CustomerBurgerHouseMenuActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerBurgerHouseMenuActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetFood> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetFood> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetFood>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMenuName;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_menu, parent, false);
                holder = new CustomerBurgerHouseMenuActivity.MyCustomAdapter.ViewHolder();

                holder.txtMenuName= (TextView) convertView.findViewById(R.id.txtMenuName);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txtMenuName.setText(profileListView.get(position).FoodName.toString().trim());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent i=new Intent(CustomerBurgerHouseMenuActivity.this,CustomerBurgerHouseActivity.class);
                    i.putExtra("food_id",profileListView.get(position).FoodId);
                    i.putExtra("food_name",profileListView.get(position).FoodName);
                    i.putExtra("res_id",getIntent().getStringExtra("res_id"));
                    startActivity(i);
//                    Log.e("restaurant id",profileListView.get(position).RestaurantID+"");
//                    String foodId=null;
//                    for(int i=0;i<horizontalList.size();i++){
//                        if(horizontalList.get(i).FoodName.equalsIgnoreCase(profileListView.get(position).Category)){
//                            foodId=horizontalList.get(i).FoodId;
//                        }
//                    }
//                    Log.e("food id",foodId+"");
//



                }
            });
            return convertView;
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetFood movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.FoodName.toLowerCase(Locale.getDefault()).contains(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
//                Toast.makeText(MainActivity.this, "called", Toast.LENGTH_SHORT).show();



                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myCustomAdapter.filter(searchQuery.toString().trim());
                profileList.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle(getIntent().getStringExtra("res_name"));
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}
