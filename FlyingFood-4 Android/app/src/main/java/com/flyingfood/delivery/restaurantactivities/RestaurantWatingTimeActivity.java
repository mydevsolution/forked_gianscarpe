package com.flyingfood.delivery.restaurantactivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class RestaurantWatingTimeActivity extends AppCompatActivity {

    private LinearLayout ll10minute,ll20minute,ll30minute,ll40minute,ll50minute,ll60minute;
    private ImageView imag10Check,imag20Check,imag30Check,imag40Check,imag50Check,imag60Check;
  
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_wating_time);



        imag10Check = (ImageView) findViewById(R.id.imag10Check);
        imag20Check = (ImageView) findViewById(R.id.imag20Check);
        imag30Check = (ImageView) findViewById(R.id.imag30Check);
        imag40Check = (ImageView) findViewById(R.id.imag40Check);
        imag50Check = (ImageView) findViewById(R.id.imag50Check);
        imag60Check = (ImageView) findViewById(R.id.imag60Check);


        ll10minute = (LinearLayout) findViewById(R.id.ll10minute);
        ll20minute = (LinearLayout) findViewById(R.id.ll20minute);
        ll30minute = (LinearLayout) findViewById(R.id.ll30minute);
        ll40minute = (LinearLayout) findViewById(R.id.ll40minute);
        ll50minute = (LinearLayout) findViewById(R.id.ll50minute);
        ll60minute = (LinearLayout) findViewById(R.id.ll60minute);

        imag10Check = (ImageView) findViewById(R.id.imag10Check);
        imag20Check = (ImageView) findViewById(R.id.imag20Check);
        imag30Check = (ImageView) findViewById(R.id.imag30Check);
        imag40Check = (ImageView) findViewById(R.id.imag40Check);
        imag50Check = (ImageView) findViewById(R.id.imag50Check);
        imag60Check = (ImageView) findViewById(R.id.imag60Check);


        ll10minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imag10Check.setVisibility(View.VISIBLE);
                doPostOperation("10 Min");


            }
        });
        ll20minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag20Check.setVisibility(View.VISIBLE);
                doPostOperation("20 Min");
            }
        });
        ll30minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag30Check.setVisibility(View.VISIBLE);
                doPostOperation("30 Min");

            }
        });
        ll40minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag40Check.setVisibility(View.VISIBLE);
                doPostOperation("40 Min");

            }
        });
        ll50minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imag50Check.setVisibility(View.VISIBLE);
                doPostOperation("50 Min");


            }
        });
        ll60minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imag60Check.setVisibility(View.VISIBLE);
                doPostOperation("60 Min");

            }
        });


        setToolbar();

    }
    private void doPostOperation(String min) {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantWatingTimeActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("Time", min);
                registrationObject.put("UserId", PrefUtils.getUser(RestaurantWatingTimeActivity.this).UserID+"");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.UpdateWaitingTime, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(RestaurantWatingTimeActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(RestaurantWatingTimeActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Wating Time");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}
