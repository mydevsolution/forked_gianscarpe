package com.flyingfood.delivery.courieractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;

public class CourierOrderDetailNewActivity extends AppCompatActivity {

    private ImageView imgChat, imgGo, imagMenu;
    String orderStatus;
    TextView txtNameSurname, txtAddress, txtTelephone, txtEmail, txtRestaurantDetails, txtRestaurantTime, txtDeliveryTime;
    String url,orderId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier_order_detail_new);
        imgGo = (ImageView) findViewById(R.id.imgGo);
        txtNameSurname = (TextView) findViewById(R.id.txtNameSurname);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtTelephone = (TextView) findViewById(R.id.txtTelephone);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
//        txtRestaurantDetails = (TextView) findViewById(R.id.txtRestaurantDetails);
        txtRestaurantTime = (TextView) findViewById(R.id.txtRestaurantTime);
        txtDeliveryTime = (TextView) findViewById(R.id.txtDeliveryTime);
        orderId = getIntent().getStringExtra("order_id");

        txtAddress.setText(getIntent().getStringExtra("Address"));
        //   txtNameSurname.setText(getIntent().getStringExtra("Name"));
        txtDeliveryTime.setText(getIntent().getStringExtra("DeliveryTime"));

        txtNameSurname.setText(getIntent().getStringExtra("Name"));
        txtTelephone.setText(getIntent().getStringExtra("Mobile"));
        txtEmail.setText(getIntent().getStringExtra("Email"));


        java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm a");
        java.util.Date date1 = null;
        try {
            date1 = df.parse(getIntent().getStringExtra("DeliveryTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.HOUR,0);
        cal.add(Calendar.MINUTE,-10);
        if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
            txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" PM");
        }else{
            txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" AM");
        }

        setToolbar();

        orderStatus = getIntent().getStringExtra("order_status");
        if (orderStatus.equalsIgnoreCase("1")) {
            imgGo.setBackgroundResource(R.drawable.btngo);
        } else if (orderStatus.equalsIgnoreCase("2")) {
            imgGo.setBackgroundResource(R.drawable.deliver);
        } else if (orderStatus.equalsIgnoreCase("3")) {
            imgGo.setBackgroundResource(R.drawable.delivered);
        }

        imgGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (orderStatus.equalsIgnoreCase("1")) {
                    url = AppConstants.AcceptRestaurantOrder;
                    doPostOperation(url);
                } else if (orderStatus.equalsIgnoreCase("2")) {
                    url = AppConstants.CompleteRestaurantOrder;
                    doPostOperation(url);
                } else if (orderStatus.equalsIgnoreCase("3")) {

                }

            }
        });
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Ordine #"+getIntent().getStringExtra("order_id"));
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    private void doPostOperation(String url) {

        if (isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CourierOrderDetailNewActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();


            try {
                registrationObject.put("CourierMasterId", PrefUtils.getUser(CourierOrderDetailNewActivity.this).UserID);
                registrationObject.put("OrderId", orderId);
                registrationObject.put("DeviceToken", PrefUtils.getToken(CourierOrderDetailNewActivity.this).token);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(url, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CourierOrderDetailNewActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CourierOrderDetailNewActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
