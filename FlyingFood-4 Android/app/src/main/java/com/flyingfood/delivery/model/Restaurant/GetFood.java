package com.flyingfood.delivery.model.Restaurant;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 01/07/17.
 */

public class GetFood {


    @SerializedName("FoodId")
    public String FoodId;

    @SerializedName("FoodImage")
    public String FoodImage;

    @SerializedName("FoodName")
    public String FoodName;
}
