package com.flyingfood.delivery.restaurantactivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class RestaurantAddingFoodcatActivity extends AppCompatActivity {
    private Button btnDone;
    private ImageView imgBack;
    private EditText etFoodcatName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_adding_foodcat);
        btnDone = (Button) findViewById(R.id.btnDone);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        etFoodcatName = (EditText) findViewById(R.id.etFoodcatName);
   
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etFoodcatName.getText().toString().length()==0){
                    Toast.makeText(RestaurantAddingFoodcatActivity.this, "Please Enter Category Name", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation();
                }
            }
        });

        setToolbar();
    }


    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(RestaurantAddingFoodcatActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {
                registrationObject.put("FoodName", etFoodcatName.getText().toString().trim());
                registrationObject.put("RestaurantId", PrefUtils.getUser(RestaurantAddingFoodcatActivity.this).RestaurantId);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.AddCategory, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(RestaurantAddingFoodcatActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(RestaurantAddingFoodcatActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Adding Foodcat");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
