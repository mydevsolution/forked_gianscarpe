package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.CommonChatActivity;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.GetOrderMenuItems;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CustomerOrderDetailActivity extends AppCompatActivity {

    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetOrderMenuItems> profileListView;
    private GetOrderUserIdWiseData getOrderUserIdWiseData;
    private TextView txtItemName, txtDate, txtTime1, txtTime2, txtCourierName;
    private ImageView imgChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_order_detail);

        getOrderUserIdWiseData = PrefUtils.getRestaurantOrderHistory(CustomerOrderDetailActivity.this);
        profileList = (GridView) findViewById(R.id.lvProfiles);
        profileListView = getOrderUserIdWiseData.getOrderMenuItemsArrayList;
        imgChat = (ImageView) findViewById(R.id.imgChat);

        txtItemName = (TextView) findViewById(R.id.txtItemName);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtTime1 = (TextView) findViewById(R.id.txtTime1);

        txtCourierName = (TextView) findViewById(R.id.txtCourierName);


        imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(CustomerOrderDetailActivity.this, CommonChatActivity.class);
                i.putExtra("order_id",profileListView.get(0).OrderID+"");
                if(profileListView.get(0).OrderStatusMasterID.equalsIgnoreCase("1")) {
                    i.putExtra("rec_id", profileListView.get(0).RestaurantUserID + "");
                }else{
                    i.putExtra("rec_id", profileListView.get(0).CourierUserID + "");
                }
                i.putExtra("send_id",PrefUtils.getUser(CustomerOrderDetailActivity.this).UserID+"");

                Log.e("order_id",profileListView.get(0).OrderID+"");
                if(profileListView.get(0).CourierUserID.equalsIgnoreCase("0")){
                    Log.e("rec_id",profileListView.get(0).RestaurantUserID+"");
                }else{
                    Log.e("rec_id",profileListView.get(0).CourierUserID+"");
                }
                Log.e("send_id",PrefUtils.getUser(CustomerOrderDetailActivity.this).UserID+"");
                startActivity(i);

                // finish();

            }
        });

        myCustomAdapter = new MyCustomAdapter(CustomerOrderDetailActivity.this, profileListView);
        profileList.setAdapter(myCustomAdapter);
       // txtAddress.setText(getIntent().getStringExtra("Address"));
        txtItemName.setText(profileListView.get(0).RestaurantName+ "");
        txtTime1.setText(profileListView.get(0).DeliveryTime+ "");

        String finalString = null;
        try {
            String start_dt = profileListView.get(0).OrderDate;
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
            Date date = (Date) formatter.parse(start_dt);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yy");
            finalString = newFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        txtDate.setText(finalString+ "");
    //    txtCourierName.setText(profileListView.get(0).CourierUserID+ "");

       if(profileListView.get(0).OrderStatusMasterID.equalsIgnoreCase("1")){
           txtCourierName.setText("In proparazione");
       }else if(profileListView.get(0).OrderStatusMasterID.equalsIgnoreCase("3")){
           txtCourierName.setText("Delivered");
       }

       // txtItemName.setText(profileListView.get(0).RestaurantName+ "");


        setToolbar();
    }

    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetOrderMenuItems> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMenuOrder, txtMenuPrice, txt2, txt3, txt4, txt5, txt6,txtMenuOrderDes;
            ImageView imgProfile;
            LinearLayout llFeed, llOption1, llOption2, llOption3, llOption4, llOption5, llOption6;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_order_detail_list_item, parent, false);
                holder = new ViewHolder();

                holder.txtMenuOrder = (TextView) convertView.findViewById(R.id.txtMenuOrder);
                holder.txtMenuPrice = (TextView) convertView.findViewById(R.id.txtMenuPrice);
                holder.txtMenuOrderDes = (TextView) convertView.findViewById(R.id.txtMenuOrderDes);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();


            }
            holder.txtMenuOrderDes.setText(profileListView.get(position).Description);
            holder.txtMenuOrder.setText(profileListView.get(position).MenuName + " (quantity " + profileListView.get(position).Quntity + ")");
            holder.txtMenuPrice.setText("€"+(Double.parseDouble(profileListView.get(position).Price)*Integer.parseInt(profileListView.get(position).Quntity)) );


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return convertView;
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Ordine #"+profileListView.get(0).OrderID);
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
