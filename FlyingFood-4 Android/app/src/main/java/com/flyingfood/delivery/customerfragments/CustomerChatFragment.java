package com.flyingfood.delivery.customerfragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.CommonChatActivity;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerChatListActivity;
import com.flyingfood.delivery.model.Account.GetLastOrderMEssageCustomerMain;
import com.flyingfood.delivery.model.Account.GetLastOrderMessageCustomer;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerChatFragment extends Fragment {

    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetLastOrderMessageCustomer> profileListView;
    
    public CustomerChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_customer_chat, container, false);
        profileList = (GridView)convertView.findViewById(R.id.lvProfiles);
        getOrdersUserIdWise();
        return  convertView;
    }


    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("CustomerId", PrefUtils.getUser(getActivity()).UserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GET_LAST_ORDER_MESSAGE_CUSOMER, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetLastOrderMEssageCustomerMain submitResponse = new GsonBuilder().create().fromJson(response, GetLastOrderMEssageCustomerMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getLastOrderMessageCustomerData.getLastOrderMessageCustomerArrayList;

                        myCustomAdapter = new MyCustomAdapter(getActivity(), profileListView);
                        profileList.setAdapter(myCustomAdapter);



                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetLastOrderMessageCustomer> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetLastOrderMessageCustomer> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetLastOrderMessageCustomer>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMessage, txtTitle;
            ImageView imgRestaurant;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_chat_list_item, parent, false);
                holder = new MyCustomAdapter.ViewHolder();

//
                holder.txtMessage= (TextView) convertView.findViewById(R.id.txtMessage);
                holder.txtTitle= (TextView) convertView.findViewById(R.id.txtTitle);
                holder.imgRestaurant= (ImageView) convertView.findViewById(R.id.imgItemIcon);


                convertView.setTag(holder);
            } else {
                holder = (MyCustomAdapter.ViewHolder) convertView.getTag();
            }

            holder.txtMessage.setText(profileListView.get(position).Text.trim());
            holder.txtTitle.setText(profileListView.get(position).RestaurantName.trim());
            Glide.with(getActivity()).load(profileListView.get(position).RestaurantImage).into(holder.imgRestaurant);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i=new Intent(getActivity(), CommonChatActivity.class);
                    i.putExtra("order_id",profileListView.get(position).OrderId+"");

                    i.putExtra("isFromChat",true);
                        i.putExtra("rec_id",profileListView.get(position).ReceiverID+"");

                    i.putExtra("send_id",profileListView.get(position).SenderID+"");
                    startActivity(i);
                }
            });
            return convertView;
        }



    }

    

}
