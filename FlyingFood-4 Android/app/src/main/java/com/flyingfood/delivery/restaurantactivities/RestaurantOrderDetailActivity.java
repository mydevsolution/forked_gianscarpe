package com.flyingfood.delivery.restaurantactivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.CommonChatActivity;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerOrderDetailActivity;
import com.flyingfood.delivery.model.Cart.GetOrderMenuItems;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class RestaurantOrderDetailActivity extends AppCompatActivity {
    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetOrderMenuItems> profileListView;
    private ImageView imgChat;
    private GetOrderUserIdWiseData getOrderUserIdWiseData;
    TextView txtNameSurname, txtAddress, txtTelephone, txtEmail, txtRestaurantDetails, txtRestaurantTime, txtDeliveryTime,txtDes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_order_detail);

        getOrderUserIdWiseData = PrefUtils.getRestaurantOrderHistory(RestaurantOrderDetailActivity.this);
        profileList = (GridView) findViewById(R.id.lvProfiles);
        profileListView = getOrderUserIdWiseData.getOrderMenuItemsArrayList;
        imgChat = (ImageView) findViewById(R.id.imgChat);
        txtNameSurname = (TextView) findViewById(R.id.txtNameSurname);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtTelephone = (TextView) findViewById(R.id.txtTelephone);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtRestaurantTime = (TextView) findViewById(R.id.txtRestaurantTime);
        txtDeliveryTime = (TextView) findViewById(R.id.txtDeliveryTime);
        txtDes= (TextView) findViewById(R.id.txtDes);
        txtDes.setText(getOrderUserIdWiseData.OrderDescription);

        imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RestaurantOrderDetailActivity.this, CommonChatActivity.class);
                i.putExtra("order_id",profileListView.get(0).OrderID+"");
                if(profileListView.get(0).OrderStatusMasterID.equalsIgnoreCase("1")) {
                    i.putExtra("rec_id", profileListView.get(0).CustomerUserID + "");
                }else{
                    i.putExtra("rec_id", profileListView.get(0).CourierUserID + "");
                }
                i.putExtra("send_id",PrefUtils.getUser(RestaurantOrderDetailActivity.this).UserID+"");

                startActivity(i);
            }
        });
        txtAddress.setText(getIntent().getStringExtra("Address"));
        //   txtNameSurname.setText(getIntent().getStringExtra("Name"));
        txtDeliveryTime.setText(getIntent().getStringExtra("DeliveryTime").split(" - ")[0]);

        txtNameSurname.setText(getIntent().getStringExtra("Name"));
        txtTelephone.setText(getIntent().getStringExtra("Mobile"));
        txtEmail.setText(getIntent().getStringExtra("Email"));

        java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm a", Locale.US);
        java.util.Date date1 = null;
        try {
            date1 = df.parse(getIntent().getStringExtra("DeliveryTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.HOUR,0);
        cal.add(Calendar.MINUTE,-15);
        if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
            txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" PM");
        }else{
            txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" AM");
        }


        myCustomAdapter = new MyCustomAdapter(RestaurantOrderDetailActivity.this, profileListView);
        profileList.setAdapter(myCustomAdapter);


        setToolbar();
    }

    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetOrderMenuItems> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMenuOrder, txtMenuPrice, txt2, txt3, txt4, txt5, txt6,txtMenuOrderDes;
            ImageView imgProfile;
            LinearLayout llFeed, llOption1, llOption2, llOption3, llOption4, llOption5, llOption6;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_order_detail_list_item, parent, false);
                holder = new ViewHolder();

                holder.txtMenuOrder = (TextView) convertView.findViewById(R.id.txtMenuOrder);
                holder.txtMenuPrice = (TextView) convertView.findViewById(R.id.txtMenuPrice);
                holder.txtMenuOrderDes = (TextView) convertView.findViewById(R.id.txtMenuOrderDes);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();


            }
            holder.txtMenuOrderDes.setText(profileListView.get(position).Description);
            holder.txtMenuOrder.setText(profileListView.get(position).MenuName + " (quantity " + profileListView.get(position).Quntity + ")");
            holder.txtMenuPrice.setText("€"+(Double.parseDouble(profileListView.get(position).Price)*Integer.parseInt(profileListView.get(position).Quntity)));

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return convertView;
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Ordine #" + profileListView.get(0).OrderID+ " Details");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
