package com.flyingfood.delivery.commonactivities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonfragments.CourierLoginFragment;
import com.flyingfood.delivery.commonfragments.CouriorRegFragment;
import com.flyingfood.delivery.commonfragments.CustomerLoginFragment;
import com.flyingfood.delivery.commonfragments.CustomerRegFragment;
import com.flyingfood.delivery.commonfragments.RestaurantLoginFragment;
import com.flyingfood.delivery.commonfragments.RestaurantRegFragment;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.storage.RandomNumberModel;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    SegmentedGroup   sbg;
    ImageView imgBack;
    private ViewPager viewPager;
    private Button btnSignUp,btnSignIn;
    RandomNumberModel randomNumberModel=null;


    public int getRandomNumber(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1)) + min;
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            randomNumberModel= PrefUtils.getRandom(MainActivity.this);
            if(randomNumberModel==null){
                randomNumberModel=new RandomNumberModel();
                randomNumberModel.randomNumber=getRandomNumber(1,10000000)+"";
                PrefUtils.setRandom(randomNumberModel,MainActivity.this);
            }
        } catch (Exception e) {
            e.printStackTrace();
            randomNumberModel=new RandomNumberModel();
            randomNumberModel.randomNumber=getRandomNumber(1,10000000)+"";
            PrefUtils.setRandom(randomNumberModel,MainActivity.this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


            SegmentedGroup segmented3 = (SegmentedGroup)findViewById(R.id.segmentedButtonGroup);
            //Tint color, and text color when checked
           segmented3.setTintColor(Color.parseColor("#ef0c22"), Color.parseColor("#ffffff"));

        segmented3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.btnSignUp:

                        viewPager = (ViewPager) findViewById(R.id.viewpager);
                        setupViewPager(viewPager,0);

                        tabLayout = (TabLayout) findViewById(R.id.tabs);
                        tabLayout.setupWithViewPager(viewPager);
                        break;
                    case R.id.btnSignIn:
                        viewPager = (ViewPager) findViewById(R.id.viewpager);
                        setupViewPager(viewPager,1);

                        tabLayout = (TabLayout) findViewById(R.id.tabs);
                        tabLayout.setupWithViewPager(viewPager);
                        break;
                    default:
                        // Nothing to do
                }
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager,1);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        btnSignUp= (Button) findViewById(R.id.btnSignUp);
        btnSignIn= (Button) findViewById(R.id.btnSignIn);

        imgBack =(ImageView) findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });



    }



    private void setupViewPager(ViewPager viewPager, int position) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        if(position==0){
            adapter.addFragment(new CustomerRegFragment(), "UTENTE");
            adapter.addFragment(new CouriorRegFragment(), "CORRIERE");
            adapter.addFragment(new RestaurantRegFragment(), "RISTORANTE");
        }else{
            adapter.addFragment(new CustomerLoginFragment(), "UTENTE");
            adapter.addFragment(new CourierLoginFragment(), "CORRIERE");
            adapter.addFragment(new RestaurantLoginFragment(), "RISTORANTE");
        }

        viewPager.setAdapter(adapter);


    }
class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
