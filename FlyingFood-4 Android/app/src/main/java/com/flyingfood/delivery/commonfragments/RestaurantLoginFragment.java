package com.flyingfood.delivery.commonfragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerForgotPasswordActivity;
import com.flyingfood.delivery.customeractivities.CustomerIndirizziSalvatiActivity;
import com.flyingfood.delivery.model.LoginResponse;
import com.flyingfood.delivery.restaurantactivities.RestaurantHomeActivity;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;
//import com.flyingfood.delivery.restaurantactivities.RestorantHomeActivity;


public class RestaurantLoginFragment extends Fragment {

    public RestaurantLoginFragment() {
        // Required empty public constructor
    }

    private TextView txtForgottenEmail;
    private EditText edtUserName,edtConfirmPassword;

    private Button btnLogin,btnSignInFacebook;
    // fb
    private static final String TAG = "Nirav";
    private CallbackManager callbackManager;
    private LoginButton loginButton;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View convertView= inflater.inflate(R.layout.fragment_restaurant, container, false);
        edtUserName= (EditText) convertView.findViewById(R.id.edtUserName);
        edtConfirmPassword= (EditText) convertView.findViewById(R.id.edtConfirmPassword);

        txtForgottenEmail= (TextView) convertView.findViewById(R.id.txtForgottenEmail);
        txtForgottenEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(), CustomerForgotPasswordActivity.class);
                startActivity(i);
            }
        });

        btnSignInFacebook= (Button) convertView.findViewById(R.id.btnSignInFacebook);
        loginButton= (LoginButton)convertView.findViewById(R.id.login_button);


        btnLogin= (Button) convertView.findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edtUserName.getText().length()==0){
                    Toast.makeText(getActivity(), "Please Enter UserName", Toast.LENGTH_SHORT).show();
                }else if(edtConfirmPassword.getText().toString().length()==0){
                    Toast.makeText(getActivity(), "Please Enter Password", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation();

                }




            }
        });
        return convertView;
    }

    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                try {
                    registrationObject.put("DeviceToken", PrefUtils.getToken(getActivity()).token);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                registrationObject.put("DeviceType", "2");
                registrationObject.put("Password", edtConfirmPassword.getText().toString().trim());
                registrationObject.put("RoleMasterID", "2");
                registrationObject.put("Username", edtUserName.getText().toString().trim());
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.LOGIN, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    LoginResponse submitResponse = new GsonBuilder().create().fromJson(response, LoginResponse.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                        PrefUtils.setUser(submitResponse,getActivity());
                        Intent i=new Intent(getActivity(), RestaurantHomeActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void doPostOperation(String id,String username,String email) {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {

                registrationObject.put("Email", username+"");
                registrationObject.put("FacebookID", id);
                registrationObject.put("Mobile","");
                registrationObject.put("Name", username+"");
                registrationObject.put("DeviceToken", PrefUtils.getToken(getActivity()).token);
                registrationObject.put("DeviceType", "2");
                registrationObject.put("Password", "");
                registrationObject.put("RoleMasterID", "2");
                registrationObject.put("Username", username);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.REGISTRATION, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    LoginResponse submitResponse = new GsonBuilder().create().fromJson(response, LoginResponse.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                        PrefUtils.setUser(submitResponse,getActivity());
                        Intent i=new Intent(getActivity(), CustomerIndirizziSalvatiActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }

    @Override
    public void onResume() {
        super.onResume();


        callbackManager= CallbackManager.Factory.create();



        loginButton.setReadPermissions("public_profile", "email","user_friends");
        loginButton.setFragment(this);
        btnSignInFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isNetworkConnected()){
                    loginButton.performClick();

                    loginButton.setPressed(true);

                    loginButton.invalidate();

                    loginButton.registerCallback(callbackManager, mCallBack);

                    loginButton.setPressed(false);

                    loginButton.invalidate();
                }else {
                    Toast.makeText(getActivity(), "No internet connection available...please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("callback called","yes");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }



    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            Log.e("callback called","yes");

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.e("in facebook response","yes");
                            Log.e("response: ", response + "");

                            try {
                                doPostOperation(object.getString("id").toString(),object.getString("name").toString(),object.getString("email").toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
//            progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
//            progressDialog.dismiss();
            Log.e("error",e.getMessage());
        }
    };





}
