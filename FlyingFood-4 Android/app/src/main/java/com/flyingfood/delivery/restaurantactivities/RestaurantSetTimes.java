package com.flyingfood.delivery.restaurantactivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.util.Locale.*;

public class RestaurantSetTimes extends AppCompatActivity {

    Spinner sp_lunch_start_time, sp_lunch_end_time, sp_dinner_start_time, sp_dinner_end_time;
    Spinner sp_day_of_week;
    String currentDay = "Monday";
    private String[] daysOfWeek = {"Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica" };
    final String[] actualValues = {"Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
    private String currentLunchStartTime, currentLunchEndTime, currentDinnerStartTime, currentDinnerEndTime;
    private String[] timesArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_set_times);

        Button btnSave = (Button) findViewById(R.id.btnUpdateRestaurantTimes) ;

        sp_lunch_start_time = (Spinner) findViewById(R.id.sp_lunch_start_time);
        sp_lunch_end_time = (Spinner) findViewById(R.id.sp_lunch_end_time);
        sp_dinner_start_time = (Spinner) findViewById(R.id.sp_dinner_start_time);
        sp_dinner_end_time = (Spinner) findViewById(R.id.sp_dinner_end_time);
        sp_day_of_week = (Spinner) findViewById(R.id.sp_dayOfWeek);
        setTimesSpinner();





        ArrayAdapter spinnerAdapter = new CustomArrayAdapter(this,
                 daysOfWeek);
        sp_day_of_week.setAdapter(spinnerAdapter);

        sp_day_of_week.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                currentDay = actualValues[arg2];

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkConnected()) {

                    final ProgressDialog progressDialog = new ProgressDialog(RestaurantSetTimes.this);
                    progressDialog.setMessage("loading.....");
                    progressDialog.setCancelable(false);
                    progressDialog.show();


                    JSONObject registrationObject = new JSONObject();



                    try {
                        registrationObject.put("DayOfWeek", currentDay);
                        registrationObject.put("DinnerEndTime", currentDinnerEndTime);
                        registrationObject.put("DinnerStartTime", currentDinnerStartTime);
                        registrationObject.put("LunchEndTime", currentLunchEndTime);
                        registrationObject.put("LunchStartTime", currentLunchStartTime);
                        registrationObject.put("RestaurantId", PrefUtils.getUser(RestaurantSetTimes.this).RestaurantId+"");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.e(" request object", registrationObject + "");


                    PostServiceCall serviceCall = new PostServiceCall(AppConstants.SetOpenTimeRestaurant, registrationObject) {
                        @Override
                        public void response(String response) {
                            progressDialog.dismiss();

                            CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                            if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                                Toast.makeText(getApplicationContext(), "Orario di " + currentDay + " aggiornato con successo", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("login response...", response + "");


                        }

                        @Override
                        public void error(String error) {
                            progressDialog.dismiss();
                            Log.e("login response...", error + "");
                        }
                    };
                    serviceCall.call();






                }
            }
        });
        setToolbar();

    }

    private void setTimesSpinner()
    {
        List<String> times = new ArrayList<>();
        try
        {
            final long ONE_MINUTE_IN_MILLIS=60000;

            DateFormat formatter = new SimpleDateFormat("h.mm a", Locale.US);
            Date initialHour = formatter.parse("8.00 AM");
            Date finalHour = formatter.parse("11.59 PM");
            long t= initialHour.getTime();

            while(initialHour.compareTo(finalHour) == -1 )
            {
                times.add(formatter.format(initialHour));
                initialHour = new Date(initialHour.getTime() + (30 * ONE_MINUTE_IN_MILLIS));
            }

        }catch (Exception e)
        {
            finish();
        }
        times.add("CHIUSO");
        timesArray = times.toArray(new String[0]);
        ArrayAdapter spinnerAdapter = new CustomArrayAdapter(this,
                timesArray);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_dinner_start_time.setAdapter(spinnerAdapter);


        sp_dinner_start_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (!timesArray[arg2].equals("CHIUSO"))
                    currentDinnerStartTime = timesArray[arg2];
                else
                    currentDinnerStartTime = "0";

            }



            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        sp_lunch_start_time.setAdapter(spinnerAdapter);

        sp_lunch_start_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (!timesArray[arg2].equals("CHIUSO"))
                    currentLunchStartTime = timesArray[arg2];
                else
                    currentLunchStartTime = "0";

            }



            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        sp_dinner_end_time.setAdapter(spinnerAdapter);

        sp_dinner_end_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (!timesArray[arg2].equals("CHIUSO"))
                    currentDinnerEndTime = timesArray[arg2];
                else
                    currentDinnerEndTime = "0";

            }



            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        sp_lunch_end_time.setAdapter(spinnerAdapter);

        sp_lunch_end_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (!timesArray[arg2].equals("CHIUSO"))
                    currentLunchEndTime = timesArray[arg2];
                else
                    currentLunchEndTime = "0";

            }



            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });



    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Add Order");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

    }

    static class CustomArrayAdapter<T> extends ArrayAdapter<T>
    {
        public CustomArrayAdapter(Context ctx, T [] objects)
        {
            super(ctx, android.R.layout.simple_spinner_item, objects);
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            View view = super.getView(position, convertView, parent);

            TextView text = (TextView)view.findViewById(android.R.id.text1);
            text.setTextColor(Color.WHITE);
            text.setPadding(0,4,4,0);



            return view;

        }
    }

}
