package com.flyingfood.delivery.model.Account;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 04/07/17.
 */

public class GetPaymentDetails {


    @SerializedName("CVV")
    public String CVV;

    @SerializedName("CardNumber")
    public String CardNumber;

    @SerializedName("ExpiryDate")
    public String ExpiryDate;

    @SerializedName("PaymentDetailId")
    public String PaymentDetailId;


}
