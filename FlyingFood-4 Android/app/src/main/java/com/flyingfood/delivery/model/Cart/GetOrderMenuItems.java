package com.flyingfood.delivery.model.Cart;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 01/07/17.
 */

public class GetOrderMenuItems {

    @SerializedName("CourierName")
    public String CourierName;

    @SerializedName("CourierUserID")
    public String CourierUserID;

    @SerializedName("CustomerUserID")
    public String CustomerUserID;

    @SerializedName("DeliveryAddress")
    public String DeliveryAddress;

    @SerializedName("DeliveryTime")
    public String DeliveryTime;

    @SerializedName("Description")
    public String Description;

    @SerializedName("Email")
    public String Email;

    @SerializedName("MenuName")
    public String MenuName;

    @SerializedName("Mobile")
    public String Mobile;

    @SerializedName("Name")
    public String Name;

    @SerializedName("OrderDate")
    public String OrderDate;

    @SerializedName("OrderID")
    public String OrderID;

    @SerializedName("OrderNumber")
    public String OrderNumber;

    @SerializedName("OrderStatusMasterID")
    public String OrderStatusMasterID;


    @SerializedName("Price")
    public String Price;

    @SerializedName("Quntity")
    public String Quntity;

    @SerializedName("RestaurantId")
    public String RestaurantId;

    @SerializedName("RestaurantName")
    public String RestaurantName;

    @SerializedName("RestaurantUserID")
    public String RestaurantUserID;

    @SerializedName("UserID")
    public String UserID;

}
