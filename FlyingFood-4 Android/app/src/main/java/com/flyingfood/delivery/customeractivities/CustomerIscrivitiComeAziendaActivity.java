package com.flyingfood.delivery.customeractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerIscrivitiComeAziendaActivity extends AppCompatActivity {

    private Button btnLogin;

    private EditText etMessage,etName,etEmail,etMobile,etCompanyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_iscriviti_come_azienda);
        btnLogin= (Button) findViewById(R.id.btnLogin);

        etMessage= (EditText) findViewById(R.id.etMessage);
        etName= (EditText) findViewById(R.id.etName);
        etEmail= (EditText) findViewById(R.id.etEmail);
        etMobile= (EditText) findViewById(R.id.etMobile);
        etCompanyName= (EditText) findViewById(R.id.etCompanyName);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 if(etCompanyName.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscrivitiComeAziendaActivity.this, "Please Enter Company Name", Toast.LENGTH_SHORT).show();
                }else if(etName.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscrivitiComeAziendaActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
                }else  if(etMobile.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscrivitiComeAziendaActivity.this, "Please Enter Mobile", Toast.LENGTH_SHORT).show();
                }else  if(etEmail.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscrivitiComeAziendaActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                }else  if(etMessage.getText().toString().trim().length()==0){
                    Toast.makeText(CustomerIscrivitiComeAziendaActivity.this, "Please Enter Description", Toast.LENGTH_SHORT).show();
                }else{
                    doPostOperation();
                }

            }
        });

        setToolbar();

    }


    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerIscrivitiComeAziendaActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();
            
            try {
                registrationObject.put("CompanyName", etCompanyName.getText().toString().trim());
                registrationObject.put("Description", etMessage.getText().toString().trim());
                registrationObject.put("Email", etEmail.getText().toString().trim());
                registrationObject.put("Name", etName.getText().toString().trim());
                registrationObject.put("Phone", etMobile.getText().toString().trim());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.ADD_COMPANY, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CustomerIscrivitiComeAziendaActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CustomerIscrivitiComeAziendaActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }
    }



    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Iscriviti come azienda");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }


}
