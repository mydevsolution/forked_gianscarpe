package com.flyingfood.delivery.model.Account;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 03/07/17.
 */

public class GetMessage {



    @SerializedName("DateGenerated")
    public String DateGenerated;

    @SerializedName("MessageID")
    public String MessageID;

    @SerializedName("OrderID")
    public String OrderID;

    @SerializedName("ParentMessageID")
    public String ParentMessageID;

    @SerializedName("ReceiverID")
    public String ReceiverID;

    @SerializedName("Response")
    public String Response;

    @SerializedName("SenderID")
    public String SenderID;

    @SerializedName("Text")
    public String Text;

}
