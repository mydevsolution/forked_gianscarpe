package com.flyingfood.delivery.courieractivities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.CommonChatActivity;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerOrderDetailActivity;
import com.flyingfood.delivery.model.Cart.GetOrderMenuItems;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseData;
import com.flyingfood.delivery.model.CommonResponse;
import com.flyingfood.delivery.restaurantactivities.RestaurantOrderDetailActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

public class CourierOrderDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private ImageView imgChat, imgGo, imagMenu;
    TextView txtNameSurname, txtAddress, txtTelephone, txtEmail, txtRestaurantDetails, txtRestaurantTime, txtRestaurantName, txtDeliveryTime,txtDes;
    String orderId, name, email, mobile;
    LinearLayout llMenuItem;
    String orderStatus;
    String url;
    LatLng from;
    private Double myLatitude, myLongitude;
    private GoogleMap mymap;
    private GetOrderUserIdWiseData getOrderUserIdWiseData;
   // private CourierOrderDetailMenuItemActivity.MyCustomAdapter myCustomAdapter;
    private ArrayList<GetOrderMenuItems> profileListView;


    public static JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            address = address.replaceAll(" ", "%20");
            Log.e("address url", "http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();


            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    public LatLng getLatLong(JSONObject jsonObject) {
        LatLng l = null;
        try {

            myLongitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");

            myLatitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");
            l = new LatLng(myLatitude, myLongitude);

        } catch (JSONException e) {


        }

        return l;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier_order_detail);



        txtDes= (TextView) findViewById(R.id.txtDes);
        initMap();
        Log.e("address", getIntent().getStringExtra("address_complete"));
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                from = getLatLong(getLocationInfo(getIntent().getStringExtra("address_complete")));
                Log.e("lat lng", from + "");

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                new CountDownTimer(1000, 1000) {

                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        LatLng latLng = null;
                        try {
                            latLng = new LatLng(from.latitude, from.longitude);
                            mymap.addMarker(new MarkerOptions().position(latLng).title(getIntent().getStringExtra("RetaurantName")));
//                        mymap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                            mymap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            mymap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }.start();

            }
        }.execute();

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
        getOrderUserIdWiseData = PrefUtils.getRestaurantOrderHistory(CourierOrderDetailActivity.this);
        profileListView = getOrderUserIdWiseData.getOrderMenuItemsArrayList;

        imgChat = (ImageView) findViewById(R.id.imgChat);
        imgGo = (ImageView) findViewById(R.id.imgGo);
        imagMenu = (ImageView) findViewById(R.id.imagMenu);

        //profileListView =  ArrayList<GetOrderMenuItems> getOrderMenuItemsArrayList;

        imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CourierOrderDetailActivity.this, CommonChatActivity.class);
                //  Intent i = new Intent(CourierOrderDetailActivity.this, CourierChatActivity.class);
                i.putExtra("order_id",profileListView.get(0).OrderID+"");
                i.putExtra("rec_id",profileListView.get(0).CustomerUserID+"");
                i.putExtra("send_id",PrefUtils.getUser(CourierOrderDetailActivity.this).UserID+"");
                Log.e("order_id",profileListView.get(0).OrderID+"");
                Log.e("rec_id",profileListView.get(0).CustomerUserID+"");
                Log.e("send_id",PrefUtils.getUser(CourierOrderDetailActivity.this).UserID+"");
                startActivity(i);
            }
        });
        txtRestaurantName = (TextView) findViewById(R.id.txtRestaurantName);


        txtNameSurname = (TextView) findViewById(R.id.txtNameSurname);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtTelephone = (TextView) findViewById(R.id.txtTelephone);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
//        txtRestaurantDetails = (TextView) findViewById(R.id.txtRestaurantDetails);
        txtRestaurantTime = (TextView) findViewById(R.id.txtRestaurantTime);
        txtDeliveryTime = (TextView) findViewById(R.id.txtDeliveryTime);

        txtAddress.setText(getIntent().getStringExtra("Address"));
        txtRestaurantName.setText(getIntent().getStringExtra("RetaurantName"));
        txtDeliveryTime.setText(getIntent().getStringExtra("DeliveryTime"));
        orderId = getIntent().getStringExtra("order_id");
        txtNameSurname.setText(getIntent().getStringExtra("Name"));
        txtTelephone.setText(getIntent().getStringExtra("Mobile"));
        txtEmail.setText(getIntent().getStringExtra("Email"));
        txtDes.setText(getOrderUserIdWiseData.OrderDescription);
        orderStatus = getIntent().getStringExtra("order_status");
        if (orderStatus.equalsIgnoreCase("1")) {
            imgGo.setBackgroundResource(R.drawable.btngo);
        } else if (orderStatus.equalsIgnoreCase("2")) {
            imgGo.setBackgroundResource(R.drawable.deliver);
        } else if (orderStatus.equalsIgnoreCase("3")) {
            imgGo.setBackgroundResource(R.drawable.delivered);
        }
        llMenuItem = (LinearLayout) findViewById(R.id.llMenuItem);
      //  profileListView = getOrderUserIdWiseData.getOrderMenuItemsArrayList;

        llMenuItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent i = new Intent(CourierOrderDetailActivity.this, CourierOrderDetailMenuItemActivity.class);
                //  Intent i = new Intent(CourierOrderDetailActivity.this, CourierChatActivity.class);
                startActivity(i);
            }
        });

//        name=getIntent().getStringExtra("Name");
//        email=getIntent().getStringExtra("Email");
//        mobile=getIntent().getStringExtra("Mobile");

        //  String orderid= getIntent().getStringExtra("order_id");

        imgGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (orderStatus.equalsIgnoreCase("1")) {
                    url = AppConstants.AcceptOrderCourier;
                    doPostOperation(url);
                } else if (orderStatus.equalsIgnoreCase("2")) {
                    url = AppConstants.CompleteOrderCourier;
                    doPostOperation(url);
                } else if (orderStatus.equalsIgnoreCase("3")) {

                }

            }
        });

        setToolbar();


        java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm a");
        java.util.Date date1 = null;
        try {
            date1 = df.parse(getIntent().getStringExtra("DeliveryTime"));
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            cal.add(Calendar.HOUR,0);
            cal.add(Calendar.MINUTE,-15);
            if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
                txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" PM");
            }else{
                txtRestaurantTime.setText(cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+" AM");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    private void initMap() {

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap map) {
        mymap = map;

    }

    private void doPostOperation(String url) {

        if (isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CourierOrderDetailActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();


            try {
                registrationObject.put("CourierMasterId", PrefUtils.getUser(CourierOrderDetailActivity.this).UserID);
                registrationObject.put("OrderId", orderId);

                try {
                    registrationObject.put("DeviceToken", PrefUtils.getToken(CourierOrderDetailActivity.this).token);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(url, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();

                    CommonResponse submitResponse = new GsonBuilder().create().fromJson(response, CommonResponse.class);

                    if (submitResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(CourierOrderDetailActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();

                        finish();
                    } else {
                        Toast.makeText(CourierOrderDetailActivity.this, submitResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                    Log.e("login response...", response + "");
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            //  toolbar.setTitle(getIntent().getStringExtra("menu_name"));
            toolbar.setTitle("Ordine #" + getIntent().getStringExtra("order_id") + " Details");
            //    setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

//    public void onMapReady(GoogleMap googleMap) {
//        myMap = googleMap;
//        // Add a marker in Sydney, Australia,
//        // and move the map's camera to the same location.
//        LatLng sydney = new LatLng(22.31, 73.17);
//        myMap.addMarker(new MarkerOptions().position(sydney)
//                .title("nkDroid Solutions"));
//        myMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//
//
//    }

}
