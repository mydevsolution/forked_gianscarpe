package com.flyingfood.delivery.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 30/06/17.
 */

public class CommonResponse {


    @SerializedName("ResponseCode")
    public String ResponseCode;
    @SerializedName("ResponseMsg")
    public String ResponseMsg;
}
