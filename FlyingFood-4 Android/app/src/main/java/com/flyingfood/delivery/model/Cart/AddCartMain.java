package com.flyingfood.delivery.model.Cart;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 04/07/17.
 */

public class AddCartMain {

    @SerializedName("Response")
    public CommonResponse commonResponse;

    @SerializedName("ResponseData")
    public AddCartData addCartData;
}
