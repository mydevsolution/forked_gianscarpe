package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.CommonChatActivity;

import com.flyingfood.delivery.courierfragments.CourierAccountFragment;
import com.flyingfood.delivery.courierfragments.CourierChatFragment;
import com.flyingfood.delivery.courierfragments.CourierHomeFragment;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.GetServiceCall;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customerfragments.CustomerAccountFragment;
import com.flyingfood.delivery.customerfragments.CustomerCartFragment;
import com.flyingfood.delivery.customerfragments.CustomerChatFragment;
import com.flyingfood.delivery.customerfragments.CustomerHomeFragment;
import com.flyingfood.delivery.customerfragments.CustomerOrderFragment;
import com.flyingfood.delivery.model.Account.GetAddressMain;
import com.flyingfood.delivery.model.LoginResponse;
import com.flyingfood.delivery.model.MyAddress;
import com.flyingfood.delivery.model.Restaurant.GetFood;
import com.flyingfood.delivery.model.Restaurant.GetFoodMain;
import com.flyingfood.delivery.model.Restaurant.GetFoodRestaurantWise;
import com.flyingfood.delivery.model.Restaurant.GetRestaurantFoodWiseMain;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.GsonBuilder;
import com.google.maps.android.SphericalUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CustomerHomeActivity extends AppCompatActivity {
    private TextView spinnerCustom;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    // Make sure to be using android.support.v7.app.ActionBarDrawerToggle version.
    // The android.support.v4.app.ActionBarDrawerToggle has been deprecated.
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_home);



        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();

        // Tie DrawerLayout events to the ActionBarToggle
        mDrawer.addDrawerListener(drawerToggle);


        // Find our drawer view
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        View header = nvDrawer.getHeaderView(0);


        TextView txtAccountName= (TextView) header.findViewById(R.id.txtAccountName);
        txtAccountName.setText(PrefUtils.getUser(CustomerHomeActivity.this).Username);


        // Setup drawer view
        setupDrawerContent(nvDrawer);
        setFirstItem();




    }

    @Override
    protected void onResume() {
        super.onResume();
        setToolbar();
    }

    private void setFirstItem() {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass;


        fragmentClass = CustomerHomeFragment.class;


        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        nvDrawer.getMenu().getItem(0).setChecked(true);
        // Set action bar title
        setTitle(nvDrawer.getMenu().getItem(0).getTitle());
        // Close the navigation drawer
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (menuItem.getItemId()) {
            case R.id.nav_home_fragment:
                toolbar.setTitle("");
                spinnerCustom.setText(PrefUtils.getAddress(CustomerHomeActivity.this).addressName);
                spinnerCustom.setVisibility(View.VISIBLE);
                fragmentClass = CustomerHomeFragment.class;
                break;
            case R.id.nav_order_fragment:
                toolbar.setTitle("Order");
                spinnerCustom.setVisibility(View.GONE);
                fragmentClass = CustomerOrderFragment.class;
                break;
            case R.id.nav_chat_fragment:
                toolbar.setTitle("Chat");
                spinnerCustom.setVisibility(View.GONE);
                fragmentClass = CustomerChatFragment.class;
                break;

            case R.id.nav_account_fragment:
                toolbar.setTitle("Account");
                spinnerCustom.setVisibility(View.GONE);
                fragmentClass = CustomerAccountFragment.class;
                break;
            case R.id.nav_cart_fragment:
                toolbar.setTitle("Cart");
                spinnerCustom.setVisibility(View.GONE);
                fragmentClass = CustomerCartFragment.class;
                break;
            default:
                toolbar.setTitle("");
                spinnerCustom.setText(PrefUtils.getAddress(CustomerHomeActivity.this).addressName);
                spinnerCustom.setVisibility(View.VISIBLE);
                fragmentClass = CustomerHomeFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        spinnerCustom= (TextView) toolbar.findViewById(R.id.spinnerCustom);
                        spinnerCustom.setText(PrefUtils.getAddress(CustomerHomeActivity.this).addressName);
                spinnerCustom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i=new Intent(CustomerHomeActivity.this,CustomerIndirizziSalvatiActivity.class);
                        i.putExtra("is_checkout",true);
                        startActivity(i);
                    }
                });
        if (toolbar != null) {

            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

    }


    public void updateToolbar(){

    }



}
