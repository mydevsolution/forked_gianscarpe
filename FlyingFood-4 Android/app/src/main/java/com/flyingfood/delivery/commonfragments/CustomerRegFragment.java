package com.flyingfood.delivery.commonfragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.courieractivities.CourierHomeActivity;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.customeractivities.CustomerHomeActivity;
import com.flyingfood.delivery.customeractivities.CustomerIndirizziSalvatiActivity;
import com.flyingfood.delivery.model.LoginResponse;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomerRegFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button btnSignUp;

    private EditText edtUserName,edtPassword,edtConfirmPassword,edtEmail,edtName;


    private Button btnSignInFacebook;

    // fb
    private static final String TAG = "Nirav";
    private CallbackManager callbackManager;
    private LoginButton loginButton;



    public CustomerRegFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CustomerRegFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CustomerRegFragment newInstance(String param1, String param2) {
        CustomerRegFragment fragment = new CustomerRegFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View convertView= inflater.inflate(R.layout.fragment_customer_reg, container, false);
        edtUserName= (EditText) convertView.findViewById(R.id.edtUserName);
        edtPassword= (EditText) convertView.findViewById(R.id.edtPassword);
        edtConfirmPassword= (EditText) convertView.findViewById(R.id.edtConfirmPassword);
        edtEmail= (EditText) convertView.findViewById(R.id.edtEmail);
        edtName= (EditText) convertView.findViewById(R.id.edtName);



        btnSignInFacebook= (Button) convertView.findViewById(R.id.btnSignInFacebook);
        loginButton= (LoginButton)convertView.findViewById(R.id.login_button);


        btnSignUp= (Button) convertView.findViewById(R.id.btnSignUp);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edtName.getText().toString().length()==0){
                    Toast.makeText(getActivity(), "Please Enter Name", Toast.LENGTH_SHORT).show();
                }else if(edtEmail.getText().toString().length()==0){
                    Toast.makeText(getActivity(), "Please Enter Email", Toast.LENGTH_SHORT).show();
                }else if(edtUserName.getText().toString().length()==0){
                    Toast.makeText(getActivity(), "Please Enter Username", Toast.LENGTH_SHORT).show();
                }else if(edtPassword.getText().toString().length()==0){
                    Toast.makeText(getActivity(), "Please Enter Password", Toast.LENGTH_SHORT).show();
                }else if(edtConfirmPassword.getText().toString().length()==0){
                    Toast.makeText(getActivity(), "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
                }else if(!(edtPassword.getText().toString().equalsIgnoreCase(edtConfirmPassword.getText().toString()))){
                    Toast.makeText(getActivity(), "Password must be match", Toast.LENGTH_SHORT).show();
                }else {

                    doPostOperation();

                }

            }
        });
        return convertView;
    }

    private void doPostOperation() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {


                registrationObject.put("Email", edtEmail.getText().toString().trim()+"");
                registrationObject.put("FacebookID", "");
                registrationObject.put("Mobile","");
                registrationObject.put("Name", edtName.getText().toString().trim()+"");
                registrationObject.put("DeviceToken", PrefUtils.getToken(getActivity()).token);
                registrationObject.put("DeviceType", "2");
                registrationObject.put("Password", edtConfirmPassword.getText().toString().trim());
                registrationObject.put("RoleMasterID", "3");
                registrationObject.put("Username", edtUserName.getText().toString().trim());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("registration request ", registrationObject + "");

            new PostServiceCall(AppConstants.REGISTRATION, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("registration response", response + "");
                    LoginResponse submitResponse = new GsonBuilder().create().fromJson(response, LoginResponse.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                        PrefUtils.setUser(submitResponse,getActivity());
                        Intent i=new Intent(getActivity(), CustomerIndirizziSalvatiActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("registration response", error + "");
                }
            }.call();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void doPostOperation(String id,String username,String email) {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();



            try {

                registrationObject.put("Email", email+"");
                registrationObject.put("FacebookID", id);
                registrationObject.put("Mobile","");
                registrationObject.put("Name", username+"");
                registrationObject.put("DeviceToken", PrefUtils.getToken(getActivity()).token);
                registrationObject.put("DeviceType", "2");
                registrationObject.put("Password", "");
                registrationObject.put("RoleMasterID", "3");
                registrationObject.put("Username", username);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("registration request ", registrationObject + "");

            new PostServiceCall(AppConstants.REGISTRATION, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("registration response", response + "");
                    LoginResponse submitResponse = new GsonBuilder().create().fromJson(response, LoginResponse.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                        PrefUtils.setUser(submitResponse,getActivity());
                        Intent i=new Intent(getActivity(), CustomerIndirizziSalvatiActivity.class);
                        startActivity(i);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("registration response", error + "");
                }
            }.call();

        }

    }



    @Override
    public void onResume() {
        super.onResume();


        callbackManager= CallbackManager.Factory.create();



        loginButton.setReadPermissions("public_profile", "email","user_friends");
        loginButton.setFragment(this);
        btnSignInFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isNetworkConnected()){
                    loginButton.performClick();

                    loginButton.setPressed(true);

                    loginButton.invalidate();

                    loginButton.registerCallback(callbackManager, mCallBack);

                    loginButton.setPressed(false);

                    loginButton.invalidate();
                }else {
                    Toast.makeText(getActivity(), "No internet connection available...please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("callback called","yes");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }



    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            Log.e("callback called","yes");

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            Log.e("in facebook response","yes");
                            Log.e("response: ", response + "");

                            try {
                                doPostOperation(object.getString("id").toString(),object.getString("name").toString(),object.getString("email").toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
//            progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
//            progressDialog.dismiss();
            Log.e("error",e.getMessage());
        }
    };




}
