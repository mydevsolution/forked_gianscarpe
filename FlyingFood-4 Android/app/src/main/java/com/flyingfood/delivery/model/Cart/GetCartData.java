package com.flyingfood.delivery.model.Cart;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 04/07/17.
 */

public class GetCartData {


    @SerializedName("DinnerEndTime")
    public String DinnerEndTime;

    @SerializedName("DinnerStartTime")
    public String DinnerStartTime;

    @SerializedName("Latitude")
    public String Latitude;

    @SerializedName("Longitude")
    public String Longitude;

    @SerializedName("LunchEndTime")
    public String LunchEndTime;

    @SerializedName("LunchStartTime")
    public String LunchStartTime;

    @SerializedName("RestaurantId")
    public String RestaurantId;

    @SerializedName("RestaurantImage")
    public String RestaurantImage;

    @SerializedName("RestaurantName")
    public String RestaurantName;

    @SerializedName("Timing")
    public String Timing;

    public double TotalAmount=0.0;

    @SerializedName("MenuItems")
    public ArrayList<CartItem> cartItemArrayList;


}
