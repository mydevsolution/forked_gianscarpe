package com.flyingfood.delivery.restaurantfragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.MainActivity;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.restaurantactivities.RestaurantAccountActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantCourierRequestActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantMenuActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantOrderHistoryActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantSetCloseActivity;
import com.flyingfood.delivery.restaurantactivities.RestaurantSetTimes;
import com.flyingfood.delivery.restaurantactivities.RestaurantWatingTimeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantAccountFragment extends Fragment {

    TextView txtLogout;
    LinearLayout llWaitingTime,llMenu,llOrderHistory, llSetCloseRestaurant, llSetTimes;

    public RestaurantAccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_restaurant_account, container, false);
        llWaitingTime= (LinearLayout) convertView.findViewById(R.id.llWaitingTime);
        llMenu= (LinearLayout) convertView.findViewById(R.id.llMenu);
        llOrderHistory= (LinearLayout) convertView.findViewById(R.id.llOrderHistory);
        llSetCloseRestaurant = (LinearLayout) convertView.findViewById(R.id.llSetRestaurantClose) ;
        llSetTimes = (LinearLayout) convertView.findViewById(R.id.llSetTimes) ;
        llWaitingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),RestaurantWatingTimeActivity.class);
                startActivity(intent);


            }
        });
        llMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),RestaurantMenuActivity.class);
                startActivity(intent);


            }
        });


        llOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),RestaurantOrderHistoryActivity.class);
                startActivity(intent);


            }
        });

        llSetCloseRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RestaurantSetCloseActivity.class);
                startActivity(intent);


            }
        });

        llSetTimes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent intent = new Intent(getActivity(), RestaurantSetTimes.class);
                    startActivity(intent);
            }

        });
        return convertView;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater. inflate(R.menu.menu_account_logout_button, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_account_logout_button, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnLogOut:

                PrefUtils.clearCurrentUser(getActivity());

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
