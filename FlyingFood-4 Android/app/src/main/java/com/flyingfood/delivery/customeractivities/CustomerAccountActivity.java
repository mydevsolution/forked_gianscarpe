package com.flyingfood.delivery.customeractivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.MainActivity;
import com.flyingfood.delivery.custom.PrefUtils;

public class CustomerAccountActivity extends AppCompatActivity {
    LinearLayout llAccount, llPaymentCard, llLocation, llMessageSupport, llFedel, llHouse, llCourier, llCompany;
    private TextView txtAccountName;
    TextView txtLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_account);
        txtAccountName = (TextView) findViewById(R.id.txtAccountName);
        txtAccountName.setText(PrefUtils.getUser(CustomerAccountActivity.this).Username + " Account");

        llAccount = (LinearLayout) findViewById(R.id.llAccount);
        llPaymentCard = (LinearLayout) findViewById(R.id.llPaymentCard);
        llLocation = (LinearLayout) findViewById(R.id.llLocation);
        llMessageSupport = (LinearLayout) findViewById(R.id.llMessageSupport);
        llFedel = (LinearLayout) findViewById(R.id.llFedel);
        llHouse = (LinearLayout) findViewById(R.id.llHouse);
        llCourier = (LinearLayout) findViewById(R.id.llCourier);
        llCompany = (LinearLayout) findViewById(R.id.llCompany);

        llLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerIndirizziSalvatiActivity.class);
                startActivity(i);

            }
        });

        llFedel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerRewardActivity.class);
                startActivity(i);

            }
        });
        llPaymentCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerPaymentDetailsActivity.class);
                startActivity(i);

            }
        });


        llMessageSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerRichiediSupportoActivity.class);
                startActivity(i);

            }
        });


        llHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerIscriviRistoranteActivity.class);
                startActivity(i);

            }
        });


        llCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerIscrivitiComeAziendaActivity.class);
                startActivity(i);

            }
        });

        llCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerDiventaUnCorriereActivity.class);
                startActivity(i);

            }
        });


        llAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerAccountActivity.this, CustomerInformationAccountActivity.class);
                startActivity(i);

            }
        });


//        txtLogout = (TextView) findViewById(R.id.txtLogout);
//        txtLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PrefUtils.clearCurrentUser(CustomerAccountActivity.this);
//                Intent intent = new Intent(CustomerAccountActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//
//            }
//        });

        setToolbar();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account_logout_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnLogOut:
                PrefUtils.clearRandom(CustomerAccountActivity.this);
                PrefUtils.clearCurrentUser(CustomerAccountActivity.this);

                LoginManager.getInstance().logOut();
                Intent intent = new Intent(CustomerAccountActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Account");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
