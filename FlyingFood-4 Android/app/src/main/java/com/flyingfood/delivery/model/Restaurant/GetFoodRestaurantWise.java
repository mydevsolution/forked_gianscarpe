package com.flyingfood.delivery.model.Restaurant;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 01/07/17.
 */

public class GetFoodRestaurantWise {

    @SerializedName("Address")
    public String Address;

    @SerializedName("Area")
    public String Area;

    @SerializedName("Category")
    public String Category;

    @SerializedName("City")
    public String City;

    @SerializedName("Country")
    public String Country;

    @SerializedName("DinnerEndTime")
    public String DinnerEndTime;

    @SerializedName("DinnerStartTime")
    public String DinnerStartTime;

    @SerializedName("Latitude")
    public String Latitude;

    @SerializedName("Longitude")
    public String Longitude;

    @SerializedName("LunchEndTime")
    public String LunchEndTime;

    @SerializedName("LunchStartTime")
    public String LunchStartTime;

    @SerializedName("Ratting")
    public String Ratting;

    @SerializedName("RestaurantID")
    public String RestaurantID;

    @SerializedName("RestaurantImage")
    public String RestaurantImage;

    @SerializedName("RestaurantName")
    public String RestaurantName;

    @SerializedName("State")
    public String State;

    @SerializedName("Timing")
    public String Timing;

    @SerializedName("ZIP")
    public String ZIP;


    public Double distance;

}
