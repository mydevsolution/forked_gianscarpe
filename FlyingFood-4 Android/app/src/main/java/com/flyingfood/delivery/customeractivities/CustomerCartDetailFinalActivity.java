package com.flyingfood.delivery.customeractivities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.GetServiceCall;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Cart.CartItem;
import com.flyingfood.delivery.model.Cart.GetCartDataList;
import com.flyingfood.delivery.model.ForgotPasswordResponse;
import com.flyingfood.delivery.model.TransactionResponse;
import com.flyingfood.delivery.model.storage.RandomNumberModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.GsonBuilder;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CustomerCartDetailFinalActivity extends AppCompatActivity implements OnMapReadyCallback {

    Card cardToSave;

    LinearLayout llVia, llOradio, llPragmento, llCheckout;
    private GoogleMap mymap;
    private TextView txtOrderRuppes, txtSelectTime, txtAddress;
    private GetCartDataList getCartDataList;
    double finalTotal = 0.0;
    double total = 0.0;
    RandomNumberModel randomNumberModel = null;
    private ArrayList<CartItem> profileListView;
    String timeValue;
    CardInputWidget mCardInputWidget;
    LatLng from;
    private Double myLatitude, myLongitude;
    ProgressDialog progressDialog;
    ArrayList<String> timingList=new ArrayList<>();
    private EditText etDetail,etMobile;
    Calendar myCal;
    Date myDate;
    String myDateString;
    private int position;
    private void setTotal() {
        for (int i = 0; i < profileListView.size(); i++) {
            total = total + (Double.parseDouble(profileListView.get(i).Price) * Integer.parseInt(profileListView.get(i).Quntity));

        }


        finalTotal = total + 3.0;
        txtOrderRuppes.setText("€"+finalTotal + "");

     //   holder.txtPrice.setText("€" + price + "");
    }


    public static JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            address = address.replaceAll(" ", "%20");
            Log.e("address url", "http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();


            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    public LatLng getLatLong(JSONObject jsonObject) {
        LatLng l = null;
        try {

            myLongitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lng");

            myLatitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                    .getJSONObject("geometry").getJSONObject("location")
                    .getDouble("lat");
            l = new LatLng(myLatitude, myLongitude);

        } catch (JSONException e) {


        }

        return l;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_cart_detail_final);
        mCardInputWidget = (CardInputWidget) findViewById(R.id.card_input_widget);
        myCal=Calendar.getInstance();



        if((myCal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
            myDateString= String.format("%02d.%02d", myCal.get(Calendar.HOUR), +myCal.get(Calendar.MINUTE))+" "+"PM";
        }else{
            myDateString= String.format("%02d.%02d", myCal.get(Calendar.HOUR), +myCal.get(Calendar.MINUTE))+" "+"AM";
        }


                java.text.DateFormat dft = new java.text.SimpleDateFormat("h.mm a", Locale.US);

        try {
            myDate= dft.parse(myDateString);
            myCal.setTime(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        etDetail= (EditText) findViewById(R.id.etDetail);
        etMobile= (EditText) findViewById(R.id.etMobile);
        txtOrderRuppes = (TextView) findViewById(R.id.txtOrderRuppes);
        llCheckout = (LinearLayout) findViewById(R.id.llCheckout);
        llVia = (LinearLayout) findViewById(R.id.llVia);
        llOradio = (LinearLayout) findViewById(R.id.llOradio);
        llPragmento = (LinearLayout) findViewById(R.id.llPragmento);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtSelectTime = (TextView) findViewById(R.id.txtSelectTime);

        txtSelectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = onCreateDialogSingleChoice();
                dialog.show();

            }
        });
        etDetail.setText(PrefUtils.getAddress(CustomerCartDetailFinalActivity.this).actualAddressName);
        txtAddress.setText(PrefUtils.getAddress(CustomerCartDetailFinalActivity.this).addressName);

        llVia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(CustomerCartDetailFinalActivity.this, CustomerIndirizziSalvatiActivity.class);
//                i.putExtra("is_checkout", true);
//                startActivity(i);
            }
        });

        llOradio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        llPragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerCartDetailFinalActivity.this, CustomerPaymentDetailsActivity.class);
                startActivity(i);

            }
        });

        position = getIntent().getIntExtra("position", 0);

        getCartDataList = PrefUtils.getCartDataList(CustomerCartDetailFinalActivity.this);



        Log.e("Timing",getCartDataList.getCartDataArrayList.get(position).Timing);
        Log.e("launch start",getCartDataList.getCartDataArrayList.get(position).LunchStartTime);
        Log.e("launch end",getCartDataList.getCartDataArrayList.get(position).LunchEndTime);
        Log.e("dinner start",getCartDataList.getCartDataArrayList.get(position).DinnerStartTime);
        Log.e("dinner end",getCartDataList.getCartDataArrayList.get(position).DinnerEndTime);



        if(!getCartDataList.getCartDataArrayList.get(position).LunchStartTime.equalsIgnoreCase("0")){
            java.text.DateFormat df = new java.text.SimpleDateFormat("h.mm a", Locale.US);
            java.util.Date date1 = null;
            try {
                date1 = df.parse(getCartDataList.getCartDataArrayList.get(position).LunchStartTime);
            } catch (ParseException e) {
                finish();
                e.printStackTrace();

        }
            java.util.Date date2 = null;
            try {
                date2 = df.parse(getCartDataList.getCartDataArrayList.get(position).LunchEndTime);
            } catch (ParseException e) {
                finish();
                e.printStackTrace();
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            if(cal.getTimeInMillis()<myCal.getTimeInMillis()) {
                cal.setTime(myDate);
            }
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);
            cal.add(Calendar.MINUTE,Integer.parseInt(getCartDataList.getCartDataArrayList.get(position).Timing.split(" ")[0]));


//            cal2.add(Calendar.MINUTE,Integer.parseInt(getCartDataList.getCartDataArrayList.get(position).Timing.split(" ")[0]));
            for(long i=0;i<100;i++){
                if(cal.get(Calendar.HOUR)<cal2.get(Calendar.HOUR) ){

//                Log.e("hour",cal.get(Calendar.HOUR)+"");
//                Log.e("min",cal.get(Calendar.MINUTE)+"");
                    String from=null;
                    if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
                        from= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"PM";
                    }else{
                        from= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"AM";
                    }

                    cal.add(Calendar.MINUTE,15);
//                Log.e("hour",cal.get(Calendar.HOUR)+"");
//                Log.e("min",cal.get(Calendar.MINUTE)+"");
                    String to=null;
                    if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
                        to= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"PM";
                    }else{
                        to= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"AM";
                    }
                    if(cal.getTimeInMillis()<cal2.getTimeInMillis() ) {
                        timingList.add(from + " - " + to);
                    }

                    Log.e("lunch time",from+" - "+to);
                }

            }

        }


        if(!getCartDataList.getCartDataArrayList.get(position).DinnerStartTime.equalsIgnoreCase("0")){
        java.text.DateFormat df = new java.text.SimpleDateFormat("h.mm a", Locale.US);
        java.util.Date date1 = null;
        try {
            date1 = df.parse(getCartDataList.getCartDataArrayList.get(position).DinnerStartTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.util.Date date2 = null;
        try {
            date2 = df.parse(getCartDataList.getCartDataArrayList.get(position).DinnerEndTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = date2.getTime() - date1.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);

            if(cal.getTimeInMillis()<myCal.getTimeInMillis()) {
                cal.setTime(myDate);
            }
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
            cal.add(Calendar.MINUTE,Integer.parseInt(getCartDataList.getCartDataArrayList.get(position).Timing.split(" ")[0]));
//            cal2.add(Calendar.MINUTE,Integer.parseInt(getCartDataList.getCartDataArrayList.get(position).Timing.split(" ")[0]));
    for(int i=0;i<100;i++){
        if(cal.getTimeInMillis()<cal2.getTimeInMillis() ){

//                Log.e("hour",cal.get(Calendar.HOUR)+"");
//                Log.e("min",cal.get(Calendar.MINUTE)+"");
            String from=null;
            if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
                from= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"PM";
            }else{
                from= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"AM";

            }
            cal.add(Calendar.MINUTE,15);

//                Log.e("hour",cal.get(Calendar.HOUR)+"");
//                Log.e("min",cal.get(Calendar.MINUTE)+"");
            String to=null;
            if((cal.get(Calendar.AM_PM)+"").equalsIgnoreCase("1")){
                to= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"PM";
            }else{
                to= String.format("%02d:%02d", cal.get(Calendar.HOUR), +cal.get(Calendar.MINUTE))+" "+"AM";
            }

            if(cal.getTimeInMillis()<cal2.getTimeInMillis() ) {
                timingList.add(from + " - " + to);
            }



            Log.e("dinner time",from+" - "+to);
        }

    }

}

        profileListView = getCartDataList.getCartDataArrayList.get(position).cartItemArrayList;
        setTotal();
        llCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cardToSave = mCardInputWidget.getCard();
              if(etDetail.getText().toString().length()==0){
                    Toast.makeText(CustomerCartDetailFinalActivity.this, "Selezionare Istruzio per corriere", Toast.LENGTH_SHORT).show();
                }else if(etMobile.getText().toString().length()==0){
                    Toast.makeText(CustomerCartDetailFinalActivity.this, "Selezionare il numero di cellulare", Toast.LENGTH_SHORT).show();
                }else if(txtSelectTime.getText().toString().equalsIgnoreCase("Seleziona ora")){
                    Toast.makeText(CustomerCartDetailFinalActivity.this, "Selezionare Seleziona ora", Toast.LENGTH_SHORT).show();
                }else  if (cardToSave == null) {
                  Toast.makeText(CustomerCartDetailFinalActivity.this, "Riempire i dettagli della carta", Toast.LENGTH_SHORT).show();
              }else{

                    Log.e("cvv",cardToSave.getCVC()+"");
                    Log.e("CardNumber",cardToSave.getNumber()+"");
                    Log.e("month",cardToSave.getExpMonth()+"");
                    Log.e("year",cardToSave.getExpYear()+"");
                  progressDialog   = new ProgressDialog(CustomerCartDetailFinalActivity.this);
                    progressDialog.setMessage("loading.....");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    Stripe stripe = new Stripe(CustomerCartDetailFinalActivity.this, "pk_live_goJTI3NMp5XDikKTCvGkzOBt");
                    stripe.createToken(
                            cardToSave,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    // Send token to your server
                                    Log.e("token",token.getId()+"");

                                    Log.e("url","https://www.flyingfood.it/FlyingFoodPayment/request.php?cost="+((int)finalTotal*100)+"&tokens="+token.getId()+"&key=5a14ec5b310164f2dfe49e86b06124a1&email="+PrefUtils.getUser(CustomerCartDetailFinalActivity.this).Email);
                                    new GetServiceCall("https://www.flyingfood.it/FlyingFoodPayment/request.php?cost="+((int)finalTotal*100)+"&tokens="+token.getId()+"&key=5a14ec5b310164f2dfe49e86b06124a1&email="+PrefUtils.getUser(CustomerCartDetailFinalActivity.this).Email,GetServiceCall.TYPE_JSONOBJECT){

                                        @Override
                                        public void response(String response) {
                                            TransactionResponse submitResponse = new GsonBuilder().create().fromJson(response, TransactionResponse.class);
                                            Log.e("response",response);
                                            if(submitResponse.data.equalsIgnoreCase("success")){
                                                getOrdersUserIdWise();
                                            }else{
                                                Toast.makeText(CustomerCartDetailFinalActivity.this,submitResponse.message,Toast.LENGTH_LONG).show();
                                            }

                                        }

                                        @Override
                                        public void error(VolleyError error) {
                                            Log.e("error",error.getMessage());
                                            getOrdersUserIdWise();
                                        }
                                    }.start();
                                }
                                public void onError(Exception error) {
                                    // Show localized error message
                                    Toast.makeText(CustomerCartDetailFinalActivity.this,
                                            error.getLocalizedMessage(),
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                    );

                }

            }
        });

        initMap();
        setToolbar();


        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                from = getLatLong(getLocationInfo(PrefUtils.getAddress(CustomerCartDetailFinalActivity.this).addressName));

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                new CountDownTimer(1000, 1000) {

                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        LatLng latLng = null;
                        try {
                            latLng = new LatLng(from.latitude, from.longitude);
                            mymap.addMarker(new MarkerOptions().position(latLng).title(getIntent().getStringExtra("RetaurantName")));
//                        mymap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                            mymap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            mymap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }.start();

            }
        }.execute();
    }

    public Dialog onCreateDialogSingleChoice() {

//Initialize the Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//Source of the data in the DIalog
//       = {"06:30 PM - 06:45 PM", "06:45 PM - 07:00 PM", "07:00 PM - 07:15 PM", "07:15 PM - 07:30 PM", "07:30 PM - 07:45 PM", "07:45 PM - 08:00 PM", "08:00 PM - 08:15 PM", "08:15 PM - 08:30 PM", "08:30 PM - 08:45 PM", "08:45 PM - 09:00 PM", "09:00 PM - 09:15 PM", "09:15 PM - 09:30 PM", "09:30 PM - 09:45 PM"};
        final String[] array=new String[timingList.size()];
        for(int i=0;i<timingList.size();i++){
            array[i]=timingList.get(i);
}
// Set the dialog title
        builder.setTitle("Select Time")
// Specify the list array, the items to be selected by default (null for none),
// and the listener through which to receive callbacks when items are selected
                .setSingleChoiceItems(array, 1, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
// TODO Auto-generated method stub

                        Log.e("data",array.length+"");
                        timeValue = array[which].toString();
                        txtSelectTime.setText(timeValue);
                        dialog.dismiss();
                    }
                });

// Set the action buttons
//                .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//// User clicked OK, so save the result somewhere
//// or return them to the component that opened the dialog
//timeValue = array[id].toString();
//                        txtSelectTime.setText(timeValue);
//                    }
//                })
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.dismiss();
//                    }
//                });

        return builder.create();
    }
    //  Call this function from any where of the activity through this way.


    public int getRandomNumber(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private void getOrdersUserIdWise() {

        if (isNetworkConnected()) {




            JSONArray jsonArray = new JSONArray();


            for (int i = 0; i < getCartDataList.getCartDataArrayList.get(position).cartItemArrayList.size(); i++) {
                try {
                    JSONObject productObject = new JSONObject();
                    productObject.put("Description", getCartDataList.getCartDataArrayList.get(position).cartItemArrayList.get(i).Description+"");
                    productObject.put("FoodId", getCartDataList.getCartDataArrayList.get(position).cartItemArrayList.get(i).FoodId + "");
                    productObject.put("MenuId", getCartDataList.getCartDataArrayList.get(position).cartItemArrayList.get(i).MenuId + "");
                    productObject.put("Price", getCartDataList.getCartDataArrayList.get(position).cartItemArrayList.get(i).Price + "");
                    productObject.put("Quntity", getCartDataList.getCartDataArrayList.get(position).cartItemArrayList.get(i).Quntity + "");


                    jsonArray.put(productObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            JSONObject requesObject = new JSONObject();

            try {

                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                requesObject.put("Description", etDetail.getText().toString()+"");
                requesObject.put("RestaurantId", getCartDataList.getCartDataArrayList.get(position).RestaurantId + "");
                requesObject.put("UserID", PrefUtils.getUser(CustomerCartDetailFinalActivity.this).UserID);
                requesObject.put("CartUniqueID", getCartDataList.CartUniqueID + "");
                requesObject.put("DeliveryAddress", PrefUtils.getAddress(CustomerCartDetailFinalActivity.this).addressName);
                requesObject.put("DeliveryTime",timeValue);
                requesObject.put("DeviceToken", PrefUtils.getToken(CustomerCartDetailFinalActivity.this).token);
                requesObject.put("Mobile", etMobile.getText().toString()+"");
                requesObject.put("PlaceOrderInnerReqDC", jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", requesObject + "");

            new PostServiceCall(AppConstants.PlaceOrder, requesObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("cart response...", response + "");
                    ForgotPasswordResponse submitResponse = new GsonBuilder().create().fromJson(response, ForgotPasswordResponse.class);
                    Log.e("cart response1...", response + "");
                    Log.e("in 1", "yes");

                    Log.e("after clear", "yes");
//                    try {
//                        randomNumberModel = PrefUtils.getRandom(CustomerCartDetailFinalActivity.this);
//                        if (randomNumberModel == null) {
//                            randomNumberModel = new RandomNumberModel();
//                            randomNumberModel.randomNumber = getRandomNumber(1, 10000000) + "";
//                            PrefUtils.setRandom(randomNumberModel, CustomerCartDetailFinalActivity.this);
//                        }
//                    } catch (Exception e) {
//
//                        e.printStackTrace();
//                        randomNumberModel = new RandomNumberModel();
//                        randomNumberModel.randomNumber = getRandomNumber(1, 10000000) + "";
//                        PrefUtils.setRandom(randomNumberModel, CustomerCartDetailFinalActivity.this);
//                    }

                    Toast.makeText(CustomerCartDetailFinalActivity.this, "Ordine piazzato!", Toast.LENGTH_LONG).show();

                    Log.e("yes here", "true");
                    finish();
//                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
//
//
//
//                    } else {
//                        Log.e("in 1","no");
//                        Toast.makeText(CustomerCartDetailFinalActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
//                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("in error", "yes");
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    private void initMap() {

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void onMapReady(GoogleMap map) {
        mymap = map;

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("CheckOut");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }
}
