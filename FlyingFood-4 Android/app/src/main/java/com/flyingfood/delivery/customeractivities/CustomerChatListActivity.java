package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.CommonChatActivity;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;

import com.flyingfood.delivery.model.Account.GetLastOrderMEssageCustomerMain;
import com.flyingfood.delivery.model.Account.GetLastOrderMessageCustomer;
import com.flyingfood.delivery.model.Cart.GetOrderUserIdWiseMain;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CustomerChatListActivity extends AppCompatActivity {


    private GridView profileList;
    private CustomerChatListActivity.MyCustomAdapter myCustomAdapter;
    private ArrayList<GetLastOrderMessageCustomer> profileListView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_chat_list);
        setToolbar();
        profileList = (GridView)findViewById(R.id.lvProfiles);
        getOrdersUserIdWise();
        
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("Chat");
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }




    private void getOrdersUserIdWise() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerChatListActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("CustomerId", PrefUtils.getUser(CustomerChatListActivity.this).UserID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GET_LAST_ORDER_MESSAGE_CUSOMER, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetLastOrderMEssageCustomerMain submitResponse = new GsonBuilder().create().fromJson(response, GetLastOrderMEssageCustomerMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {
                        profileListView=submitResponse.getLastOrderMessageCustomerData.getLastOrderMessageCustomerArrayList;

                        myCustomAdapter = new CustomerChatListActivity.MyCustomAdapter(CustomerChatListActivity.this, profileListView);
                        profileList.setAdapter(myCustomAdapter);



                    } else {
                        Toast.makeText(CustomerChatListActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerChatListActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetLastOrderMessageCustomer> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetLastOrderMessageCustomer> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetLastOrderMessageCustomer>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtMessage, txtTitle;
            ImageView imgRestaurant;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final CustomerChatListActivity.MyCustomAdapter.ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_chat_list_item, parent, false);
                holder = new CustomerChatListActivity.MyCustomAdapter.ViewHolder();

//
                holder.txtMessage= (TextView) convertView.findViewById(R.id.txtMessage);
                holder.txtTitle= (TextView) convertView.findViewById(R.id.txtTitle);
                holder.imgRestaurant= (ImageView) convertView.findViewById(R.id.imgItemIcon);


                convertView.setTag(holder);
            } else {
                holder = (CustomerChatListActivity.MyCustomAdapter.ViewHolder) convertView.getTag();
            }

            holder.txtMessage.setText(profileListView.get(position).Text.trim());
            holder.txtTitle.setText(profileListView.get(position).RestaurantName.trim());
            Glide.with(CustomerChatListActivity.this).load(profileListView.get(position).RestaurantImage).into(holder.imgRestaurant);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i=new Intent(CustomerChatListActivity.this, CommonChatActivity.class);
                    i.putExtra("order_id",profileListView.get(position).OrderId+"");
                    if(profileListView.get(position).ReceiverID.equalsIgnoreCase(PrefUtils.getUser(CustomerChatListActivity.this).UserID)){
                        i.putExtra("rec_id",profileListView.get(position).SenderID+"");
                    }else{
                        i.putExtra("rec_id",profileListView.get(position).ReceiverID+"");
                    }
                    i.putExtra("send_id",PrefUtils.getUser(CustomerChatListActivity.this).UserID+"");
                    startActivity(i);
                }
            });
            return convertView;
        }



    }
    
}
