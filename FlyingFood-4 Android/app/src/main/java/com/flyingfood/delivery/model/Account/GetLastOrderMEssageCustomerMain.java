package com.flyingfood.delivery.model.Account;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 01/07/17.
 */

public class GetLastOrderMEssageCustomerMain {

    @SerializedName("Response")
   public  CommonResponse commonResponse;

    @SerializedName("ResponseData")
   public GetLastOrderMessageCustomerData getLastOrderMessageCustomerData;
}
