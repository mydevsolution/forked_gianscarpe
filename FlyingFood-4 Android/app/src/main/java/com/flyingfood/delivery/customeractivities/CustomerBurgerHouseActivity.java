package com.flyingfood.delivery.customeractivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.custom.AppConstants;
import com.flyingfood.delivery.custom.PostServiceCall;
import com.flyingfood.delivery.custom.PrefUtils;
import com.flyingfood.delivery.model.Restaurant.GetRestaurantMenu;
import com.flyingfood.delivery.model.Restaurant.GetRestaurantMenuMain;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class CustomerBurgerHouseActivity extends AppCompatActivity {


    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<GetRestaurantMenu> profileListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_burger_house);
setToolbar();
        profileList = (GridView)findViewById(R.id.lvProfiles);
        getRestaurantMenu();
    }



    private void getRestaurantMenu() {

        if(isNetworkConnected()) {

            final ProgressDialog progressDialog = new ProgressDialog(CustomerBurgerHouseActivity.this);
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();


            JSONObject registrationObject = new JSONObject();

            try {
                registrationObject.put("FoodId",getIntent().getStringExtra("food_id"));
                registrationObject.put("RestaurantId",getIntent().getStringExtra("res_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("login request object", registrationObject + "");

            new PostServiceCall(AppConstants.GetRestaurantMenu, registrationObject) {

                @Override
                public void response(String response) {
                    progressDialog.dismiss();
                    Log.e("login response...", response + "");
                    GetRestaurantMenuMain submitResponse = new GsonBuilder().create().fromJson(response, GetRestaurantMenuMain.class);

                    if (submitResponse.commonResponse.ResponseCode.equalsIgnoreCase("1")) {

                        profileListView=submitResponse.getRestaurantMenuData.getRestaurantMenuArrayList;
                        Log.e("size",profileListView.size()+"");
                        myCustomAdapter=new CustomerBurgerHouseActivity.MyCustomAdapter(CustomerBurgerHouseActivity.this,profileListView);
                        profileList.setAdapter(myCustomAdapter);
                    } else {
                        Toast.makeText(CustomerBurgerHouseActivity.this, submitResponse.commonResponse.ResponseMsg, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void error(String error) {
                    progressDialog.dismiss();
                    Log.e("login response...", error + "");
                }
            }.call();

        }

    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) CustomerBurgerHouseActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<GetRestaurantMenu> profileListView;
        Context context;

        @Override
        public int getCount() {
            return profileListView.size();
        }
        ArrayList<GetRestaurantMenu> arraylist;

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
            arraylist = new ArrayList<GetRestaurantMenu>();
            arraylist.addAll(profileListView);
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtPrice,txtName,txtSubName;



        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.customer_burger_house_list_item, parent, false);
                holder = new ViewHolder();

                holder.txtPrice= (TextView) convertView.findViewById(R.id.txtPrice);
                holder.txtName= (TextView) convertView.findViewById(R.id.txtName);
                holder.txtSubName= (TextView) convertView.findViewById(R.id.txtSubName);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.txtPrice.setText("€"+profileListView.get(position).Price);
            holder.txtName.setText(profileListView.get(position).MenuName.trim());
            holder.txtSubName.setText(profileListView.get(position).Description.trim());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Log.e("description",profileListView.get(position).Description);
                    Log.e("food id",profileListView.get(position).FoodId);
                    Log.e("menu id",profileListView.get(position).MenuId);
                    Log.e("menu name",profileListView.get(position).MenuName);
                    Log.e("price",profileListView.get(position).Price);
                    Log.e("restaurant id",profileListView.get(position).RestaurantId);



                    Intent i=new Intent(CustomerBurgerHouseActivity.this,CustomerProductDetailActivity.class);
                    i.putExtra("description",profileListView.get(position).Description+"");
                    i.putExtra("food_id",profileListView.get(position).FoodId+"");
                    i.putExtra("menu_id",profileListView.get(position).MenuId+"");
                    i.putExtra("menu_name",profileListView.get(position).MenuName+"");
                    i.putExtra("price",profileListView.get(position).Price+"");
                    i.putExtra("res_id",profileListView.get(position).RestaurantId+"");


                    startActivity(i);
                    finish();



//                    Log.e("restaurant id",profileListView.get(position).RestaurantID+"");
//                    String foodId=null;
//                    for(int i=0;i<horizontalList.size();i++){
//                        if(horizontalList.get(i).FoodName.equalsIgnoreCase(profileListView.get(position).Category)){
//                            foodId=horizontalList.get(i).FoodId;
//                        }
//                    }
//                    Log.e("food id",foodId+"");
//



                }
            });
            return convertView;
        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            profileListView.clear();
            if (charText.length() == 0) {
                profileListView.addAll(arraylist);

            } else {
                for (GetRestaurantMenu movieDetails : arraylist) {
                    if (charText.length() != 0 && movieDetails.MenuName.toLowerCase(Locale.getDefault()).contains(charText)) {
                        profileListView.add(movieDetails);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
//                Toast.makeText(MainActivity.this, "called", Toast.LENGTH_SHORT).show();



                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myCustomAdapter.filter(searchQuery.toString().trim());
                profileList.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle(getIntent().getStringExtra("food_name"));
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

}
