package com.flyingfood.delivery.model.Restaurant;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 01/07/17.
 */

public class GetRestaurantMenuData {

    @SerializedName("Data")
    public ArrayList<GetRestaurantMenu> getRestaurantMenuArrayList;
}
