package com.flyingfood.delivery.model.Restaurant;

import com.flyingfood.delivery.model.CommonResponse;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mr. DIPAK on 07-Jul-17.
 */

public class RestaurantAddOrderResponse {



    @SerializedName("response")
   public CommonResponse commonResponse;

    public CommonResponse getCommonResponse() {
        return commonResponse;
    }

    public void setCommonResponse(CommonResponse commonResponse) {
        this.commonResponse = commonResponse;
    }
}
