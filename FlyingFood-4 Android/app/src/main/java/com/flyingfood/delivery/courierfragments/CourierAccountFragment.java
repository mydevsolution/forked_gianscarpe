package com.flyingfood.delivery.courierfragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyingfood.delivery.R;
import com.flyingfood.delivery.commonactivities.MainActivity;
import com.flyingfood.delivery.courieractivities.CourierAccountActivity;
import com.flyingfood.delivery.courieractivities.CourierOrderHistoryActivity;
import com.flyingfood.delivery.courieractivities.CourierPesonalDataActivity;
import com.flyingfood.delivery.courieractivities.CourierRichiediSupportoActivity;
import com.flyingfood.delivery.courieractivities.CourierWeeklyAvaibilityActivity;
import com.flyingfood.delivery.custom.PrefUtils;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourierAccountFragment extends Fragment {

    LinearLayout llPersonalData, llOrderHistory, llWeeklyAvaibility, llPaymentMethod, llSupportMail;
    TextView txtLogout;
    public CourierAccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View convertView= inflater.inflate(R.layout.fragment_courier_account, container, false);
        llPersonalData = (LinearLayout) convertView.findViewById(R.id.llPersonalData);
        llOrderHistory = (LinearLayout)convertView. findViewById(R.id.llOrderHistory);
        llWeeklyAvaibility = (LinearLayout)convertView. findViewById(R.id.llWeeklyAvaibility);
        llPaymentMethod = (LinearLayout) convertView.findViewById(R.id.llPaymentMethod);
        llSupportMail = (LinearLayout)convertView. findViewById(R.id.llSupportMail);

        Calendar calendar = Calendar.getInstance();
        final int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                // Current day is Sunday

            case Calendar.MONDAY:
                // Current day is Monday

            case Calendar.TUESDAY:
                // etc.
        }

        llPersonalData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CourierPesonalDataActivity.class);
                startActivity(intent);

            }
        });
        llOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CourierOrderHistoryActivity.class);
                startActivity(intent);


            }
        });
        llWeeklyAvaibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(day== Calendar.MONDAY){
                    Intent intent = new Intent(getActivity(), CourierWeeklyAvaibilityActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getActivity(), "You can change only on monday", Toast.LENGTH_SHORT).show();
                }



            }
        });
        llPaymentMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(),Cour.class);
//                startActivity(intent);


            }
        });
        llSupportMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CourierRichiediSupportoActivity.class);
                startActivity(intent);


            }
        });
        
        return convertView;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater. inflate(R.menu.menu_account_logout_button, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_account_logout_button, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnLogOut:

                PrefUtils.clearCurrentUser(getActivity());

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
