package com.flyingfood.delivery.restaurantactivities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyingfood.delivery.R;

import java.util.ArrayList;

public class RestaurantOrderActivity extends AppCompatActivity {


    private GridView profileList;
    private MyCustomAdapter myCustomAdapter;
    private ArrayList<String> profileListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_order);

        profileList = (GridView)findViewById(R.id.lvProfiles);
        profileListView=new ArrayList<>();

        myCustomAdapter = new MyCustomAdapter(RestaurantOrderActivity.this, profileListView);
        profileList.setAdapter(myCustomAdapter);
    }
    class MyCustomAdapter extends BaseAdapter

    {

        private ArrayList<String> profileListView;
        Context context;

        @Override
        public int getCount() {
            return 10;
        }

        public MyCustomAdapter(Context context, ArrayList postArrayList) {

            this.context = context;
            this.profileListView = postArrayList;
        }

        @Override
        public Object getItem(int position) {
            return profileListView.get(position);
        }

        class ViewHolder {
            TextView txtQuestion, txt1,txt2,txt3,txt4,txt5,txt6;
            ImageView imgProfile;
            LinearLayout llFeed,llOption1,llOption2,llOption3,llOption4,llOption5,llOption6;

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.restaurant_order_list_item, parent, false);
                holder = new ViewHolder();

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return convertView;
        }
    }
}
