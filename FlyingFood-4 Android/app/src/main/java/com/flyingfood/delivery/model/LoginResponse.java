package com.flyingfood.delivery.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nirav on 30/06/17.
 */

public class LoginResponse {

    @SerializedName("Address")
    public String Address;
    @SerializedName("Age")
    public String Age;
    @SerializedName("CanOrder")
    public String CanOrder;
    @SerializedName("CourierAvailableDays")
    public String CourierAvailableDays;
    @SerializedName("Email")
    public String Email;
    @SerializedName("EndTime")
    public String EndTime;
    @SerializedName("Mobile")
    public String Mobile;
    @SerializedName("Name")
    public String Name;
    @SerializedName("RestaurantId")
    public String RestaurantId;
    @SerializedName("RoleMasterID")
    public String RoleMasterID;
    @SerializedName("StartTime")
    public String StartTime;
    @SerializedName("UserID")
    public String UserID;
    @SerializedName("Username")
    public String Username;
    @SerializedName("VehicleType")
    public String VehicleType;
    @SerializedName("WaitingTime")
    public String WaitingTime;
    @SerializedName("Response")
    public CommonResponse commonResponse;
}
