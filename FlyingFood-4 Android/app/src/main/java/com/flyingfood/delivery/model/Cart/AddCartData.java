package com.flyingfood.delivery.model.Cart;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nirav on 04/07/17.
 */

public class AddCartData {

    @SerializedName("Data")
    public ArrayList<CartItem> cartItemArrayList;

}
